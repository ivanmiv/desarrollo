package Registros;

import Datos.ManejoDatosRepuesto;
import Datos.ManejoDatosSede;
import Datos.ValidarNumero;
import javax.swing.JFrame;
import javax.swing.JOptionPane;




public class IngresarRepuesto extends javax.swing.JFrame {

    JFrame padre;
    public IngresarRepuesto(JFrame padre) {
        this.padre=padre;
        initComponents();  
        setLocationRelativeTo(null);
        new ManejoDatosSede().CargarDatosComboBox(jcbNombreSede, "sedes");
    
        jtfCantidadR.setInputVerifier(new ValidarNumero(jlValidacion, 100));
        jtfPrecioR.setInputVerifier(new ValidarNumero(jlValidacion, 100));
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCrearUser = new javax.swing.JPanel();
        jtfCantidadR = new javax.swing.JTextField();
        jlIconRepuestos = new javax.swing.JLabel();
        jlNombreR = new javax.swing.JLabel();
        jlCantidadR = new javax.swing.JLabel();
        jlCodigoSedeR = new javax.swing.JLabel();
        jlDescripcion = new javax.swing.JLabel();
        jlPrecio = new javax.swing.JLabel();
        jlBotonResgistrarS = new javax.swing.JLabel();
        jlAtras = new javax.swing.JLabel();
        jtfPrecioR = new javax.swing.JTextField();
        jtfNombreR = new javax.swing.JTextField();
        jcbNombreSede = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtaDescripcionR = new javax.swing.JTextArea();
        jlValidacion = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelCrearUser.setBackground(new java.awt.Color(255, 255, 255));

        jlIconRepuestos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/IconRepuestos.jpg"))); // NOI18N

        jlNombreR.setText("Nombre:");

        jlCantidadR.setText("Cantidad:");

        jlCodigoSedeR.setText("Cod. Sede:");

        jlDescripcion.setText("Descripcion:");

        jlPrecio.setText("Precio:");

        jlBotonResgistrarS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/registrar.jpg"))); // NOI18N
        jlBotonResgistrarS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonResgistrarSMouseClicked(evt);
            }
        });

        jlAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlAtrasMouseClicked(evt);
            }
        });

        jcbNombreSede.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione" }));

        jtaDescripcionR.setColumns(20);
        jtaDescripcionR.setRows(5);
        jScrollPane1.setViewportView(jtaDescripcionR);

        jlValidacion.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanelCrearUserLayout = new javax.swing.GroupLayout(jPanelCrearUser);
        jPanelCrearUser.setLayout(jPanelCrearUserLayout);
        jPanelCrearUserLayout.setHorizontalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlPrecio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jtfPrecioR, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlCantidadR)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                        .addComponent(jtfCantidadR, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlNombreR)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jtfNombreR, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlCodigoSedeR)
                            .addComponent(jlDescripcion))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jcbNombreSede, 0, 275, Short.MAX_VALUE)
                            .addComponent(jScrollPane1))))
                .addGap(0, 92, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconRepuestos)
                        .addGap(58, 58, 58)
                        .addComponent(jlAtras)
                        .addGap(43, 43, 43))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlBotonResgistrarS)
                        .addGap(183, 183, 183))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlValidacion)
                        .addGap(219, 219, 219))))
        );
        jPanelCrearUserLayout.setVerticalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jlAtras))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jlIconRepuestos)))
                .addGap(44, 44, 44)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlNombreR)
                    .addComponent(jtfNombreR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCantidadR)
                    .addComponent(jtfCantidadR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfPrecioR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlPrecio))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbNombreSede, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlCodigoSedeR))
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jlDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34)
                .addComponent(jlValidacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlBotonResgistrarS)
                .addContainerGap(54, Short.MAX_VALUE))
        );

        getContentPane().add(jPanelCrearUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlAtrasMouseClicked
      this.setVisible(false);
      padre.setVisible(true);
       this.dispose();
    }//GEN-LAST:event_jlAtrasMouseClicked

    private void jlBotonResgistrarSMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonResgistrarSMouseClicked
        
        String nombresede = (String)jcbNombreSede.getSelectedItem();
        
        String nombre = jtfNombreR.getText().trim();
        
        String descripcion = jtaDescripcionR.getText();
        int cantidad = 0;
        int precio = 0;
        int sede = 0;
        
        if(jlValidacion.getText().length()!=0){
            return;
        }
        if (nombre.length() == 0
            || nombresede.length() == 0
            || descripcion.length() == 0
            || jtfPrecioR.getText().trim().length() == 0
            || jtfCantidadR.getText().trim().length() == 0
            || nombresede.equalsIgnoreCase("Seleccione")
            ) {
            JOptionPane.showMessageDialog(this, "Por favor complete todos los campos", "Ingrso de repuesto de vehiculo", JOptionPane.INFORMATION_MESSAGE);
            return;
        }else {
            cantidad = Integer.parseInt(jtfCantidadR.getText().trim());
            precio = Integer.parseInt(jtfPrecioR.getText().trim());
            sede = new ManejoDatosSede().TraerCodigoSedeNombre(nombresede.trim());
        }
        if(!nombresede.equalsIgnoreCase("Seleccione") && cantidad>0 && precio>0){
                 
            String mensaje = new ManejoDatosRepuesto().RealizarRegistroRepuesto(nombre, precio, cantidad,
                    descripcion,sede);
            JOptionPane.showMessageDialog(this, mensaje, "Registro de repuesto", JOptionPane.INFORMATION_MESSAGE);
            this.padre.setVisible(true);
            this.dispose();
        
        }else {
            JOptionPane.showMessageDialog(this, "problema seleccionando la sede o algun valor numerico", "Registro de repuesto", JOptionPane.INFORMATION_MESSAGE);
        }
        
        
    }//GEN-LAST:event_jlBotonResgistrarSMouseClicked

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelCrearUser;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox jcbNombreSede;
    private javax.swing.JLabel jlAtras;
    private javax.swing.JLabel jlBotonResgistrarS;
    private javax.swing.JLabel jlCantidadR;
    private javax.swing.JLabel jlCodigoSedeR;
    private javax.swing.JLabel jlDescripcion;
    private javax.swing.JLabel jlIconRepuestos;
    private javax.swing.JLabel jlNombreR;
    private javax.swing.JLabel jlPrecio;
    private javax.swing.JLabel jlValidacion;
    private javax.swing.JTextArea jtaDescripcionR;
    private javax.swing.JTextField jtfCantidadR;
    private javax.swing.JTextField jtfNombreR;
    private javax.swing.JTextField jtfPrecioR;
    // End of variables declaration//GEN-END:variables
}
