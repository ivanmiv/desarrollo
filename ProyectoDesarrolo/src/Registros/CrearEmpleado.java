package Registros;


import Datos.*;
import Datos.ManejoDatosEmpleado;
import Datos.ManejoDatosSede;
import Datos.ValidarCantidadCaracteres;
import Datos.ValidarEmail;
import Datos.ValidarNumero;
import javax.swing.JFrame;
import javax.swing.JOptionPane;




public class CrearEmpleado extends javax.swing.JFrame {

    JFrame padre;
    public CrearEmpleado(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        new ManejoDatosSede().CargarDatosComboBox(jcbNombreSede, "sedes");
        jcbCargoUser.setSelectedItem("Gerente");
        jtfNombreUser.setInputVerifier(new ValidarCantidadCaracteres(jlValidacion, 100));
        jtfTelefono.setInputVerifier(new ValidarNumero(jlValidacion, 100));
        jtfEmailUser.setInputVerifier(new ValidarEmail(jlValidacion, 100));
        jtfCedulaUser.setInputVerifier(new ValidarNumero(jlValidacion, 100));
        
    }
    
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCrearUser = new javax.swing.JPanel();
        jlIconUSer = new javax.swing.JLabel();
        jlNombreUser = new javax.swing.JLabel();
        jlCedula = new javax.swing.JLabel();
        jlCargo = new javax.swing.JLabel();
        jlSede = new javax.swing.JLabel();
        jlDireccion = new javax.swing.JLabel();
        jlTelefono = new javax.swing.JLabel();
        jlBotonCrearUser = new javax.swing.JLabel();
        jlAtras = new javax.swing.JLabel();
        jtfCedulaUser = new javax.swing.JTextField();
        jtfEmailUser = new javax.swing.JTextField();
        jtfDireccion = new javax.swing.JTextField();
        jtfNombreUser = new javax.swing.JTextField();
        jcbCargoUser = new javax.swing.JComboBox();
        jcbNombreSede = new javax.swing.JComboBox();
        jlEmail = new javax.swing.JLabel();
        jtfTelefono = new javax.swing.JTextField();
        jlValidacion = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jtfUser = new javax.swing.JTextField();
        jtfPass = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Registrar Empleado");
        setResizable(false);

        jPanelCrearUser.setBackground(new java.awt.Color(255, 255, 255));

        jlIconUSer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/contact-icon.png"))); // NOI18N

        jlNombreUser.setText("Nombre:");

        jlCedula.setText("Cédula:");

        jlCargo.setText("Cargo:");

        jlSede.setText("Sede:");

        jlDireccion.setText("Dirección:");

        jlTelefono.setText("Teléfono:");

        jlBotonCrearUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/button crear.png"))); // NOI18N
        jlBotonCrearUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonCrearUserMouseClicked(evt);
            }
        });

        jlAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlAtrasMouseClicked(evt);
            }
        });

        jcbCargoUser.setMaximumRowCount(4);
        jcbCargoUser.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione", "Gerente", "Vendedor", "Jefe de Taller" }));

        jcbNombreSede.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione" }));

        jlEmail.setText("Email:");

        jlValidacion.setForeground(new java.awt.Color(255, 0, 0));

        jLabel1.setText("Usuario:");

        jLabel2.setText("Password:");

        javax.swing.GroupLayout jPanelCrearUserLayout = new javax.swing.GroupLayout(jPanelCrearUser);
        jPanelCrearUser.setLayout(jPanelCrearUserLayout);
        jPanelCrearUserLayout.setHorizontalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlIconUSer)
                .addGap(85, 85, 85)
                .addComponent(jlAtras)
                .addGap(43, 43, 43))
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                .addComponent(jlNombreUser)
                                .addGap(29, 29, 29)
                                .addComponent(jtfNombreUser, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                    .addComponent(jlTelefono)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jtfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                    .addComponent(jlSede)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jcbNombreSede, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                                    .addComponent(jlCargo)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jcbCargoUser, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                                    .addComponent(jlCedula)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jtfCedulaUser, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                    .addComponent(jlDireccion)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                                    .addComponent(jtfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                    .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                            .addComponent(jlEmail)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                                            .addComponent(jLabel1)
                                            .addGap(28, 28, 28)))
                                    .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jtfEmailUser, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                                        .addComponent(jtfUser)))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addGap(18, 18, 18)
                                    .addComponent(jtfPass)))))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(236, 236, 236)
                        .addComponent(jlBotonCrearUser)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jlValidacion)))
                .addGap(0, 145, Short.MAX_VALUE))
        );
        jPanelCrearUserLayout.setVerticalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlAtras)
                        .addGap(127, 127, 127))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconUSer)
                        .addGap(40, 40, 40)))
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlNombreUser)
                    .addComponent(jtfNombreUser, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCedula)
                    .addComponent(jtfCedulaUser, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCargo)
                    .addComponent(jcbCargoUser, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlSede, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbNombreSede, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlDireccion))
                .addGap(20, 20, 20)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlTelefono)
                    .addComponent(jtfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlEmail)
                    .addComponent(jtfEmailUser, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfUser, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jtfPass))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlValidacion)
                        .addGap(21, 21, 21))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlBotonCrearUser, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        getContentPane().add(jPanelCrearUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlAtrasMouseClicked

    private void jlBotonCrearUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonCrearUserMouseClicked
        // Crear usuario 
        String contrasena = new Encriptar().EncriptarMd5(jtfPass.getText().trim());
        
        String nombre = jtfNombreUser.getText().trim();
        String cedula = jtfCedulaUser.getText().trim();
        String usuario = jtfUser.getText().trim();
        String direccion = jtfDireccion.getText().trim();
        String email = jtfEmailUser.getText().trim();
        String telefono = jtfTelefono.getText().trim();
        
        
        if(jlValidacion.getText().length()!=0){
            return;
        }
        if (cedula.length() == 0
            || cedula.length() == 0
            || usuario.length() == 0
            || direccion.length() == 0
            ) {
            JOptionPane.showMessageDialog(this, "Por favor complete todos los campos", "Cotizacion de vehiculo", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        
        
        int cargoSeleccionado = jcbCargoUser.getSelectedIndex();
        String sede = (String)jcbNombreSede.getSelectedItem();
        
        if (cargoSeleccionado==1){
            
        JOptionPane.showMessageDialog(this,"La sede a la que le asigno el gerente ya posee un gerente.\n"
                + "Este sera almacenado como gerente registrado en alguno sede\npero no sera gerente de la misma.\n "
                + "Para asiganrlo como gerente de alguna sede debe ir a las opciones de sede y agregar o modificar alguna sede", "Creacion de Empleado", JOptionPane.INFORMATION_MESSAGE);
          
        }
        
        String mensaje=new ManejoDatosEmpleado().RealizarRegistroEmpleado(nombre, cedula,
                usuario, direccion, email, telefono, contrasena, cargoSeleccionado,sede);
        JOptionPane.showMessageDialog(this, mensaje, "Registro de empleado", JOptionPane.INFORMATION_MESSAGE);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonCrearUserMouseClicked



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanelCrearUser;
    private javax.swing.JComboBox jcbCargoUser;
    private javax.swing.JComboBox jcbNombreSede;
    private javax.swing.JLabel jlAtras;
    private javax.swing.JLabel jlBotonCrearUser;
    private javax.swing.JLabel jlCargo;
    private javax.swing.JLabel jlCedula;
    private javax.swing.JLabel jlDireccion;
    private javax.swing.JLabel jlEmail;
    private javax.swing.JLabel jlIconUSer;
    private javax.swing.JLabel jlNombreUser;
    private javax.swing.JLabel jlSede;
    private javax.swing.JLabel jlTelefono;
    private javax.swing.JLabel jlValidacion;
    private javax.swing.JTextField jtfCedulaUser;
    private javax.swing.JTextField jtfDireccion;
    private javax.swing.JTextField jtfEmailUser;
    private javax.swing.JTextField jtfNombreUser;
    private javax.swing.JTextField jtfPass;
    private javax.swing.JTextField jtfTelefono;
    private javax.swing.JTextField jtfUser;
    // End of variables declaration//GEN-END:variables
}
