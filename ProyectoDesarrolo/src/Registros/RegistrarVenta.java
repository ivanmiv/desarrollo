/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registros;

import Datos.ManejoDatosEmpleado;
import Datos.ManejoDatosSede;
import Datos.ManejoDatosVenta;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import Datos.ValidarCantidadCaracteres;
import Datos.ValidarEmail;
import Datos.ValidarNumero;

/**
 *
 * @author Monitor
 */
public class RegistrarVenta extends javax.swing.JFrame {

    JFrame padre;

    public RegistrarVenta(JFrame padre, String cedulaVendedor) {
        this.padre = padre;
        initComponents();
        setLocationRelativeTo(null);
        jtfIdentificacionV.setText(cedulaVendedor);       

        jtfDirV.setInputVerifier(new ValidarCantidadCaracteres(jlValidacion, 100));
        jtfIdentificacionC.setInputVerifier(new ValidarCantidadCaracteres(jlValidacion, 100));
        jtfTelefonoC.setInputVerifier(new ValidarNumero(jlValidacion, 10));
        jtfEmail.setInputVerifier(new ValidarEmail(jlValidacion, 100)); 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpPanel = new javax.swing.JPanel();
        jpDatosVendedor = new javax.swing.JPanel();
        jlIdentificacionV = new javax.swing.JLabel();
        jtfIdentificacionV = new javax.swing.JTextField();
        jpDatosComprador = new javax.swing.JPanel();
        jlNombreC = new javax.swing.JLabel();
        jIdentifiacionC = new javax.swing.JLabel();
        jlTelefonoC = new javax.swing.JLabel();
        jDirC = new javax.swing.JLabel();
        jtfIdentificacionC = new javax.swing.JTextField();
        jtfNombreC = new javax.swing.JTextField();
        jtfTelefonoC = new javax.swing.JTextField();
        jtfDirV = new javax.swing.JTextField();
        jtfEmail = new javax.swing.JTextField();
        jDirC1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jlRegistrar = new javax.swing.JLabel();
        jlBotonAtras = new javax.swing.JLabel();
        jpDatosVehiculo = new javax.swing.JPanel();
        jlMarcaV = new javax.swing.JLabel();
        jtfColorV = new javax.swing.JTextField();
        jlSedeV = new javax.swing.JLabel();
        jtfSedeV = new javax.swing.JTextField();
        jlPlacaV = new javax.swing.JLabel();
        jtfModelo = new javax.swing.JTextField();
        jtfPrecio = new javax.swing.JTextField();
        jtfMarcaV = new javax.swing.JTextField();
        jlChasisV = new javax.swing.JLabel();
        jtfChasisV = new javax.swing.JTextField();
        jlModeloV = new javax.swing.JLabel();
        jlPrecio = new javax.swing.JLabel();
        jlColorV = new javax.swing.JLabel();
        jpboton2 = new javax.swing.JPanel();
        jbSeleccionar = new javax.swing.JLabel();
        jtfPlaca = new javax.swing.JFormattedTextField();
        jlValidacion = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jpPanel.setBackground(new java.awt.Color(255, 255, 255));

        jpDatosVendedor.setBackground(new java.awt.Color(255, 255, 255));
        jpDatosVendedor.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos del Vendedor", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jlIdentificacionV.setText("Identificación:");

        jtfIdentificacionV.setEditable(false);
        jtfIdentificacionV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfIdentificacionVActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpDatosVendedorLayout = new javax.swing.GroupLayout(jpDatosVendedor);
        jpDatosVendedor.setLayout(jpDatosVendedorLayout);
        jpDatosVendedorLayout.setHorizontalGroup(
            jpDatosVendedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDatosVendedorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlIdentificacionV)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtfIdentificacionV, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpDatosVendedorLayout.setVerticalGroup(
            jpDatosVendedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDatosVendedorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpDatosVendedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlIdentificacionV, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfIdentificacionV, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jpDatosComprador.setBackground(new java.awt.Color(255, 255, 255));
        jpDatosComprador.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos del Comprador", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jlNombreC.setText("Nombre:");

        jIdentifiacionC.setText("Identificación:");

        jlTelefonoC.setText("Teléfono:");

        jDirC.setText("Dirreción:");

        jtfIdentificacionC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfIdentificacionCActionPerformed(evt);
            }
        });

        jtfTelefonoC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfTelefonoCActionPerformed(evt);
            }
        });

        jDirC1.setText("E-mail:");

        javax.swing.GroupLayout jpDatosCompradorLayout = new javax.swing.GroupLayout(jpDatosComprador);
        jpDatosComprador.setLayout(jpDatosCompradorLayout);
        jpDatosCompradorLayout.setHorizontalGroup(
            jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDatosCompradorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jpDatosCompradorLayout.createSequentialGroup()
                            .addComponent(jlNombreC)
                            .addGap(23, 23, 23))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDatosCompradorLayout.createSequentialGroup()
                            .addComponent(jDirC)
                            .addGap(18, 18, 18)))
                    .addGroup(jpDatosCompradorLayout.createSequentialGroup()
                        .addComponent(jDirC1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtfNombreC)
                    .addComponent(jtfEmail)
                    .addComponent(jtfDirV, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDatosCompradorLayout.createSequentialGroup()
                        .addComponent(jIdentifiacionC)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jtfIdentificacionC, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDatosCompradorLayout.createSequentialGroup()
                        .addComponent(jlTelefonoC)
                        .addGap(18, 18, 18)
                        .addComponent(jtfTelefonoC, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jpDatosCompradorLayout.setVerticalGroup(
            jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDatosCompradorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jtfTelefonoC, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpDatosCompradorLayout.createSequentialGroup()
                        .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtfNombreC, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlTelefonoC)
                            .addComponent(jlNombreC))
                        .addGap(8, 8, 8)
                        .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jIdentifiacionC)
                                .addComponent(jtfIdentificacionC, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jtfDirV, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jDirC)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpDatosCompradorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDirC1)
                    .addComponent(jtfEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)))
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("<html>Mediante el presente contrato el VENDEDOR transfiere a titulo de venta y el COMPRADOR adquiere la propiedad del vehiculo   Automotor que a continuacion se describe: </html>");
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/IMG/logo abc.png"))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("CONTRATO DE COMPRA VENTA DE AUTOS");

        jlRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/registrar.jpg"))); // NOI18N
        jlRegistrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlRegistrarMouseClicked(evt);
            }
        });

        jlBotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlBotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAtrasMouseClicked(evt);
            }
        });

        jpDatosVehiculo.setBackground(new java.awt.Color(255, 255, 255));
        jpDatosVehiculo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos del Vehiculo", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jlMarcaV.setText("Marca:");

        jtfColorV.setEditable(false);

        jlSedeV.setText("Sede:");

        jtfSedeV.setEditable(false);

        jlPlacaV.setText("Placa:");

        jtfModelo.setEditable(false);

        jtfPrecio.setEditable(false);

        jtfMarcaV.setEditable(false);

        jlChasisV.setText("Chasis:");

        jtfChasisV.setEditable(false);

        jlModeloV.setText("Modelo:");

        jlPrecio.setText("Precio:");

        jlColorV.setText("Color:");

        jpboton2.setBackground(new java.awt.Color(101, 101, 102));
        jpboton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jpboton2MouseClicked(evt);
            }
        });

        jbSeleccionar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jbSeleccionar.setForeground(new java.awt.Color(254, 254, 254));
        jbSeleccionar.setText("<html><p><u>Seleccionar Vehiculo</u></html></p>");

        javax.swing.GroupLayout jpboton2Layout = new javax.swing.GroupLayout(jpboton2);
        jpboton2.setLayout(jpboton2Layout);
        jpboton2Layout.setHorizontalGroup(
            jpboton2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpboton2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jbSeleccionar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpboton2Layout.setVerticalGroup(
            jpboton2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpboton2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbSeleccionar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        try {
            jtfPlaca.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("UUU-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jpDatosVehiculoLayout = new javax.swing.GroupLayout(jpDatosVehiculo);
        jpDatosVehiculo.setLayout(jpDatosVehiculoLayout);
        jpDatosVehiculoLayout.setHorizontalGroup(
            jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jpboton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                        .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlModeloV)
                            .addComponent(jlChasisV)
                            .addComponent(jlMarcaV, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlSedeV))
                        .addGap(18, 18, 18)
                        .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDatosVehiculoLayout.createSequentialGroup()
                                .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                                        .addComponent(jtfChasisV, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jlPlacaV)
                                        .addGap(23, 23, 23))
                                    .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                                        .addComponent(jtfModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jlColorV)
                                        .addGap(24, 24, 24)))
                                .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jtfPlaca, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                                    .addComponent(jtfColorV)))
                            .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                                    .addComponent(jtfMarcaV, javax.swing.GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
                                    .addGap(23, 23, 23)
                                    .addComponent(jlPrecio)
                                    .addGap(12, 12, 12)
                                    .addComponent(jtfPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jtfSedeV, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpDatosVehiculoLayout.setVerticalGroup(
            jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlChasisV)
                    .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jlPlacaV)
                        .addComponent(jtfPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jtfChasisV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(23, 23, 23)
                .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jtfColorV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jlModeloV)
                        .addComponent(jtfModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jlColorV))
                .addGap(17, 17, 17)
                .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jlMarcaV))
                    .addComponent(jtfMarcaV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jlPrecio))
                    .addGroup(jpDatosVehiculoLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jtfPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addGroup(jpDatosVehiculoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlSedeV, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfSedeV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(jpboton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jlValidacion.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jpPanelLayout = new javax.swing.GroupLayout(jpPanel);
        jpPanel.setLayout(jpPanelLayout);
        jpPanelLayout.setHorizontalGroup(
            jpPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jlRegistrar)
                .addGap(308, 308, 308))
            .addGroup(jpPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jpDatosVehiculo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jpPanelLayout.createSequentialGroup()
                        .addGroup(jpPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpPanelLayout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addGroup(jpPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpPanelLayout.createSequentialGroup()
                                        .addGap(464, 464, 464)
                                        .addComponent(jlBotonAtras))))
                            .addGroup(jpPanelLayout.createSequentialGroup()
                                .addComponent(jpDatosVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jpDatosComprador, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlValidacion, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(185, 185, 185))

        );
        jpPanelLayout.setVerticalGroup(
            jpPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpPanelLayout.createSequentialGroup()
                .addGroup(jpPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpPanelLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jlBotonAtras, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)))
                .addGroup(jpPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1)
                    .addComponent(jpDatosVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jpDatosComprador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpDatosVehiculo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlValidacion, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlRegistrar)
                .addGap(39, 39, 39))
        );

        jLabel1.getAccessibleContext().setAccessibleName("Mediante el presente CONTRATO el VENDEDOR transfiere a titulo de venta y el COMPRADOR adquiere la propiedad \\n  \ndel vehiculo Automotor que a continuacion se describe: ");

        getContentPane().add(jpPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlBotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonAtrasMouseClicked

    private void jtfTelefonoCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfTelefonoCActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfTelefonoCActionPerformed

    private void jtfIdentificacionCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfIdentificacionCActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfIdentificacionCActionPerformed

    private void jtfIdentificacionVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfIdentificacionVActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfIdentificacionVActionPerformed

    private void jpboton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jpboton2MouseClicked
        BuscarVehiculo buscarV = new BuscarVehiculo(this, "ESPECIFICO");
        buscarV.setVisible(true);
    }//GEN-LAST:event_jpboton2MouseClicked

    private void jlRegistrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRegistrarMouseClicked

        String cedula = jtfIdentificacionV.getText().trim();
        String chasis = jtfChasisV.getText().trim();
        String pago = "EFECTIVO";
        String cedula_comprador = jtfIdentificacionC.getText().trim();
        String nombre_comprador = jtfNombreC.getText().trim().toUpperCase();
        String direccion_comprador = jtfDirV.getText().trim().toUpperCase();
        String telefono_comprador = jtfTelefonoC.getText().trim();
        String correo_comprador = jtfEmail.getText().trim().toUpperCase();
        String placa = jtfPlaca.getText().trim();

        if (jlValidacion.getText().length() != 0) {
            return;
        }
        if (cedula.length() == 0
                || cedula_comprador.length() == 0
                || nombre_comprador.length() == 0
                || direccion_comprador.length() == 0
                || telefono_comprador.length() == 0
                || placa.length() == 0
                || chasis.length() == 0) {
            JOptionPane.showMessageDialog(this, "Por favor complete todos los campos", "Registro de venta", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        String mensaje = new ManejoDatosVenta().RealizarRegistroVenta(cedula, chasis, pago, cedula_comprador, nombre_comprador,
                direccion_comprador, telefono_comprador,correo_comprador, placa);
        JOptionPane.showMessageDialog(this, mensaje, "Registro de venta", JOptionPane.INFORMATION_MESSAGE);
        padre.setVisible(true);

        new Reportes.FacturaVenta(cedula_comprador, chasis);

        this.dispose();

    }//GEN-LAST:event_jlRegistrarMouseClicked

    public void CargarDatosVehiculo(String chasis, String modelo, String marca, String color, String sede, String precio) {
        this.jtfChasisV.setText(chasis);
        this.jtfModelo.setText(modelo);
        this.jtfMarcaV.setText(marca);
        this.jtfColorV.setText(color);
        this.jtfSedeV.setText(sede);
        this.jtfPrecio.setText(precio);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jDirC;
    private javax.swing.JLabel jDirC1;
    private javax.swing.JLabel jIdentifiacionC;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jbSeleccionar;
    private javax.swing.JLabel jlBotonAtras;
    private javax.swing.JLabel jlChasisV;
    private javax.swing.JLabel jlColorV;
    private javax.swing.JLabel jlIdentificacionV;
    private javax.swing.JLabel jlMarcaV;
    private javax.swing.JLabel jlModeloV;
    private javax.swing.JLabel jlNombreC;
    private javax.swing.JLabel jlPlacaV;
    private javax.swing.JLabel jlPrecio;
    private javax.swing.JLabel jlRegistrar;
    private javax.swing.JLabel jlSedeV;
    private javax.swing.JLabel jlTelefonoC;
    private javax.swing.JLabel jlValidacion;
    private javax.swing.JPanel jpDatosComprador;
    private javax.swing.JPanel jpDatosVehiculo;
    private javax.swing.JPanel jpDatosVendedor;
    private javax.swing.JPanel jpPanel;
    private javax.swing.JPanel jpboton2;
    private javax.swing.JTextField jtfChasisV;
    private javax.swing.JTextField jtfColorV;
    private javax.swing.JTextField jtfDirV;
    private javax.swing.JTextField jtfEmail;
    private javax.swing.JTextField jtfIdentificacionC;
    private javax.swing.JTextField jtfIdentificacionV;
    private javax.swing.JTextField jtfMarcaV;
    private javax.swing.JTextField jtfModelo;
    private javax.swing.JTextField jtfNombreC;
    private javax.swing.JFormattedTextField jtfPlaca;
    private javax.swing.JTextField jtfPrecio;
    private javax.swing.JTextField jtfSedeV;
    private javax.swing.JTextField jtfTelefonoC;
    // End of variables declaration//GEN-END:variables
}
