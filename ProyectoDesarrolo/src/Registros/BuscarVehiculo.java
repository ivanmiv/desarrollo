package Registros;

import Datos.ManejoDatosVehiculo;
import javax.swing.JFrame;

public class BuscarVehiculo extends javax.swing.JFrame {

    ManejoDatosVehiculo manejodatos;
    String tipo;
    JFrame padre;

    public BuscarVehiculo(JFrame padre, String tipo) {

        initComponents();
        this.tipo = tipo;
        this.padre = padre;

        manejodatos = new ManejoDatosVehiculo();
        switch (tipo) {
            case "ESPECIFICO":
                manejodatos.ObtenerDatosVehiculosDisponibles(jTableVehiculo);
                break;
            case "GENERAL":
                manejodatos.ObtenerDatosModelosDisponibles(jTableVehiculo);
                break;
            default:
                ;
        }

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableVehiculo = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jCombofiltro = new javax.swing.JComboBox();
        jtfValorBusqueda = new javax.swing.JTextField();
        jlBotonAplicar = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jbSeleccionar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(720, 585));

        jPanel3.setBackground(new java.awt.Color(254, 254, 254));
        jPanel3.setMinimumSize(new java.awt.Dimension(687, 569));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jTableVehiculo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Chasis", "Marca", "Modelo", "Placa", "Cod_sede", "Color", "Precio", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Double.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTableVehiculo);

        jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel2.setBackground(new java.awt.Color(254, 254, 254));

        jLabel1.setText("Valor filtro:");
        jPanel2.add(jLabel1);

        jCombofiltro.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Filtrar", "Marca", "Modelo" }));
        jPanel2.add(jCombofiltro);

        jtfValorBusqueda.setColumns(25);
        jtfValorBusqueda.setMinimumSize(new java.awt.Dimension(100, 128));
        jtfValorBusqueda.setName(""); // NOI18N
        jtfValorBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfValorBusquedaActionPerformed(evt);
            }
        });
        jPanel2.add(jtfValorBusqueda);

        jlBotonAplicar.setForeground(new java.awt.Color(254, 254, 254));
        jlBotonAplicar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonConsultar.jpg"))); // NOI18N
        jlBotonAplicar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAplicarMouseClicked(evt);
            }
        });
        jPanel2.add(jlBotonAplicar);

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel4.setMaximumSize(new java.awt.Dimension(129, 42));
        jPanel4.setMinimumSize(new java.awt.Dimension(129, 42));

        jbSeleccionar.setText("Seleccionar");
        jbSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSeleccionarActionPerformed(evt);
            }
        });
        jPanel4.add(jbSeleccionar);

        jPanel1.add(jPanel4, java.awt.BorderLayout.SOUTH);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 720, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 663, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 585, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtfValorBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfValorBusquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfValorBusquedaActionPerformed

    private void jlBotonAplicarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAplicarMouseClicked
        String valorcombofiltro = (String) jCombofiltro.getSelectedItem();
        String valorbusqueda = jtfValorBusqueda.getText();

        if (!valorcombofiltro.equals("Filtrar") && valorbusqueda.length() != 0) {
            manejodatos.ObtenerDatosVehiculosFiltrados(jTableVehiculo, valorcombofiltro, valorbusqueda);

            switch (tipo) {
                case "ESPECIFICO":
                    manejodatos.ObtenerDatosVehiculosFiltrados(jTableVehiculo, valorcombofiltro, valorbusqueda);
                    break;
                case "GENERAL":
                    manejodatos.ObtenerDatosModelosFiltrados(jTableVehiculo, valorcombofiltro, valorbusqueda);
                    break;
                default:
                ;
            }

        } else if (valorbusqueda.length() == 0) {
            manejodatos.ObtenerDatosVehiculosDisponibles(jTableVehiculo);
        }

    }//GEN-LAST:event_jlBotonAplicarMouseClicked

    private void jbSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbSeleccionarActionPerformed

        switch (tipo) {
            case "ESPECIFICO":
                RegistrarVenta venta = (RegistrarVenta) padre;

                int row = jTableVehiculo.getSelectedRow();

                if (row == -1) {
                    return;
                }
                String chasis = (String) jTableVehiculo.getModel().getValueAt(row, 0);
                String modelo = (String) jTableVehiculo.getModel().getValueAt(row, 1);
                String precio = (String) jTableVehiculo.getModel().getValueAt(row, 2);
                String color = (String) jTableVehiculo.getModel().getValueAt(row, 3);
                String marca = (String) jTableVehiculo.getModel().getValueAt(row, 4);
                String sede = (String) jTableVehiculo.getModel().getValueAt(row, 5);

                venta.CargarDatosVehiculo(chasis, modelo, marca, color, sede, precio);

                break;
            case "GENERAL":
                RegistrarCotizacion cotizacion = (RegistrarCotizacion) padre;
                int rowG = jTableVehiculo.getSelectedRow();

                if (rowG == -1) {
                    return;
                }
                
                
                String modeloG = (String) jTableVehiculo.getModel().getValueAt(rowG, 0);
                String precioG = (String) jTableVehiculo.getModel().getValueAt(rowG, 3);
                String colorG = (String) jTableVehiculo.getModel().getValueAt(rowG, 2);
                String marcaG = (String) jTableVehiculo.getModel().getValueAt(rowG, 1);
                String sedeG = (String) jTableVehiculo.getModel().getValueAt(rowG, 4);
                
                cotizacion.CargarDatosVehiculo(modeloG, marcaG, colorG, sedeG, precioG);
                
                break;
            default:
                ;
        }

        this.dispose();
    }//GEN-LAST:event_jbSeleccionarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox jCombofiltro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableVehiculo;
    private javax.swing.JButton jbSeleccionar;
    private javax.swing.JLabel jlBotonAplicar;
    private javax.swing.JTextField jtfValorBusqueda;
    // End of variables declaration//GEN-END:variables
}
