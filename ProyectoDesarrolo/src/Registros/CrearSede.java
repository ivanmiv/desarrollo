package Registros;

import Datos.ManejoDatosEmpleado;
import Datos.ManejoDatosSede;
import Datos.ValidarCantidadCaracteres;
import Datos.ValidarNumero;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class CrearSede extends javax.swing.JFrame {

    JFrame padre;

    public CrearSede(JFrame padre) {
        this.padre = padre;
        initComponents();
        setLocationRelativeTo(null);
        new ManejoDatosEmpleado().ColocarGerentesLibres(jcbGerente);
    
    jtfCiudadSede.setInputVerifier(new ValidarCantidadCaracteres(jlValidacion, 100));
    jtfDireccionSede.setInputVerifier(new ValidarCantidadCaracteres(jlValidacion, 100));
    jtfNombreSede.setInputVerifier(new ValidarCantidadCaracteres(jlValidacion, 100));
    jtfTelefonoSede.setInputVerifier(new ValidarNumero(jlValidacion, 10));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCrearSede = new javax.swing.JPanel();
        jtfNombreSede = new javax.swing.JTextField();
        jlIconSede = new javax.swing.JLabel();
        jlnombreSede = new javax.swing.JLabel();
        jlCedulaGerente = new javax.swing.JLabel();
        jlCiudadSede = new javax.swing.JLabel();
        jlTelefonoSede = new javax.swing.JLabel();
        jlDirSede = new javax.swing.JLabel();
        jlBotonCrear = new javax.swing.JLabel();
        jlBotonAtras = new javax.swing.JLabel();
        jtfDireccionSede = new javax.swing.JTextField();
        jtfCiudadSede = new javax.swing.JTextField();
        jtfTelefonoSede = new javax.swing.JTextField();
        jcbGerente = new javax.swing.JComboBox();
        jlValidacion = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanelCrearSede.setBackground(new java.awt.Color(255, 255, 255));

        jlIconSede.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/SedeIcon.png"))); // NOI18N

        jlnombreSede.setText("Nombre:");

        jlCedulaGerente.setText("Cédula Gerente:");

        jlCiudadSede.setText("Ciudad:");

        jlTelefonoSede.setText("Telèfono:");

        jlDirSede.setText("Direcciòn:");

        jlBotonCrear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/button crear.png"))); // NOI18N
        jlBotonCrear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonCrearMouseClicked(evt);
            }
        });

        jlBotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlBotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAtrasMouseClicked(evt);
            }
        });

        jcbGerente.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione" }));

        jlValidacion.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanelCrearSedeLayout = new javax.swing.GroupLayout(jPanelCrearSede);
        jPanelCrearSede.setLayout(jPanelCrearSedeLayout);
        jPanelCrearSedeLayout.setHorizontalGroup(
            jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                .addGap(220, 220, 220)
                .addComponent(jlBotonCrear)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                        .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlDirSede)
                            .addComponent(jlCedulaGerente)
                            .addComponent(jlCiudadSede)
                            .addComponent(jlnombreSede)
                            .addComponent(jlTelefonoSede))
                        .addGap(23, 23, 23)
                        .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jtfCiudadSede, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jtfTelefonoSede, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jtfDireccionSede, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jtfNombreSede, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jcbGerente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(jlIconSede)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 98, Short.MAX_VALUE)
                        .addComponent(jlBotonAtras)
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearSedeLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlValidacion)
                .addGap(221, 221, 221))
        );
        jPanelCrearSedeLayout.setVerticalGroup(
            jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jlIconSede))
                    .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jlBotonAtras)))
                .addGap(36, 36, 36)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlnombreSede)
                    .addComponent(jtfNombreSede, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlTelefonoSede, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfTelefonoSede, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfDireccionSede, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlDirSede))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlCedulaGerente)
                    .addComponent(jcbGerente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCiudadSede)
                    .addComponent(jtfCiudadSede, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(jlValidacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlBotonCrear, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        getContentPane().add(jPanelCrearSede, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlBotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonAtrasMouseClicked

    private void jlBotonCrearMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonCrearMouseClicked
        String cedulaGerente = (String) jcbGerente.getSelectedItem();
        String ciudad = jtfCiudadSede.getText().trim();
        String direccion = jtfDireccionSede.getText().trim();
        String nombreSede = jtfNombreSede.getText().trim();
        String telefono = jtfTelefonoSede.getText().trim();
        
        if(jlValidacion.getText().length()!=0){
            return;
        }
        if (cedulaGerente.length() == 0
            || ciudad.length() == 0
            || telefono.length() == 0
            || direccion.length() == 0
            ) {
            JOptionPane.showMessageDialog(this, "Por favor complete todos los campos", "Cotizacion de vehiculo", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        
        if (!cedulaGerente.equals("Seleccione")) {
            String mensaje = new ManejoDatosSede().RealizarRegistroSede(cedulaGerente, ciudad, direccion, nombreSede, telefono);
            JOptionPane.showMessageDialog(this, mensaje, "Registro de empleado", JOptionPane.INFORMATION_MESSAGE);
            padre.setVisible(true);
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "No ha seleccionado una cedula de gerente", "Registro de empleado", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_jlBotonCrearMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelCrearSede;
    private javax.swing.JComboBox jcbGerente;
    private javax.swing.JLabel jlBotonAtras;
    private javax.swing.JLabel jlBotonCrear;
    private javax.swing.JLabel jlCedulaGerente;
    private javax.swing.JLabel jlCiudadSede;
    private javax.swing.JLabel jlDirSede;
    private javax.swing.JLabel jlIconSede;
    private javax.swing.JLabel jlTelefonoSede;
    private javax.swing.JLabel jlValidacion;
    private javax.swing.JLabel jlnombreSede;
    private javax.swing.JTextField jtfCiudadSede;
    private javax.swing.JTextField jtfDireccionSede;
    private javax.swing.JTextField jtfNombreSede;
    private javax.swing.JTextField jtfTelefonoSede;
    // End of variables declaration//GEN-END:variables
}
