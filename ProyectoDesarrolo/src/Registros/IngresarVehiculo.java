package Registros;

import Datos.ManejoDatosSede;
import Datos.ManejoDatosVehiculo;
import Datos.ValidarCantidadCaracteres;
import Datos.ValidarNumero;
import javax.swing.JFrame;
import javax.swing.JOptionPane;




public class IngresarVehiculo extends javax.swing.JFrame {

    JFrame padre;
    public IngresarVehiculo(JFrame padre) {
        
        this.padre=padre;
        initComponents();
        new ManejoDatosSede().CargarDatosComboBox(jcbSedeV, "sedes");
        setLocationRelativeTo(null);
        
        jtfPrecio.setInputVerifier(new ValidarNumero(jlValidacion, 100));
        
        jtfModelo.setInputVerifier(new ValidarCantidadCaracteres(jlValidacion, 4));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCrearUser = new javax.swing.JPanel();
        jtfModelo = new javax.swing.JTextField();
        jlIconVehiculos = new javax.swing.JLabel();
        jlChasisV = new javax.swing.JLabel();
        jlModeloV = new javax.swing.JLabel();
        jlColorV = new javax.swing.JLabel();
        jlMarcaV = new javax.swing.JLabel();
        jlSedeV = new javax.swing.JLabel();
        jlPlacaV = new javax.swing.JLabel();
        jlEstadoV = new javax.swing.JLabel();
        jlBotonResgistrarS = new javax.swing.JLabel();
        jlAtras = new javax.swing.JLabel();
        jtfPrecio = new javax.swing.JTextField();
        jtfPlacaV = new javax.swing.JTextField();
        jtfChasisV = new javax.swing.JTextField();
        jcbMarca = new javax.swing.JComboBox();
        jcbColorV = new javax.swing.JComboBox();
        jlPrecio = new javax.swing.JLabel();
        jcbSedeV = new javax.swing.JComboBox();
        jcbEstadoV = new javax.swing.JComboBox();
        jlValidacion = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelCrearUser.setBackground(new java.awt.Color(255, 255, 255));

        jlIconVehiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/iconVehiculos.jpg"))); // NOI18N

        jlChasisV.setText("Chasis:");

        jlModeloV.setText("Modelo:");

        jlColorV.setText("Color:");

        jlMarcaV.setText("Marca:");

        jlSedeV.setText("Sede:");

        jlPlacaV.setText("Placa:");

        jlEstadoV.setText("Estado:");

        jlBotonResgistrarS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/registrar.jpg"))); // NOI18N
        jlBotonResgistrarS.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonResgistrarSMouseClicked(evt);
            }
        });

        jlAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlAtrasMouseClicked(evt);
            }
        });

        jtfPlacaV.setToolTipText("");

        jcbMarca.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "audi", "bmw", "chevrolet", "citroen", "ferrari", "ford", "honda", "hyundai", "jaguar", "jeep", "kia", "lamborghini", "maserati", "mazda", "mercedes", "mitsubishi", "nissan", "peugeot", "porsche", "renault", "skoda", "ssangyong", "suzuki", "tata", "toyota", "volkswagen", "volvo" }));

        jcbColorV.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "blanco", "rojo", "negro", "gris", "azul", "amarillo", "verde" }));

        jlPrecio.setText("precio:");

        jcbSedeV.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione" }));

        jcbEstadoV.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "DISPONIBLE", "VENDIDO" }));

        jlValidacion.setForeground(new java.awt.Color(204, 0, 0));

        javax.swing.GroupLayout jPanelCrearUserLayout = new javax.swing.GroupLayout(jPanelCrearUser);
        jPanelCrearUser.setLayout(jPanelCrearUserLayout);
        jPanelCrearUserLayout.setHorizontalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlPlacaV)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jtfPlacaV, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlColorV)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jcbColorV, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlModeloV)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                        .addComponent(jtfModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlMarcaV)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jcbMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlChasisV)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jtfChasisV, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlEstadoV)
                            .addComponent(jlSedeV))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jcbSedeV, 0, 275, Short.MAX_VALUE)
                            .addComponent(jcbEstadoV, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlPrecio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jtfPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(112, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconVehiculos)
                        .addGap(85, 85, 85)
                        .addComponent(jlAtras)
                        .addGap(43, 43, 43))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlValidacion)
                        .addGap(232, 232, 232))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlBotonResgistrarS)
                        .addGap(195, 195, 195))))
        );
        jPanelCrearUserLayout.setVerticalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlAtras)
                        .addGap(127, 127, 127))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconVehiculos)
                        .addGap(40, 40, 40)))
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlChasisV)
                    .addComponent(jtfChasisV, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlModeloV)
                    .addComponent(jtfModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfPlacaV, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlPlacaV))
                .addGap(34, 34, 34)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlMarcaV))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlColorV)
                    .addComponent(jcbColorV, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlSedeV, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbSedeV, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlEstadoV)
                    .addComponent(jcbEstadoV, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlPrecio))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlValidacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addComponent(jlBotonResgistrarS)
                .addContainerGap())
        );

        getContentPane().add(jPanelCrearUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlAtrasMouseClicked
      this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlAtrasMouseClicked

    private void jlBotonResgistrarSMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonResgistrarSMouseClicked
        String nombresede = (String)jcbSedeV.getSelectedItem();
        
        String chasis = jtfChasisV.getText().trim();
        String modelo = jtfModelo.getText().trim();
        String placa = jtfPlacaV.getText().trim();
        String marca = (String)jcbMarca.getSelectedItem();
        String color = (String)jcbColorV.getSelectedItem();      
        String estado = (String)jcbEstadoV.getSelectedItem();
               
        if(jlValidacion.getText().length()!=0){
            return;
        }
        if (chasis.length() == 0
            || modelo.length() == 0
            || jtfPrecio.getText().trim().length() == 0
            || marca.length() == 0
            || nombresede.equalsIgnoreCase("Seleccione")    
                ) {
            JOptionPane.showMessageDialog(this, "Por favor complete  o verifique todos los campos", "Ingreso de vehiculo", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        else{         
            int codsede = new ManejoDatosSede().TraerCodigoSedeNombre(nombresede.trim());
            int Precio = Integer.parseInt(jtfPrecio.getText().trim());
       
            String mensaje= new ManejoDatosVehiculo().RealizarRegistroVehiculo(chasis, modelo, placa, marca, color, codsede, estado, Precio);
            JOptionPane.showMessageDialog(this, mensaje, "Registro de vehiculo", JOptionPane.INFORMATION_MESSAGE);
            this.padre.setVisible(true);
            this.dispose();       
        }
        
        
         
    }//GEN-LAST:event_jlBotonResgistrarSMouseClicked

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelCrearUser;
    private javax.swing.JComboBox jcbColorV;
    private javax.swing.JComboBox jcbEstadoV;
    private javax.swing.JComboBox jcbMarca;
    private javax.swing.JComboBox jcbSedeV;
    private javax.swing.JLabel jlAtras;
    private javax.swing.JLabel jlBotonResgistrarS;
    private javax.swing.JLabel jlChasisV;
    private javax.swing.JLabel jlColorV;
    private javax.swing.JLabel jlEstadoV;
    private javax.swing.JLabel jlIconVehiculos;
    private javax.swing.JLabel jlMarcaV;
    private javax.swing.JLabel jlModeloV;
    private javax.swing.JLabel jlPlacaV;
    private javax.swing.JLabel jlPrecio;
    private javax.swing.JLabel jlSedeV;
    private javax.swing.JLabel jlValidacion;
    private javax.swing.JTextField jtfChasisV;
    private javax.swing.JTextField jtfModelo;
    private javax.swing.JTextField jtfPlacaV;
    private javax.swing.JTextField jtfPrecio;
    // End of variables declaration//GEN-END:variables
}
