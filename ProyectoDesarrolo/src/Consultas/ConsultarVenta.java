package Consultas;


import Datos.ManejoDatosVenta;
import javax.swing.JFrame;


public class ConsultarVenta extends javax.swing.JFrame {
    JFrame padre;
   
    public ConsultarVenta(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        new ManejoDatosVenta().ObtenerDatosVentas(jtableConsultarVenta);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpConsultarVenta = new javax.swing.JPanel();
        jlbotonConsultar = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtableConsultarVenta = new javax.swing.JTable();
        jlIconVenta = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(791, 679));
        setResizable(false);

        jpConsultarVenta.setBackground(new java.awt.Color(255, 255, 255));
        jpConsultarVenta.setLayout(null);

        jlbotonConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonConsultar.jpg"))); // NOI18N
        jlbotonConsultar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonConsultarMouseClicked(evt);
            }
        });
        jpConsultarVenta.add(jlbotonConsultar);
        jlbotonConsultar.setBounds(340, 630, 115, 38);

        jtableConsultarVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Codigo_Venta", "Precio", "Chasis"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jtableConsultarVenta.setGridColor(new java.awt.Color(255, 255, 255));
        jtableConsultarVenta.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtableConsultarVenta);

        jpConsultarVenta.add(jScrollPane1);
        jScrollPane1.setBounds(30, 150, 740, 437);

        jlIconVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/IconVentas.jpg"))); // NOI18N
        jpConsultarVenta.add(jlIconVenta);
        jlIconVenta.setBounds(350, 30, 90, 90);

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });
        jpConsultarVenta.add(jlbotonAtras);
        jlbotonAtras.setBounds(670, 20, 98, 40);

        getContentPane().add(jpConsultarVenta, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos de Botones
    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);        
        this.dispose();        
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlbotonConsultarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonConsultarMouseClicked
        int row=jtableConsultarVenta.getSelectedRow();
        if (row == -1) return;
        String chasis = (String) jtableConsultarVenta.getModel().getValueAt(row, 1);
        String cedula_comprador = (String) jtableConsultarVenta.getModel().getValueAt(row, 3);
        new Reportes.FacturaVenta(cedula_comprador, chasis);
    }//GEN-LAST:event_jlbotonConsultarMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jlIconVenta;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlbotonConsultar;
    private javax.swing.JPanel jpConsultarVenta;
    private javax.swing.JTable jtableConsultarVenta;
    // End of variables declaration//GEN-END:variables
}
