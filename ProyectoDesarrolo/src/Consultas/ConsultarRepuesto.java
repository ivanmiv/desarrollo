package Consultas;

import javax.swing.*;
import Datos.ManejoDatos;
import Datos.ManejoDatosRepuesto;
import javax.swing.JFrame;


public class ConsultarRepuesto extends javax.swing.JFrame {

    JFrame padre;
    public ConsultarRepuesto(JFrame padre) {
        this.padre = padre;
        initComponents();
        setLocationRelativeTo(null);
        new ManejoDatosRepuesto().ObtenerDatosRepuestos( jtableMostrarRepuestos);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtableMostrarRepuestos = new javax.swing.JTable();
        jlIconRepuesto = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();
        jlbotonConsultar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jtableMostrarRepuestos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jtableMostrarRepuestos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Codigo", "Nombre", "Precio"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jtableMostrarRepuestos.setGridColor(new java.awt.Color(255, 255, 255));
        jtableMostrarRepuestos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtableMostrarRepuestos);

        jlIconRepuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/IconRepuestos.jpg"))); // NOI18N

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });

        jlbotonConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonConsultar.jpg"))); // NOI18N
        jlbotonConsultar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonConsultarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(182, 182, 182)
                .addComponent(jlIconRepuesto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlbotonAtras)
                .addGap(18, 18, 18))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(190, 190, 190)
                .addComponent(jlbotonConsultar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlIconRepuesto)
                    .addComponent(jlbotonAtras))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jlbotonConsultar)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos de Botones
    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
     this.setVisible(false);
     padre.setVisible(true);
     this.dispose();   
                
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlbotonConsultarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonConsultarMouseClicked
       this.setVisible(false);
        int row=jtableMostrarRepuestos.getSelectedRow();
        String codigo = (String)jtableMostrarRepuestos.getModel().getValueAt(row, 0);
        ResultadoConsultaRepuesto u = new ManejoDatosRepuesto().CargarDatosRepuestoAConsultar(this,codigo);
        u.setVisible(true);
    }//GEN-LAST:event_jlbotonConsultarMouseClicked

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jlIconRepuesto;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlbotonConsultar;
    private javax.swing.JTable jtableMostrarRepuestos;
    // End of variables declaration//GEN-END:variables
}
