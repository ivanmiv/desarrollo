package Consultas;

import javax.swing.JFrame;

public class ResultadoConsultaRepuesto extends javax.swing.JFrame {

    JFrame padre;

    public ResultadoConsultaRepuesto(JFrame padre, int cod_repuesto, String nombre,
            double precio, String cantidad, String descripcion, String sede) {
        this.padre = padre;
        initComponents();
        setLocationRelativeTo(null);
        this.jtfCodigoR.setText(Integer.toString(cod_repuesto));
        this.jtfNombreR.setText(nombre);
        this.jtfPrecioR.setText(Double.toString(precio));
        this.jtfCantidadR.setText(cantidad);
        this.jtaDescripcionR.setText(descripcion);
        this.jtfSedeR.setText(sede);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCrearUser = new javax.swing.JPanel();
        jtfCantidadR = new javax.swing.JTextField();
        jlIconRepuestos = new javax.swing.JLabel();
        jlCodigoR = new javax.swing.JLabel();
        jlCantidadR = new javax.swing.JLabel();
        jlSedeR = new javax.swing.JLabel();
        jlDescripcion = new javax.swing.JLabel();
        jlPrecio = new javax.swing.JLabel();
        jlBotonResgistrarS = new javax.swing.JLabel();
        jlAtras = new javax.swing.JLabel();
        jtfPrecioR = new javax.swing.JTextField();
        jtfCodigoR = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtaDescripcionR = new javax.swing.JTextArea();
        jtfSedeR = new javax.swing.JTextField();
        jlNombreR = new javax.swing.JLabel();
        jtfNombreR = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelCrearUser.setBackground(new java.awt.Color(255, 255, 255));

        jtfCantidadR.setEditable(false);

        jlIconRepuestos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/IconRepuestos.jpg"))); // NOI18N

        jlCodigoR.setText("Código:");

        jlCantidadR.setText("Cantidad:");

        jlSedeR.setText("Sede:");

        jlDescripcion.setText("Descripción:");

        jlPrecio.setText("Precio:");

        jlBotonResgistrarS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/boton aceptar.jpg"))); // NOI18N

        jlAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlAtrasMouseClicked(evt);
            }
        });

        jtfPrecioR.setEditable(false);

        jtfCodigoR.setEditable(false);

        jtaDescripcionR.setEditable(false);
        jtaDescripcionR.setColumns(20);
        jtaDescripcionR.setLineWrap(true);
        jtaDescripcionR.setRows(5);
        jScrollPane1.setViewportView(jtaDescripcionR);

        jtfSedeR.setEditable(false);

        jlNombreR.setText("Nombre:");

        jtfNombreR.setEditable(false);

        javax.swing.GroupLayout jPanelCrearUserLayout = new javax.swing.GroupLayout(jPanelCrearUser);
        jPanelCrearUser.setLayout(jPanelCrearUserLayout);
        jPanelCrearUserLayout.setHorizontalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                .addContainerGap(239, Short.MAX_VALUE)
                .addComponent(jlIconRepuestos)
                .addGap(58, 58, 58)
                .addComponent(jlAtras)
                .addGap(43, 43, 43))
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addComponent(jlBotonResgistrarS))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                .addGap(67, 67, 67)
                                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                                            .addComponent(jlSedeR)
                                            .addGap(27, 27, 27))
                                        .addComponent(jlCantidadR)
                                        .addComponent(jlNombreR))
                                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                        .addComponent(jlCodigoR)
                                        .addGap(13, 13, 13))
                                    .addComponent(jlPrecio, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(32, 32, 32))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jlDescripcion)
                                .addGap(18, 18, 18)))
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jtfCodigoR, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jtfNombreR, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jtfPrecioR, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jtfCantidadR, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jtfSedeR, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                            .addComponent(jScrollPane1))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanelCrearUserLayout.setVerticalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jlAtras))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jlIconRepuestos)))
                .addGap(44, 44, 44)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jtfCodigoR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtfNombreR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlNombreR))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtfCantidadR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlCantidadR))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtfPrecioR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlPrecio))
                        .addGap(19, 19, 19)
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtfSedeR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlSedeR)))
                    .addComponent(jlCodigoR))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlBotonResgistrarS)
                .addContainerGap())
        );

        getContentPane().add(jPanelCrearUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
    }//GEN-LAST:event_jlAtrasMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelCrearUser;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jlAtras;
    private javax.swing.JLabel jlBotonResgistrarS;
    private javax.swing.JLabel jlCantidadR;
    private javax.swing.JLabel jlCodigoR;
    private javax.swing.JLabel jlDescripcion;
    private javax.swing.JLabel jlIconRepuestos;
    private javax.swing.JLabel jlNombreR;
    private javax.swing.JLabel jlPrecio;
    private javax.swing.JLabel jlSedeR;
    private javax.swing.JTextArea jtaDescripcionR;
    private javax.swing.JTextField jtfCantidadR;
    private javax.swing.JTextField jtfCodigoR;
    private javax.swing.JTextField jtfNombreR;
    private javax.swing.JTextField jtfPrecioR;
    private javax.swing.JTextField jtfSedeR;
    // End of variables declaration//GEN-END:variables
}
