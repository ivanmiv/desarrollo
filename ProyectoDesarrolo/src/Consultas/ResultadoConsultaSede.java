package Consultas;


import javax.swing.JFrame;
import javax.swing.JOptionPane;




public class ResultadoConsultaSede extends javax.swing.JFrame {

    JFrame padre;
    
    public ResultadoConsultaSede(JFrame padre, String nombre, String telefono, String direccion,
        String ciudad, String cedula_gerente) {
        this.padre=padre;
        initComponents();
      //  JOptionPane.showMessageDialog(null, telefono);
        setLocationRelativeTo(null);
        this.jtfNombreSede.setText(nombre);
        this.jtftelefono.setText(telefono);
        this.jtfDireccionSede.setText(direccion);
        this.jtfCiudadSede.setText(ciudad);
        this.jtfCedulaGerente.setText(cedula_gerente);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCrearSede = new javax.swing.JPanel();
        jtfNombreSede = new javax.swing.JTextField();
        jlIconSede = new javax.swing.JLabel();
        jlnombreSede = new javax.swing.JLabel();
        jlCedulaGerente = new javax.swing.JLabel();
        jlCiudadSede = new javax.swing.JLabel();
        jlTelefonoSede = new javax.swing.JLabel();
        jlDirSede = new javax.swing.JLabel();
        jlBotonAceptar = new javax.swing.JLabel();
        jlBotonAtras = new javax.swing.JLabel();
        jtfDireccionSede = new javax.swing.JTextField();
        jtfCiudadSede = new javax.swing.JTextField();
        jtfCedulaGerente = new javax.swing.JTextField();
        jtftelefono = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanelCrearSede.setBackground(new java.awt.Color(255, 255, 255));

        jtfNombreSede.setEditable(false);

        jlIconSede.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/SedeIcon.png"))); // NOI18N

        jlnombreSede.setText("Nombre:");

        jlCedulaGerente.setText("Cèdula Gerente:");

        jlCiudadSede.setText("Ciudad:");

        jlTelefonoSede.setText("Telefono:");

        jlDirSede.setText("Direcciòn:");

        jlBotonAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/boton aceptar.jpg"))); // NOI18N
        jlBotonAceptar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAceptarMouseClicked(evt);
            }
        });

        jlBotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlBotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAtrasMouseClicked(evt);
            }
        });

        jtfDireccionSede.setEditable(false);

        jtfCiudadSede.setEditable(false);

        jtfCedulaGerente.setEditable(false);

        jtftelefono.setEditable(false);

        javax.swing.GroupLayout jPanelCrearSedeLayout = new javax.swing.GroupLayout(jPanelCrearSede);
        jPanelCrearSede.setLayout(jPanelCrearSedeLayout);
        jPanelCrearSedeLayout.setHorizontalGroup(
            jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jlIconSede)
                        .addGap(59, 59, 59)
                        .addComponent(jlBotonAtras)
                        .addGap(19, 19, 19))
                    .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                        .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlCedulaGerente)
                            .addComponent(jlCiudadSede)
                            .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlnombreSede)
                                    .addComponent(jlTelefonoSede)))
                            .addComponent(jlDirSede))
                        .addGap(23, 23, 23)
                        .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jtfCiudadSede, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jtfNombreSede, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jtftelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jtfDireccionSede, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jtfCedulaGerente, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 82, Short.MAX_VALUE))))
            .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                .addGap(220, 220, 220)
                .addComponent(jlBotonAceptar)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanelCrearSedeLayout.setVerticalGroup(
            jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jlBotonAtras))
                    .addGroup(jPanelCrearSedeLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(jlIconSede)))
                .addGap(22, 22, 22)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfNombreSede, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlnombreSede))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtftelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlTelefonoSede, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfDireccionSede, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlDirSede))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfCedulaGerente, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlCedulaGerente))
                .addGap(26, 26, 26)
                .addGroup(jPanelCrearSedeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCiudadSede)
                    .addComponent(jtfCiudadSede, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlBotonAceptar)
                .addGap(27, 27, 27))
        );

        getContentPane().add(jPanelCrearSede, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlBotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
    }//GEN-LAST:event_jlBotonAtrasMouseClicked

    private void jlBotonAceptarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAceptarMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        
    }//GEN-LAST:event_jlBotonAceptarMouseClicked



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelCrearSede;
    private javax.swing.JLabel jlBotonAceptar;
    private javax.swing.JLabel jlBotonAtras;
    private javax.swing.JLabel jlCedulaGerente;
    private javax.swing.JLabel jlCiudadSede;
    private javax.swing.JLabel jlDirSede;
    private javax.swing.JLabel jlIconSede;
    private javax.swing.JLabel jlTelefonoSede;
    private javax.swing.JLabel jlnombreSede;
    private javax.swing.JTextField jtfCedulaGerente;
    private javax.swing.JTextField jtfCiudadSede;
    private javax.swing.JTextField jtfDireccionSede;
    private javax.swing.JTextField jtfNombreSede;
    private javax.swing.JTextField jtftelefono;
    // End of variables declaration//GEN-END:variables
}
