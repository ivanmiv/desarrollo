package Consultas;


import Datos.ManejoDatosEmpleado;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class ConsultarEmpleado extends javax.swing.JFrame {
    JFrame padre;
   
    public ConsultarEmpleado(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        new ManejoDatosEmpleado().ObtenerDatosEmpleados(jtableConsultarUser);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtableConsultarUser = new javax.swing.JTable();
        jlIconUser = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();
        jlbotonConsultar = new javax.swing.JLabel();
        jbrefrescar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Consultar Empleado");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jtableConsultarUser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nombre", "Cedula", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jtableConsultarUser.setGridColor(new java.awt.Color(255, 255, 255));
        jtableConsultarUser.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtableConsultarUser);

        jlIconUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/contact-icon.png"))); // NOI18N

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });

        jlbotonConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonConsultar.jpg"))); // NOI18N
        jlbotonConsultar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonConsultarMouseClicked(evt);
            }
        });

        jbrefrescar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/reload.png"))); // NOI18N
        jbrefrescar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbrefrescarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jbrefrescar)
                .addGap(118, 118, 118)
                .addComponent(jlIconUser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlbotonAtras)
                .addGap(18, 18, 18))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(53, 53, 53))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(190, 190, 190)
                .addComponent(jlbotonConsultar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlIconUser)
                    .addComponent(jlbotonAtras)
                    .addComponent(jbrefrescar, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jlbotonConsultar)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos de Botones
    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);        
        this.dispose();        
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlbotonConsultarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonConsultarMouseClicked
        this.setVisible(false);
        int row=jtableConsultarUser.getSelectedRow();
        String cedula = (String)jtableConsultarUser.getModel().getValueAt(row, 1);
       
        ResultadoConsultaEmpleado u = new ManejoDatosEmpleado().CargarDatosEmpleadoAConsultar(this,cedula);
        u.setVisible(true);
    }//GEN-LAST:event_jlbotonConsultarMouseClicked

    private void jbrefrescarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbrefrescarMouseClicked
        new ManejoDatosEmpleado().ObtenerDatosEmpleados(jtableConsultarUser);
    }//GEN-LAST:event_jbrefrescarMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jbrefrescar;
    private javax.swing.JLabel jlIconUser;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlbotonConsultar;
    private javax.swing.JTable jtableConsultarUser;
    // End of variables declaration//GEN-END:variables
}
