package Consultas;


import Datos.ManejoDatos;
import Datos.ManejoDatosSede;
import javax.swing.JFrame;

public class ResultadoConsultaEmpleado extends javax.swing.JFrame {

    JFrame padre;
    public ResultadoConsultaEmpleado(JFrame padre, String dia,String mes,String anho, String nombre, String cedula, 
            String cargo, String sede, String estado, String direccion, String telefono, String email,String user) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        jtfDia.setText(dia);
        jtfMes.setText(mes);
        jtfAnho.setText(anho);
        jtfNombreUser.setText(nombre);
        jtfCedulaUSer.setText(cedula);
        jtfCargoUSer.setText(cargo);
        jtfCargoUSer.setText(cargo);
        jtfSedeUsuario.setText(new ManejoDatosSede().TraerNombreSedeCodigo(Integer.parseInt(sede)));
        jtfEstadoUSer3.setText(estado);
        jtfDireccion.setText(direccion);
        jtfTelefono.setText(telefono);
        jtfEmailUSer.setText(email);
        jtfUser.setText(user);
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelResultadoConsultaUser = new javax.swing.JPanel();
        jlIconUSer = new javax.swing.JLabel();
        jlNombreUser = new javax.swing.JLabel();
        jlCedula = new javax.swing.JLabel();
        jlCargo = new javax.swing.JLabel();
        jlSede = new javax.swing.JLabel();
        jlDireccion = new javax.swing.JLabel();
        jlTelefono = new javax.swing.JLabel();
        jlAtras = new javax.swing.JLabel();
        jtfCargoUSer = new javax.swing.JTextField();
        jtfEmailUSer = new javax.swing.JTextField();
        jtfDireccion = new javax.swing.JTextField();
        jtfNombreUser = new javax.swing.JTextField();
        jlFechaRegistro = new javax.swing.JLabel();
        jlDia = new javax.swing.JLabel();
        jlMes = new javax.swing.JLabel();
        jlAno = new javax.swing.JLabel();
        jlEmail = new javax.swing.JLabel();
        jtfTelefono = new javax.swing.JTextField();
        jtfCedulaUSer = new javax.swing.JTextField();
        jtfSedeUsuario = new javax.swing.JTextField();
        jtfDia = new javax.swing.JTextField();
        jtfMes = new javax.swing.JTextField();
        jtfAnho = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jtfEstadoUSer3 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jtfUser = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanelResultadoConsultaUser.setBackground(new java.awt.Color(255, 255, 255));

        jlIconUSer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/contact-icon.png"))); // NOI18N

        jlNombreUser.setText("Nombre:");

        jlCedula.setText("Cédula:");

        jlCargo.setText("Cargo:");

        jlSede.setText("Sede:");

        jlDireccion.setText("Dirección");

        jlTelefono.setText("Teléfono:");

        jlAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlAtrasMouseClicked(evt);
            }
        });

        jtfCargoUSer.setEditable(false);

        jtfEmailUSer.setEditable(false);

        jtfDireccion.setEditable(false);

        jtfNombreUser.setEditable(false);
        jtfNombreUser.setDisabledTextColor(new java.awt.Color(1, 1, 1));

        jlFechaRegistro.setText("Fecha Registro:");

        jlDia.setText("Dia");

        jlMes.setText("Mes");

        jlAno.setText("Año");

        jlEmail.setText("Email:");

        jtfTelefono.setEditable(false);

        jtfCedulaUSer.setEditable(false);

        jtfSedeUsuario.setEditable(false);
        jtfSedeUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfSedeUsuarioActionPerformed(evt);
            }
        });

        jtfDia.setEditable(false);

        jtfMes.setEditable(false);
        jtfMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfMesActionPerformed(evt);
            }
        });

        jtfAnho.setEditable(false);

        jLabel1.setText("Estado:");

        jtfEstadoUSer3.setEditable(false);
        jtfEstadoUSer3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfEstadoUSer3ActionPerformed(evt);
            }
        });

        jLabel2.setText("Usuario:");

        jtfUser.setEditable(false);

        javax.swing.GroupLayout jPanelResultadoConsultaUserLayout = new javax.swing.GroupLayout(jPanelResultadoConsultaUser);
        jPanelResultadoConsultaUser.setLayout(jPanelResultadoConsultaUserLayout);
        jPanelResultadoConsultaUserLayout.setHorizontalGroup(
            jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelResultadoConsultaUserLayout.createSequentialGroup()
                .addContainerGap(247, Short.MAX_VALUE)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelResultadoConsultaUserLayout.createSequentialGroup()
                        .addComponent(jlIconUSer)
                        .addGap(198, 198, 198))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelResultadoConsultaUserLayout.createSequentialGroup()
                        .addComponent(jlAtras)
                        .addGap(24, 24, 24))))
            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                        .addComponent(jlFechaRegistro)
                        .addGap(64, 64, 64)
                        .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jtfDia, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addComponent(jlDia)
                                .addGap(15, 15, 15)))
                        .addGap(34, 34, 34)
                        .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jtfMes, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(jlMes)))
                        .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(jlAno))
                            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addComponent(jtfAnho, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                        .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jtfNombreUser, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addComponent(jlDireccion)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jtfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addComponent(jlTelefono)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jtfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jtfCedulaUSer, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(66, 66, 66)
                                .addComponent(jtfEstadoUSer3, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jtfCargoUSer, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addComponent(jlSede)
                                .addGap(75, 75, 75)
                                .addComponent(jtfSedeUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jlNombreUser, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlCedula, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlCargo, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlEmail)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jtfEmailUSer, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                                    .addComponent(jtfUser))))
                        .addContainerGap(106, Short.MAX_VALUE))))
        );
        jPanelResultadoConsultaUserLayout.setVerticalGroup(
            jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jlAtras)
                .addGap(2, 2, 2)
                .addComponent(jlIconUSer)
                .addGap(27, 27, 27)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                        .addComponent(jlFechaRegistro)
                        .addGap(29, 29, 29))
                    .addGroup(jPanelResultadoConsultaUserLayout.createSequentialGroup()
                        .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jlMes)
                            .addComponent(jlAno)
                            .addComponent(jlDia))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtfAnho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtfMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtfDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)))
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlNombreUser)
                    .addComponent(jtfNombreUser, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfCedulaUSer, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlCedula))
                .addGap(11, 11, 11)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfCargoUSer, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlCargo))
                .addGap(13, 13, 13)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfSedeUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlSede, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfEstadoUSer3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlDireccion))
                .addGap(16, 16, 16)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlTelefono))
                .addGap(19, 19, 19)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfEmailUSer, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlEmail))
                .addGap(18, 18, 18)
                .addGroup(jPanelResultadoConsultaUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jtfUser, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(131, 131, 131))
        );

        getContentPane().add(jPanelResultadoConsultaUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

  //Eventos Botones  
    
    private void jlAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
    }//GEN-LAST:event_jlAtrasMouseClicked

    private void jtfSedeUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfSedeUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfSedeUsuarioActionPerformed

    private void jtfMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfMesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfMesActionPerformed

    private void jtfEstadoUSer3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfEstadoUSer3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfEstadoUSer3ActionPerformed

    
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanelResultadoConsultaUser;
    private javax.swing.JLabel jlAno;
    private javax.swing.JLabel jlAtras;
    private javax.swing.JLabel jlCargo;
    private javax.swing.JLabel jlCedula;
    private javax.swing.JLabel jlDia;
    private javax.swing.JLabel jlDireccion;
    private javax.swing.JLabel jlEmail;
    private javax.swing.JLabel jlFechaRegistro;
    private javax.swing.JLabel jlIconUSer;
    private javax.swing.JLabel jlMes;
    private javax.swing.JLabel jlNombreUser;
    private javax.swing.JLabel jlSede;
    private javax.swing.JLabel jlTelefono;
    private javax.swing.JTextField jtfAnho;
    private javax.swing.JTextField jtfCargoUSer;
    private javax.swing.JTextField jtfCedulaUSer;
    private javax.swing.JTextField jtfDia;
    private javax.swing.JTextField jtfDireccion;
    private javax.swing.JTextField jtfEmailUSer;
    private javax.swing.JTextField jtfEstadoUSer3;
    private javax.swing.JTextField jtfMes;
    private javax.swing.JTextField jtfNombreUser;
    private javax.swing.JTextField jtfSedeUsuario;
    private javax.swing.JTextField jtfTelefono;
    private javax.swing.JTextField jtfUser;
    // End of variables declaration//GEN-END:variables
}
