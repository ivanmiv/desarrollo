package Consultas;

import Datos.ManejoDatosSede;
import javax.swing.JFrame;

public class ResultadoConsultaVehiculo extends javax.swing.JFrame {
    
    JFrame padre;

    public ResultadoConsultaVehiculo(JFrame padre, String chasis, String modelo, String precio,
            String color, String placa, String marca, String estado, String sede) {
        this.padre = padre;
        initComponents();        
        setLocationRelativeTo(null);
        jtfChasisV.setText(chasis);
        jtfColorV.setText(color);
        jtfEstado.setText(estado);
        jtfModelo.setText(modelo);
        jtfMarcaV.setText(marca);
        jtfPlacaV1.setText(placa);
        jtfPrecio.setValue(Integer.parseInt(precio));
        jtfSedeV.setText(new ManejoDatosSede().TraerNombreSedeCodigo( Integer.parseInt(sede)));
       
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCrearUser = new javax.swing.JPanel();
        jtfModelo = new javax.swing.JTextField();
        jlIconVehiculos = new javax.swing.JLabel();
        jlChasisV = new javax.swing.JLabel();
        jlModeloV = new javax.swing.JLabel();
        jlColorV = new javax.swing.JLabel();
        jlMarcaV = new javax.swing.JLabel();
        jlSedeV = new javax.swing.JLabel();
        jlPlacaV = new javax.swing.JLabel();
        jlEstadoV = new javax.swing.JLabel();
        jlBotonResgistrarS = new javax.swing.JLabel();
        jlAtras = new javax.swing.JLabel();
        jtfMarcaV = new javax.swing.JTextField();
        jtfChasisV = new javax.swing.JTextField();
        jlPrecio = new javax.swing.JLabel();
        jtfPlacaV1 = new javax.swing.JTextField();
        jtfColorV = new javax.swing.JTextField();
        jtfSedeV = new javax.swing.JTextField();
        jtfEstado = new javax.swing.JTextField();
        jtfPrecio = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelCrearUser.setBackground(new java.awt.Color(255, 255, 255));

        jtfModelo.setEditable(false);

        jlIconVehiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/iconVehiculos.jpg"))); // NOI18N

        jlChasisV.setText("Chasis:");

        jlModeloV.setText("Modelo:");

        jlColorV.setText("Color:");

        jlMarcaV.setText("Marca:");

        jlSedeV.setText("Sede:");

        jlPlacaV.setText("Placa:");

        jlEstadoV.setText("Estado:");

        jlBotonResgistrarS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/boton aceptar.jpg"))); // NOI18N

        jlAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlAtrasMouseClicked(evt);
            }
        });

        jtfMarcaV.setEditable(false);

        jtfChasisV.setEditable(false);

        jlPrecio.setText("Precio:");

        jtfPlacaV1.setEditable(false);

        jtfColorV.setEditable(false);

        jtfSedeV.setEditable(false);

        jtfEstado.setEditable(false);

        jtfPrecio.setEditable(false);
        jtfPrecio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("###,###.###"))));

        javax.swing.GroupLayout jPanelCrearUserLayout = new javax.swing.GroupLayout(jPanelCrearUser);
        jPanelCrearUser.setLayout(jPanelCrearUserLayout);
        jPanelCrearUserLayout.setHorizontalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlPlacaV)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jtfPlacaV1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlColorV)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jtfColorV, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlChasisV)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                            .addComponent(jtfChasisV, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlSedeV)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jtfSedeV, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlMarcaV, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jtfMarcaV, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                            .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jlEstadoV)
                                .addComponent(jlPrecio))
                            .addGap(18, 18, 18)
                            .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jtfEstado, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                                .addComponent(jtfPrecio))))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlModeloV)
                        .addGap(18, 18, 18)
                        .addComponent(jtfModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 88, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconVehiculos)
                        .addGap(85, 85, 85)
                        .addComponent(jlAtras)
                        .addGap(43, 43, 43))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlBotonResgistrarS)
                        .addGap(197, 197, 197))))
        );
        jPanelCrearUserLayout.setVerticalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlAtras)
                        .addGap(127, 127, 127))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconVehiculos)
                        .addGap(40, 40, 40)))
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlChasisV)
                    .addComponent(jtfChasisV, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlModeloV)
                    .addComponent(jtfModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlPlacaV)
                    .addComponent(jtfPlacaV1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlMarcaV)
                    .addComponent(jtfMarcaV, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlColorV)
                    .addComponent(jtfColorV, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlSedeV, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfSedeV, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlEstadoV)
                    .addComponent(jtfEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlPrecio)
                    .addComponent(jtfPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlBotonResgistrarS)
                .addGap(27, 27, 27))
        );

        getContentPane().add(jPanelCrearUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlAtrasMouseClicked
        this.setVisible(false);
        this.dispose();
        padre.setVisible(true);
    }//GEN-LAST:event_jlAtrasMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelCrearUser;
    private javax.swing.JLabel jlAtras;
    private javax.swing.JLabel jlBotonResgistrarS;
    private javax.swing.JLabel jlChasisV;
    private javax.swing.JLabel jlColorV;
    private javax.swing.JLabel jlEstadoV;
    private javax.swing.JLabel jlIconVehiculos;
    private javax.swing.JLabel jlMarcaV;
    private javax.swing.JLabel jlModeloV;
    private javax.swing.JLabel jlPlacaV;
    private javax.swing.JLabel jlPrecio;
    private javax.swing.JLabel jlSedeV;
    private javax.swing.JTextField jtfChasisV;
    private javax.swing.JTextField jtfColorV;
    private javax.swing.JTextField jtfEstado;
    private javax.swing.JTextField jtfMarcaV;
    private javax.swing.JTextField jtfModelo;
    private javax.swing.JTextField jtfPlacaV1;
    private javax.swing.JFormattedTextField jtfPrecio;
    private javax.swing.JTextField jtfSedeV;
    // End of variables declaration//GEN-END:variables
}
