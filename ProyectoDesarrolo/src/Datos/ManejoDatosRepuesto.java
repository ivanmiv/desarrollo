/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Consultas.ConsultarRepuesto;
import Consultas.ResultadoConsultaEmpleado;
import Consultas.ResultadoConsultaRepuesto;
import Modificacion.ModificarRepuesto;
import Modificacion.RealizarModificacionRepuestos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ivanmvr
 */
public class ManejoDatosRepuesto {

    public ManejoDatosRepuesto() {
    }

    public String RealizarRegistroRepuesto(String nombre, int precio, int cantidad, String descripcion, int sede) {

        String mensaje = new Datos.ConsultasRepuesto().IngresarRepuesto(nombre, precio, cantidad, descripcion, sede);
        return mensaje;
    }

    public void ObtenerDatosRepuestos(JTable tabla) {

        ArrayList<ArrayList<Object>> informacion = new ArrayList();

        ResultSet datos = new ConsultasRepuesto().BuscarRepuestosDisponibles();

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "Codigo", "Nombre", "Precio"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de repuestos

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("cod_repuesto"));
                datosFila.add(datos.getString("nombre"));
                datosFila.add(datos.getString("precio"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (NullPointerException e) {
            System.err.println(e);
        } catch (SQLException ex) {
            System.err.println(ex);
        }

        tabla.setModel(modelo);

    }

    public void ObtenerDatosRepuestosFiltrados(JTable tabla, String filtro, String valor) {

        String consulta = "";
        if (filtro.equalsIgnoreCase("cod_repuesto") || filtro.equalsIgnoreCase("precio")) {

            consulta = "SELECT cod_repuesto,nombre,precio FROM repuestos WHERE cantidad > 0 and "
                    + filtro + " = '" + Integer.parseInt(valor) + "';";

        } else {
            consulta = "SELECT cod_repuesto,nombre,precio FROM repuestos WHERE cantidad > 0 and "
                    + filtro + " ILIKE '%" + valor + "%';";

        }

        ResultSet datos = new Datos.Consultas().EjecutarConsulta(consulta);

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "Codigo", "Nombre", "Precio"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de repuestos

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("cod_repuesto"));
                datosFila.add(datos.getString("nombre"));
                datosFila.add(datos.getString("precio"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }

        tabla.setModel(modelo);

    }

    public ResultadoConsultaRepuesto CargarDatosRepuestoAConsultar(ConsultarRepuesto padre, String codigo) {
        ResultadoConsultaRepuesto consultaRepuestoEspecifico = null;

        try {
            ConsultasRepuesto consulta = new ConsultasRepuesto();
            ResultSet datos = consulta.BuscarRepuesto("cod_repuesto", codigo);

            datos.next();

            ResultSet datosSede =new Datos.ConsultasSede().BuscarSede("cod_sede",datos.getString("cod_sede"));
            datosSede.next();
            String sede = datosSede.getString("nombre");
            consultaRepuestoEspecifico = new ResultadoConsultaRepuesto(padre,
                    datos.getInt("cod_repuesto"),
                    datos.getString("nombre"),
                    datos.getDouble("precio"),
                    datos.getString("cantidad"),
                    datos.getString("descripcion"),
                    sede
            );

        } catch (SQLException ex) {
            System.err.println(ex);
        }

        return consultaRepuestoEspecifico;
    }
    
    
    public RealizarModificacionRepuestos CargarDatosRepuestoAModificar(ModificarRepuesto padre, String codigo){
        RealizarModificacionRepuestos modificacionRepuestoEspecifico = null;
        
        try{
            
        ConsultasRepuesto consulta = new ConsultasRepuesto();
        ResultSet datos = consulta.BuscarRepuesto("cod_repuesto", codigo);
        datos.next();
        ResultSet datosSede =new Datos.ConsultasSede().BuscarSede("cod_sede",datos.getString("cod_sede"));
        datosSede.next();
        String sede = datosSede.getString("nombre");
        
        modificacionRepuestoEspecifico = new RealizarModificacionRepuestos(padre,
                datos.getString("cod_repuesto"),
                datos.getString("nombre"),
                datos.getString("precio"),
                datos.getString("cantidad"),
                datos.getString("descripcion"),
                sede);
        
        } catch (SQLException ex) {
            System.err.println(ex);
        }        
        return modificacionRepuestoEspecifico;
    }
    
    
    public String RealizarModificacionRepuesto(String codigo,String nombre,String precio,String cantidad,
                                String descripcion,String sede) {
        ConsultasRepuesto consulta = new ConsultasRepuesto();
        String cod_sede = new Datos.Consultas().TraerCodigoSedeNombre(sede);
        
        
        
        String mensaje = consulta.ModificarRepuesto(codigo,nombre,precio,cantidad,descripcion,cod_sede);
        return mensaje;
    }
    
}
