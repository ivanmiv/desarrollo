
/*  
 * 
 * 
 * 
 */
package Datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasReporte {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasReporte() {

    }

    ResultSet ConsultarVentasYearMonth(int mes, int anho) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;

        // Create a calendar object of the desired month 
        System.out.println(mes);
        Calendar cal = new GregorianCalendar(anho, mes - 1, 1);
        // Get the number of days in that month 
        int dias = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        String subConsultaDias = "SELECT 1 as dia  ";
        for (int i = 2; i <= dias; i++) {
            subConsultaDias += " UNION ALL SELECT " + i + " \n ";
        }

        String consulta = "SELECT COALESCE(cantidad,0) cantidad , dias.dia , COALESCE(mes," + mes + ") mes , COALESCE(anho,'" + anho + "') anho  FROM "
                + "( " + subConsultaDias + " ) dias"
                + " LEFT JOIN "
                + " (SELECT COUNT(*) cantidad, EXTRACT(DAY FROM fecha) dia,EXTRACT(MONTH FROM fecha) mes,\n"
                + " EXTRACT(YEAR FROM fecha) anho\n"
                + "FROM ventas\n"
                + "WHERE EXTRACT(MONTH FROM fecha)=" + mes + "\n"
                + "AND EXTRACT(YEAR FROM fecha)=" + anho + "\n"
                + "GROUP BY fecha\n"
                + "ORDER BY dia ) cantidad_ventas  "
                + "ON cantidad_ventas.dia = dias.dia;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarVentasYearMonth(int mes, int anho, String sede) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;
        sede = new Datos.Consultas().TraerCodigoSedeNombre(sede);

        // Create a calendar object of the desired month 
        System.out.println(mes);
        Calendar cal = new GregorianCalendar(anho, mes - 1, 1);
        // Get the number of days in that month 
        int dias = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        String subConsultaDias = "SELECT 1 as dia  ";
        for (int i = 2; i <= dias; i++) {
            subConsultaDias += " UNION ALL SELECT " + i + " \n ";
        }

        String consulta = "SELECT COALESCE(cantidad,0) cantidad , dias.dia , COALESCE(mes," + mes + ") mes , COALESCE(anho,'" + anho + "') anho  FROM "
                + "( " + subConsultaDias + " ) dias"
                + " LEFT JOIN "
                + " (SELECT COUNT(*) cantidad, EXTRACT(DAY FROM fecha) dia,EXTRACT(MONTH FROM fecha) mes,\n"
                + " EXTRACT(YEAR FROM fecha) anho\n"
                + " FROM ventas\n"
                + " WHERE EXTRACT(MONTH FROM fecha)=" + mes + "\n"
                + " AND EXTRACT(YEAR FROM fecha)=" + anho + "\n"
                + " AND  cod_sede =" + sede + "\n"
                + " GROUP BY fecha\n"
                + " ORDER BY dia ) cantidad_ventas  "
                + " ON cantidad_ventas.dia = dias.dia "
                + " ;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarVentasYear(int anho) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;
        String consulta = "SELECT  todos_meses.mes, COALESCE(cantidad,0) cantidad,COALESCE(anho,'" + anho + "') anho "
                + " FROM (SELECT 1 as mes UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4"
                + " UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 UNION ALL SELECT 10"
                + " UNION ALL SELECT 11 UNION ALL SELECT 12)  todos_meses "
                + " LEFT JOIN "
                + " (SELECT COUNT(fecha) cantidad, EXTRACT(YEAR FROM fecha) anho, EXTRACT(MONTH FROM fecha) mes "
                + " FROM ventas WHERE EXTRACT(YEAR FROM fecha) ='" + anho + "'"
                + " GROUP BY  EXTRACT(YEAR FROM fecha),EXTRACT(MONTH FROM fecha)) cantidad_ventas "
                + " ON cantidad_ventas.mes = todos_meses.mes;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarVentasYear(int anho, String sede) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;

        sede = new Datos.Consultas().TraerCodigoSedeNombre(sede);
        String consulta = "SELECT  todos_meses.mes, COALESCE(cantidad,0) cantidad,COALESCE(anho,'" + anho + "') anho "
                + " FROM (SELECT 1 as mes UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4"
                + " UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 UNION ALL SELECT 10"
                + " UNION ALL SELECT 11 UNION ALL SELECT 12)  todos_meses "
                + " LEFT JOIN "
                + " (SELECT COUNT(fecha) cantidad, EXTRACT(YEAR FROM fecha) anho, EXTRACT(MONTH FROM fecha) mes "
                + " FROM ventas WHERE EXTRACT(YEAR FROM fecha) ='" + anho + "' AND cod_sede = " + sede
                + " GROUP BY  EXTRACT(YEAR FROM fecha),EXTRACT(MONTH FROM fecha)) cantidad_ventas "
                + " ON cantidad_ventas.mes = todos_meses.mes;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarOrdenesYearMonth(int mes, int anho) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;

        // Create a calendar object of the desired month 
        System.out.println(mes);
        Calendar cal = new GregorianCalendar(anho, mes - 1, 1);
        // Get the number of days in that month 
        int dias = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        String subConsultaDias = "SELECT 1 as dia  ";
        for (int i = 2; i <= dias; i++) {
            subConsultaDias += " UNION ALL SELECT " + i + " \n ";
        }

        String consulta = "SELECT COALESCE(cantidad,0) cantidad , dias.dia , COALESCE(mes," + mes + ") mes , COALESCE(anho,'" + anho + "') anho  FROM "
                + "( " + subConsultaDias + " ) dias"
                + " LEFT JOIN "
                + " (SELECT COUNT(*) cantidad, EXTRACT(DAY FROM fecha) dia,EXTRACT(MONTH FROM fecha) mes,\n"
                + " EXTRACT(YEAR FROM fecha) anho\n"
                + "FROM ordenes\n"
                + "WHERE EXTRACT(MONTH FROM fecha)=" + mes + "\n"
                + "AND EXTRACT(YEAR FROM fecha)=" + anho + "\n"
                + "GROUP BY fecha\n"
                + "ORDER BY dia ) cantidad_ordenes  "
                + "ON cantidad_ordenes.dia = dias.dia;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarOrdenesYearMonth(int mes, int anho, String sede) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;
        sede = new Datos.Consultas().TraerCodigoSedeNombre(sede);

        // Create a calendar object of the desired month 
        System.out.println(mes);
        Calendar cal = new GregorianCalendar(anho, mes - 1, 1);
        // Get the number of days in that month 
        int dias = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        String subConsultaDias = "SELECT 1 as dia  ";
        for (int i = 2; i <= dias; i++) {
            subConsultaDias += " UNION ALL SELECT " + i + " \n ";
        }

        String consulta = "SELECT COALESCE(cantidad,0) cantidad , dias.dia , COALESCE(mes," + mes + ") mes , COALESCE(anho,'" + anho + "') anho  FROM "
                + "( " + subConsultaDias + " ) dias"
                + " LEFT JOIN "
                + " (SELECT COUNT(*) cantidad, EXTRACT(DAY FROM fecha) dia,EXTRACT(MONTH FROM fecha) mes,\n"
                + " EXTRACT(YEAR FROM fecha) anho\n"
                + " FROM ordenes\n"
                + " WHERE EXTRACT(MONTH FROM fecha)=" + mes + "\n"
                + " AND EXTRACT(YEAR FROM fecha)=" + anho + "\n"
                + " AND  cod_sede =" + sede + "\n"
                + " GROUP BY fecha\n"
                + " ORDER BY dia ) cantidad_ordenes  "
                + " ON cantidad_ordenes.dia = dias.dia "
                + " ;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarOrdenesYear(int anho) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;
        String consulta = "SELECT  todos_meses.mes, COALESCE(cantidad,0) cantidad,COALESCE(anho,'" + anho + "') anho "
                + " FROM (SELECT 1 as mes UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4"
                + " UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 UNION ALL SELECT 10"
                + " UNION ALL SELECT 11 UNION ALL SELECT 12)  todos_meses "
                + " LEFT JOIN "
                + " (SELECT COUNT(fecha) cantidad, EXTRACT(YEAR FROM fecha) anho, EXTRACT(MONTH FROM fecha) mes "
                + " FROM ordenes WHERE EXTRACT(YEAR FROM fecha) ='" + anho + "'"
                + " GROUP BY  EXTRACT(YEAR FROM fecha),EXTRACT(MONTH FROM fecha)) cantidad_ordenes "
                + " ON cantidad_ordenes.mes = todos_meses.mes;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarOrdenesYear(int anho, String sede) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;

        sede = new Datos.Consultas().TraerCodigoSedeNombre(sede);
        String consulta = "SELECT  todos_meses.mes, COALESCE(cantidad,0) cantidad,COALESCE(anho,'" + anho + "') anho "
                + " FROM (SELECT 1 as mes UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4"
                + " UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 UNION ALL SELECT 10"
                + " UNION ALL SELECT 11 UNION ALL SELECT 12)  todos_meses "
                + " LEFT JOIN "
                + " (SELECT COUNT(fecha) cantidad, EXTRACT(YEAR FROM fecha) anho, EXTRACT(MONTH FROM fecha) mes "
                + " FROM ordenes WHERE EXTRACT(YEAR FROM fecha) ='" + anho + "' AND cod_sede = " + sede
                + " GROUP BY  EXTRACT(YEAR FROM fecha),EXTRACT(MONTH FROM fecha)) cantidad_ordenes "
                + " ON cantidad_ordenes.mes = todos_meses.mes;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarCotizacionesYearMonth(int mes, int anho) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;

        // Create a calendar object of the desired month 
        System.out.println(mes);
        Calendar cal = new GregorianCalendar(anho, mes - 1, 1);
        // Get the number of days in that month 
        int dias = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        String subConsultaDias = "SELECT 1 as dia  ";
        for (int i = 2; i <= dias; i++) {
            subConsultaDias += " UNION ALL SELECT " + i + " \n ";
        }

        String consulta = "SELECT COALESCE(cantidad,0) cantidad , dias.dia , COALESCE(mes," + mes + ") mes , COALESCE(anho,'" + anho + "') anho  FROM "
                + "( " + subConsultaDias + " ) dias"
                + " LEFT JOIN "
                + " (SELECT COUNT(*) cantidad, EXTRACT(DAY FROM fecha) dia,EXTRACT(MONTH FROM fecha) mes,\n"
                + " EXTRACT(YEAR FROM fecha) anho\n"
                + "FROM cotizaciones\n"
                + "WHERE EXTRACT(MONTH FROM fecha)=" + mes + "\n"
                + "AND EXTRACT(YEAR FROM fecha)=" + anho + "\n"
                + "GROUP BY fecha\n"
                + "ORDER BY dia ) cantidad_cotizaciones  "
                + "ON cantidad_cotizaciones.dia = dias.dia;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }

    ResultSet ConsultarCotizacionesYear(int anho) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoVentas = null;
        String consulta = "SELECT  todos_meses.mes, COALESCE(cantidad,0) cantidad,COALESCE(anho,'" + anho + "') anho "
                + " FROM (SELECT 1 as mes UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4"
                + " UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 UNION ALL SELECT 10"
                + " UNION ALL SELECT 11 UNION ALL SELECT 12)  todos_meses "
                + " LEFT JOIN "
                + " (SELECT COUNT(fecha) cantidad, EXTRACT(YEAR FROM fecha) anho, EXTRACT(MONTH FROM fecha) mes "
                + " FROM cotizaciones WHERE EXTRACT(YEAR FROM fecha) ='" + anho + "'"
                + " GROUP BY  EXTRACT(YEAR FROM fecha),EXTRACT(MONTH FROM fecha)) cantidad_cotizaciones "
                + " ON cantidad_cotizaciones.mes = todos_meses.mes;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoVentas = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoVentas;
    }
    
    
    ResultSet ConsultarEmpleados() {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultado = null;
        String consulta = "SELECT cedula,empleados.nombre nombre,empleados.telefono telefono,empleados.direccion direccion "
                + ",email,estado,perfil,fecha_ingreso,sedes.nombre nombre_sede  "
                + "FROM empleados INNER JOIN sedes ON empleados.cod_sede = sedes.cod_sede"
                + " ORDER BY fecha_ingreso;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultado = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultado;

    }

    ResultSet ConsultarSedes() {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultado = null;
        String consulta
                = "SELECT sedes.cod_sede cod_sede,sedes.nombre nombre_sede,sedes.direccion direccion_sede, "
                + " sedes.telefono telefono_sede, sedes.ciudad ciudad_sede ,cedula_gerente,empleados.nombre nombre_gerente , "
                + " empleados.telefono telefono_gerente "
                + " FROM sedes INNER JOIN empleados ON empleados.cedula = sedes.cedula_gerente ORDER BY cod_sede; ";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultado = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultado;
    }

    ResultSet ConsultarVehiculos() {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultado = null;
        String consulta = "SELECT chasis,modelo,marca,precio,color,placa,estado,nombre FROM vehiculos "
                + " INNER JOIN sedes ON sedes.cod_sede = vehiculos.cod_sede ORDER BY chasis;";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultado = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultado;

    }

    ResultSet ConsultarRepuestos() {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultado = null;
        String consulta = "SELECT cod_repuesto,repuestos.nombre nombre,precio,cantidad,sedes.nombre sede  "
                + "FROM repuestos INNER JOIN sedes ON repuestos.cod_sede = sedes.cod_sede ORDER BY cod_repuesto; ";

        System.out.println(consulta);
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultado = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultado;
    }

    

}
