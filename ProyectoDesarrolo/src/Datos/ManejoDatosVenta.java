/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modificacion.RealizarModificacionVenta;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ivanmvr
 */
public class ManejoDatosVenta {

    public ManejoDatosVenta() {
    }
    
    public String RealizarRegistroVenta(String cedula,String chasis,String pago,String cedula_comprador,
            String nombre_comprador,String direccion_comprador,String telefono_comprador,String correo_comprador, String placa){
        
        Calendar c = new GregorianCalendar();
        String dia = Integer.toString(c.get(Calendar.DATE));
        String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
        String annio = Integer.toString(c.get(Calendar.YEAR));
        String fecha = annio + "-" + mes + "-" + dia;
        
        ConsultasVenta venta = new ConsultasVenta();
        String mensaje = venta.IngresarVenta(cedula, chasis, pago, fecha, cedula_comprador, 
                nombre_comprador, direccion_comprador, telefono_comprador,correo_comprador, placa);
        
        return mensaje;
        
    }
    
    
    
    public ResultSet GenerarDatosFacturaVenta(String cedula_comprador,String chasis){
        
        ConsultasVenta venta = new ConsultasVenta();
        ResultSet datos_venta = venta.ConsultarDatosFactura(cedula_comprador,chasis);
        
        return datos_venta;
        
    }

    public void ObtenerDatosVentas(JTable tabla) {
        
        ResultSet datos = new ConsultasVenta().BuscarVentas();

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "codigo venta","chasis","placa", "cedula comprador"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de repuestos

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("cod_venta"));
                datosFila.add(datos.getString("chasis"));
                datosFila.add(datos.getString("placa"));
                datosFila.add(datos.getString("cedula_comprador"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
        }

        tabla.setModel(modelo);
    }

    
    public RealizarModificacionVenta CargarDatosVentaAModificar(JFrame padre,String cod_venta,
            String cedula_comprador,String chasis){
        RealizarModificacionVenta modificacion = null;
        
        try {
            ConsultasVenta consulta = new ConsultasVenta();
            ResultSet datos = consulta.ConsultarDatosFactura(cedula_comprador, chasis);
            datos.next();
            
            modificacion = new RealizarModificacionVenta(padre,
                    datos.getString("cod_venta"), datos.getString("cedulaV"),
                    datos.getString("cedula_comprador"),datos.getString("nombre_comprador"),
                    datos.getString("telefono_comprador"),datos.getString("direccion_comprador"),
                    datos.getString("chasis"),datos.getString("modelo"),
                    datos.getString("marca"),datos.getString("color"),
                    datos.getString("precio"),datos.getString("nombres"),
                    datos.getString("correo_electronico_comprador"));
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        
        return modificacion;
    }
    
    
    public String ModificarVenta(String cod_venta, String pago, String cedula_comprador,
            String nombre_comprador, String direccion_comprador, String telefono_comprador,String correo_comprador) {
        ConsultasVenta consulta = new ConsultasVenta();
        
        String mensaje = consulta.ModificarVenta(cod_venta,cedula_comprador,nombre_comprador,direccion_comprador,
                telefono_comprador,correo_comprador);
        
        return mensaje;
    }

    
}
