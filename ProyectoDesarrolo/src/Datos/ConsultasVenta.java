
/*  
 * 
 * 
 * 
 */
package Datos;

import Datos.Fachada;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasVenta {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasVenta() {

    }

    public String IngresarVenta(String cedula_vendedor,
            String chasis, String pago, String fecha, String cedula_comprador,
            String nombre_comprador, String direccion_comprador,
            String telefono_comprador, String correo_comprador, String placa) {

        int cod_sede_venta = Integer.parseInt(new ManejoDatosEmpleado().TraerSedeEmpleado(cedula_vendedor));

        String mensaje = "";
        String query = "INSERT INTO ventas(cedula,chasis,pago,fecha,cedula_comprador,nombre_comprador,direccion_comprador,telefono_comprador,correo_electronico_comprador,cod_sede)"
                + "VALUES ('" + cedula_vendedor + "','" + chasis.toUpperCase() + "','" + pago + "','"
                + fecha + "','" + cedula_comprador + "','" + nombre_comprador.toUpperCase() + "','" + direccion_comprador.toUpperCase() + "','" + telefono_comprador + "' , '" + correo_comprador.toUpperCase() + "' , " + cod_sede_venta + ");";
        String query2 = "UPDATE vehiculos SET placa='" + placa.toUpperCase() + "', estado = 'VENDIDO'" + " WHERE chasis = '" + chasis + "';";

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {

            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);

            if (msgexito == 1) {
                mensaje = "Registro ingresado con exito";
                Statement sentencia2 = this.conexion.createStatement();
                sentencia2.executeUpdate(query2);
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }

    public ResultSet ConsultarDatosFactura(String cedula_comprador, String chasis) {

        ResultSet datos_venta = null;
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();

        String query = "SELECT cod_venta,nombres,direccions,telefonos ,chasis,precio,modelo,marca,placa,color,"
                + "fecha,cedula_comprador,nombre_comprador,direccion_comprador,telefono_comprador,"
                + "cedulaV,nombre,correo_electronico_comprador "
                + "FROM vehiculos c(chasis,modelo,precio,color,placa,marca,estadoV,cod_sede_vehiculo)"
                + "NATURAL JOIN sedes s(cod_sede,nombreS,direccionS,telefonoS,ciudadS,cedula_gerente)"
                + "NATURAL JOIN empleados e(cedulaV,nombre,telefono,direccion,email,estadoE,perfil,usuario,contrasena,fecha_ingreso,cod_sede_empleado)"
                + "NATURAL JOIN ventas v(cod_venta,cedulaV,chasis,fecha,pagoV,cedula_comprador,nombre_comprador,direccion_comprador,telefono_comprador,"
                + "     correo_electronico_comprador,cod_sede)"
                + "WHERE chasis = '" + chasis + "'"
                + "AND cedula_comprador= '" + cedula_comprador + "' "
                + ";";
        System.out.println(query);
        try {
            this.sentencia = this.conexion.createStatement();
            datos_venta = this.sentencia.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error al consultar datos factura " + ex);
        }

        return datos_venta;

    }

    public ResultSet BuscarVentas() {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        ResultSet resultadoAux = null;
        String consulta = "SELECT cod_venta,chasis,placa,cedula_comprador "
                + "FROM ventas NATURAL JOIN vehiculos";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        } catch (NullPointerException e) {

        }
        fachada.CerrarBd();
        return resultadoAux;
    }

    public String ModificarVenta(String cod_venta, String cedula_comprador, String nombre_comprador, String direccion_comprador,
            String telefono_comprador, String correo_comprador) {

        String mensaje = "";
        String query
                = "UPDATE ventas "
                + "SET cedula_comprador = '" + cedula_comprador + "' ,"
                + "nombre_comprador = '" + nombre_comprador + "' ,"
                + "direccion_comprador = '" + direccion_comprador + "' ,"
                + "telefono_comprador = '" + telefono_comprador + "' "
                + "correo_electronico_comprador = '" + correo_comprador + "' "
                + "WHERE cod_venta = '" + cod_venta + "';";

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);
            if (msgexito == 1) {
                mensaje = "Registro modificado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo modificar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }
}
