/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Consultas.ResultadoConsultaVehiculo;
import Modificacion.RealizarModificacionVehiculo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ivanmvr
 */
public class ManejoDatosVehiculo {

    public ManejoDatosVehiculo() {
    }
    
    public String RealizarRegistroVehiculo( String chasis, String modelo, String placa, 
            String marca, String color, int codsede, String estado,int Precio){
               
            String mensaje = new Datos.ConsultasVehiculo().IngresarVehiculo(chasis, modelo, placa, marca, color, codsede, estado, Precio);
                  
        return mensaje;
        
        
    }

    public void ObtenerDatosVehiculosDisponibles(JTable tabla) {

        ArrayList<ArrayList<Object>> informacion = new ArrayList();

        ResultSet datos = new ConsultasVehiculo().BuscarVehiculosDisponibles();

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "chasis", "modelo", "precio", "color", "marca", "sede"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de repuestos

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("chasis"));
                datosFila.add(datos.getString("modelo"));
                datosFila.add(datos.getString("precio"));
                datosFila.add(datos.getString("color"));
                datosFila.add(datos.getString("marca"));
                datosFila.add(datos.getString("nombre"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
        }

        tabla.setModel(modelo);

    }

    public void ObtenerDatosVehiculosFiltrados(JTable tabla, String filtro, String valor) {

        ArrayList<ArrayList<Object>> informacion = new ArrayList();
        String consulta = "";

        if (filtro.equalsIgnoreCase("Marca") || filtro.equalsIgnoreCase("Modelo")) {

            consulta = "SELECT chasis,modelo,precio,color,placa,marca,estado,nombre FROM vehiculos "
                    + "NATURAL JOIN sedes WHERE"
                    + filtro + " ILIKE '%" + valor + "%' AND estado= 'DISPONIBLE';";

        } else {
            consulta = " SELECT chasis,modelo,precio,color,placa,marca,estado,nombre FROM vehiculos "
                    + "NATURAL JOIN sedes WHERE"
                    + filtro + " = '" + Integer.parseInt(valor) + "' AND estado= 'DISPONIBLE';";

        }

        ResultSet datos = new Datos.Consultas().EjecutarConsulta(consulta);

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "chasis", "modelo", "precio", "color", "placa", "marca", "estado", "sede"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de repuestos

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("chasis"));
                datosFila.add(datos.getString("modelo"));
                datosFila.add(datos.getString("precio"));
                datosFila.add(datos.getString("color"));
                datosFila.add(datos.getString("placa"));
                datosFila.add(datos.getString("marca"));
                datosFila.add(datos.getString("estado"));
                datosFila.add(datos.getString("nombre"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
        }

        tabla.setModel(modelo);

    }
    
    public ResultadoConsultaVehiculo CargarDatosVehiculoAConsultar(JFrame padre, String chasis) {
         
        ResultadoConsultaVehiculo resultado = null;
        
        try {
            ConsultasVehiculo consulta = new ConsultasVehiculo();
            ResultSet datos = consulta.BuscarVehiculo("chasis", "'"+chasis+"'");

            datos.next();
            resultado = new ResultadoConsultaVehiculo(padre,
                    datos.getString("chasis"), datos.getString("modelo"),
                    datos.getString("precio"), datos.getString("color"),
                    datos.getString("placa"), datos.getString("marca"),datos.getString("estado"),datos.getString("cod_sede"));

        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        return resultado;
    
    
    
    }
    
    public RealizarModificacionVehiculo CargarDatosvehiculoAModificar(JFrame padre, String chasis) {
        RealizarModificacionVehiculo modificacion = null;
        
        try {
            ConsultasVehiculo consulta = new ConsultasVehiculo();
            ResultSet datos = consulta.BuscarVehiculo("chasis", "'"+chasis+"'");

            datos.next();
            modificacion = new RealizarModificacionVehiculo(padre,
                    datos.getString("chasis"), datos.getString("modelo"),
                    datos.getString("precio"), datos.getString("color"),
                    datos.getString("placa"), datos.getString("marca"),datos.getString("estado"),datos.getString("cod_sede"));

        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        return modificacion;
    
    }
    
    public String RealizarModificacionVehiculo(String chasisactual,String chasis, String modelo, int precio, 
            String color, String placa, String marca, String estado,String sede){
        ConsultasVehiculo consulta = new ConsultasVehiculo();
        int cod_sede =  Integer.parseInt(new Datos.Consultas().TraerCodigoSedeNombre(sede)) ;
        
      
        
        String mensaje = consulta.ModificarVehiculo(chasisactual,chasis, modelo, precio, color, placa, marca, estado, cod_sede);
    
        return mensaje;
    }

    public void ObtenerDatosModelosDisponibles(JTable tabla) {
        
        ArrayList<ArrayList<Object>> informacion = new ArrayList();

        ResultSet datos = new ConsultasVehiculo().BuscarModelosDisponibles();

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "modelo", "marca","color", "precio", "sede"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de repuestos

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("modelo"));
                datosFila.add(datos.getString("marca"));
                datosFila.add(datos.getString("color"));                
                datosFila.add(datos.getString("precio"));
                datosFila.add(datos.getString("nombre_sede"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
        }

        tabla.setModel(modelo);

    }

    public void ObtenerDatosModelosFiltrados(JTable tabla, String filtro, String valor) {

        ArrayList<ArrayList<Object>> informacion = new ArrayList();
        String consulta = "";

        if (filtro.equalsIgnoreCase("Marca") || filtro.equalsIgnoreCase("Modelo")) {

            consulta = "SELECT modelo,marca,precio,color,nombre nombre_sede FROM vehiculos NATURAL JOIN sedes WHERE "
                    + filtro + " ILIKE '%" + valor + "%' AND estado= 'DISPONIBLE' "
                    + " GROUP BY marca,modelo,precio,nombre,color\";";

        } else {
            consulta = "SELECT modelo,marca,precio,color,nombre nombre_sede FROM vehiculos NATURAL JOIN sedes WHERE "
                    + filtro + " = '" + Integer.parseInt(valor) + "' AND estado= 'DISPONIBLE'"
                    + " GROUP BY marca,modelo,precio,nombre,color\";";

        }

        ResultSet datos = new Datos.Consultas().EjecutarConsulta(consulta);

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "modelo", "marca", "color","precio", "sede"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de repuestos

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("modelo"));
                datosFila.add(datos.getString("marca"));
                datosFila.add(datos.getString("color"));
                datosFila.add(datos.getString("precio"));                
                datosFila.add(datos.getString("nombre_sede"));

                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
        }

        tabla.setModel(modelo);

    }

}
