/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.sql.Connection;

/**
 *
 * @author ivanmvr
 */
public class ManejoDatos {
    
    Connection conexion;
    
    public ManejoDatos() {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        fachada.CerrarBd();
    }

    
}
