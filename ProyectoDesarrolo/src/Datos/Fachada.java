/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ivanmvr
 */
public class Fachada {
    
    private Connection conexion;
    private String usuario, contrasena, baseDatos;
    
    public Fachada() {
        Leerconfiguracion();
    }
    
    public void Leerconfiguracion() {
        FileReader lector = null;
        String linea;
        String delimitadores = "[-]";
        String[] arraytemp;
        try {

            File archivoConf = new File("bdconf.conf");
            lector = new FileReader(archivoConf);
            BufferedReader bufer = new BufferedReader(lector);

            while ((linea = bufer.readLine()) != null) {
                linea = linea.replace(" ", "");
                if (linea.contains("user")) {
                    arraytemp = linea.split(delimitadores);
                    this.usuario = arraytemp[1];

                } else if (linea.contains("pass")) {
                    arraytemp = linea.split(delimitadores);
                    this.contrasena = arraytemp[1];
                } else if (linea.contains("db")) {
                    arraytemp = linea.split(delimitadores);
                    this.baseDatos = arraytemp[1];
                }
            }

        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar con la base de datos.\n "
                    + "Compruebe el archivo de configuración o contacte al soporte técnico", "Error al conectar(file) a la Base de datos", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar con la base de datos.\n "
                    + "Compruebe el archivo de configuración o contacte al soporte técnico", "Error al conectar a la Base de datos", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                lector.close();

            } catch (IOException ex) {
                Logger.getLogger(Consultas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void ConectarBd() {

        try {
            Class.forName("org.postgresql.Driver");
            this.conexion = DriverManager.getConnection(this.baseDatos, this.usuario, this.contrasena);
            System.out.println("conexion iniciada");
        } catch (ClassNotFoundException ex) {
            System.out.println("Clase no encontrada");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se pudo conectar con la base de datos.\n "
                    + "Compruebe el archivo de configuración o contacte al soporte técnico", "Error al conectar a la Base de datos", JOptionPane.ERROR_MESSAGE);
            System.out.println("No se pudo conectar con la base de datos");
        }catch (NoClassDefFoundError ex){
            JOptionPane.showMessageDialog(null, "No se pudo conectar con la base de datos.\n "
                    + "Compruebe el archivo de configuración o contacte al soporte técnico", "Error al conectar a la Base de datos", JOptionPane.ERROR_MESSAGE);
        }

    }

    public void CerrarBd() {

        try {

            this.conexion.close();
        } catch (SQLException ex ) {
            System.out.println("No se pudo Cerrar la conexion con la base de datos");
        } catch (NullPointerException ex ) {
            System.out.println("No se pudo Cerrar la conexion con la base de datos "+ex);
        }

    }

    public Connection getConexion() {
        return conexion;
    }
    
    
    
}
