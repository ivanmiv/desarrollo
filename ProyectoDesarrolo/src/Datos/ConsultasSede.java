
/*  
 * 
 * 
 * 
 */
package Datos;

import Datos.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasSede {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasSede() {
    
    }
    
    
    public String IngresarSede(String nombre, String direccion, String telefono, String ciudad, String cedula) {
        String mensaje = "";
        String Query = "INSERT INTO sedes (nombre,direccion,telefono,ciudad,cedula_gerente) " + "values ('" + nombre.toUpperCase() + "','" + direccion.toUpperCase() + "','" + telefono + "','" + ciudad.toUpperCase() + "','" + cedula + "');";
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(Query);
            if (msgexito == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }
    
    public ResultSet BuscarSede(String criterio, String valorbusqueda) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT * FROM sedes "
                + " WHERE " + criterio + " = " + valorbusqueda + ";";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;

    }
    
    
    public ResultSet BuscarTodasSedes(){
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT * FROM sedes ;";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;   
    }
    
    
    public ResultSet BuscarNombresSedes(){
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT nombre FROM sedes ;";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;   
    }

     String ModificarSede(int cod, String nombre, String direccion
                        ,String telefono, String ciudad, String gerente) {
        
        String mensaje = "";
        String query = 
                "UPDATE sedes "
                + "SET nombre = '"+nombre+"' ,"
                + "direccion = '"+direccion+"' ,"
                + "telefono = '"+telefono+"' ,"
                + "ciudad = '"+ciudad+"' ,"
                + "cedula_gerente = '"+gerente+"' "
                + "WHERE cod_sede = '"+cod+"';"
                ;
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);
            if (msgexito == 1) {
                mensaje = "Registro modificado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }
    

}
































