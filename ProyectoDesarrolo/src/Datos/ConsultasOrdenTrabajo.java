
/*  
 * 
 * 
 * 
 */
package Datos;

import Datos.Fachada;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasOrdenTrabajo {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasOrdenTrabajo() {
    
    }

    
    public String registrarOrdenTrabajo(String fecha,String valor, String nombre,
	String cedula, String direccion, String telefono, String placa_vehiculo, String sede, String descripcion){
        System.out.print("fecha: "+fecha+"\n");
        System.out.print("valor: "+valor+"\n");
        System.out.print("nombre: "+nombre+"\n");
        System.out.print("cedula: "+cedula+"\n");
        System.out.print("direccion: "+direccion+"\n");
        System.out.print("telefono: "+telefono+"\n");
        System.out.print("placa_vehiculo: "+placa_vehiculo+"\n");
        System.out.print("sede: "+sede+"\n");
        System.out.print("descripcion: "+descripcion+"\n");
        String mensaje = "";
        String query = "INSERT INTO ordenes (fecha,valor,cedula, descripcion,placa_vehiculo,nombre_cliente,direccion_cliente,telefono_cliente,cod_sede)" + " values ('"
                +fecha+"',"+valor+",'"+ cedula + "','" + descripcion.toUpperCase() + "','" + placa_vehiculo.toUpperCase()  + "','"+nombre+"','"+direccion+"','"+telefono+"'," + sede + ");";
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);
            if (msgexito == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
        
        
    }

    ResultSet traerTodasOrdenesPlacaIngresada(String placa) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT *  FROM ordenes WHERE placa_vehiculo = '"+placa+"' ;";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;
    
    }

    ResultSet traerInformacionOrdenEspecifica(String cod_orden, String placa) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT *  FROM ordenes WHERE placa_vehiculo = '"+placa+"' AND cod_orden ="+cod_orden+";";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;
    
    }

    ResultSet traerRepuestosOrdenEspecifica(String cod_orden) {
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT repuestos.nombre,repuestos_ordenes.cantidad FROM repuestos INNER JOIN " +
                    "repuestos_ordenes ON repuestos_ordenes.cod_repuesto=repuestos.cod_repuesto " +
                    "WHERE repuestos_ordenes.cod_orden ="+cod_orden+";";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;
    }

    String ModificarOrdenTrabajo(String cod_orden,String fecha,String valor, String nombre,
	String cedula, String direccion, String telefono, String placa_vehiculo, String sede, String descripcion){
        System.out.println("CODIGO-ORDEN "+cod_orden);
        System.out.print("fecha: "+fecha+"\n");
        System.out.print("valor: "+valor+"\n");
        System.out.print("nombre: "+nombre+"\n");
        System.out.print("cedula: "+cedula+"\n");
        System.out.print("direccion: "+direccion+"\n");
        System.out.print("telefono: "+telefono+"\n");
        System.out.print("placa_vehiculo: "+placa_vehiculo+"\n");
        System.out.print("sede: "+sede+"\n");
        System.out.print("descripcion: "+descripcion+"\n");
        String mensaje = "";
        String query = "UPDATE ordenes SET"
                + " fecha = '"+fecha
                + "', valor = "+valor
                + ", cedula = '"+cedula
                + "', descripcion = '"+descripcion
                + "', nombre_cliente = '"+nombre
                + "', direccion_cliente = '"+direccion
                + "', telefono_cliente = '"+telefono+"' WHERE cod_orden="+cod_orden+";";
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);
            if (msgexito == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro AQUI :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }

}
































