/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Consultas.ResultadoConsultaSede;
import Modificacion.RealizarModificacionSede;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ivanmvr
 */
public class ManejoDatosSede {

    public ManejoDatosSede() {
    }

    public void CargarDatosComboBox(JComboBox comboBox, String dato) {
        ConsultasSede consulta = new ConsultasSede();
        ResultSet resultado = null;

        switch (dato) {
            case "sedes":
                resultado = consulta.BuscarNombresSedes();
                break;
            default:
                break;
        }

        try {
            while (resultado.next()) {
                comboBox.addItem(resultado.getString("nombre"));

            }
        } catch (SQLException ex) {
            System.out.println("sqlE " + ex.getSQLState());
        }
    }

    public String RealizarRegistroSede(String cedulaGerente, String ciudad, String direccion, String nombreSede,
            String telefono) {

        String mensaje = new ConsultasSede().IngresarSede(nombreSede, direccion, telefono, ciudad, cedulaGerente);
        return mensaje;
    }

    public void ObtenerDatosTodaslasSedes(JTable tabla) {

        ArrayList<ArrayList<Object>> informacion = new ArrayList();
        ResultSet datos = new ConsultasSede().BuscarTodasSedes();

        DefaultTableModel modelo = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
        };
        modelo.setColumnIdentifiers(new String[]{
            "Nombre", "Identificador"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de usuarios

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("nombre"));
                datosFila.add(datos.getString("cod_sede"));
                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }

        tabla.setModel(modelo);
    }

    public ResultadoConsultaSede CargarDatosSede(JFrame padre, String cod_sede) {

        ResultadoConsultaSede consultas = null;

        try {
            ConsultasSede consulta = new ConsultasSede();
            ResultSet datos = consulta.BuscarSede("cod_sede", cod_sede);

            datos.next();
            consultas = new ResultadoConsultaSede(padre,
                    datos.getString("nombre"), datos.getString("telefono"),
                    datos.getString("direccion"), datos.getString("ciudad"),
                    datos.getString("cedula_gerente"));
            
            

        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        return consultas;
    }

    public int TraerCodigoSedeNombre(String nombre) {

        int resultado = 0;

        ResultSet datos = new ConsultasSede().BuscarSede("nombre", "'" + nombre + "'");

        try {
            while (datos.next()) {

                resultado = datos.getInt("cod_sede");

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }

        return resultado;
    }

    public String TraerNombreSedeCodigo(int codigo) {

        String resultado = "";

        ResultSet datos = new ConsultasSede().BuscarSede("cod_sede", "'" + codigo + "'");

        try {
            while (datos.next()) {

                resultado = datos.getString("nombre");

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }

        return resultado;
    }

    public RealizarModificacionSede CargarDatosSedeAModificar(JFrame padre, String nombre) {
        RealizarModificacionSede modificacion = null;

        try {
            ConsultasSede consulta = new ConsultasSede();
            ResultSet datos = consulta.BuscarSede("cod_sede", "'" + nombre + "'");

            datos.next();
            modificacion = new RealizarModificacionSede(padre, datos.getInt("cod_sede"),
                    datos.getString("nombre"), datos.getString("direccion"),
                    datos.getString("telefono"), datos.getString("ciudad"),
                    datos.getString("cedula_gerente"));

        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        return modificacion;

    }

    public String RealizarModificacionSede(int cod, String nombre, String direccion, String telefono, String ciudad, String gerente) {

        ConsultasSede consulta = new ConsultasSede();

        String mensaje = consulta.ModificarSede(cod, nombre, direccion, telefono, ciudad, gerente);

        return mensaje;

    }
}
