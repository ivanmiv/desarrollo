
/*  
 * 
 * 
 * 
 */
package Datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class Consultas {

    private Connection conexion;
    private String usuario, contrasena, baseDatos;
    private ResultSet resultado;
    private Statement sentencia;

    public Consultas() {
        
    }


    public ArrayList<Object> ValidarLogin(String usuario, String contrasena) {

        ArrayList arrayresult = new ArrayList<>();
        String criptedpass = new Encriptar().EncriptarMd5(contrasena);
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        System.out.println(new Encriptar().DesencriptarMd5("GBuXUwPkgpA="));
        
        
        String consulta
                = "SELECT * FROM empleados "                
                + " WHERE usuario = '" + usuario + "' AND contrasena = '" + criptedpass + "' AND estado = 'ACTIVO';";
        
        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (!this.resultado.isBeforeFirst()) {
                arrayresult = null;

            } else {

                while (this.resultado.next()) {

                    arrayresult.add(this.resultado.getString("cedula"));
                    arrayresult.add(this.resultado.getString("nombre"));
                    arrayresult.add(this.resultado.getString("perfil"));

                }
            }

        } catch (SQLException | NullPointerException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
            throw new NullPointerException("Error");
        } catch (NoClassDefFoundError ex){
            JOptionPane.showMessageDialog(null, "No se pudo conectar con la base de datos.\n "
                    + "Compruebe el archivo de configuración o contacte al soporte técnico", "Error al conectar a la Base de datos", JOptionPane.ERROR_MESSAGE);
        }

        fachada.CerrarBd();
        return arrayresult;
    }

    public ResultSet BusquedaGeneral(String criterio, String valorbusqueda, String tabla) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultdoAux = null;
        String consulta
                = "SELECT * FROM " + tabla + ""
                + " WHERE " + criterio + " = '" + valorbusqueda + "';";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultdoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        
        fachada.CerrarBd();
        return resultdoAux;

    }

    public ResultSet EjecutarConsulta(String Consulta) {
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultdoAux = null;

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(Consulta);

            if (this.resultado.isBeforeFirst()) {

                resultdoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultdoAux;

    }
       
    public String TraerCodigoSedeNombre(String nombre) {

        String respuesta = "";
        resultado = new Datos.ConsultasSede().BuscarSede("nombre","'"+nombre+"'");

        try {
            while (resultado.next()) {

                respuesta = resultado.getString("cod_sede");
                
            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }

        return respuesta;
    }
    
   
    
}
































