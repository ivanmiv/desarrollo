
/*  
 * 
 * 
 * 
 */
package Datos;

import Datos.Fachada;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasEmpleado {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasEmpleado() {
        
        
    }
    
    
    public ResultSet BuscarEmpleado(String criterio, String valorbusqueda) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultdoAux = null;
        String consulta
                = "SELECT * FROM empleados "
                + " WHERE " + criterio + " = " + valorbusqueda + ";";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultdoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultdoAux;

    }

    
    public ResultSet BuscarEmpleados(){
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT * FROM empleados ;";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;
    }
    
    public String IngresarEmpleado(String cedula, String nombre, String telefono, String direccion, String email, String estado,
            String perfil, String usuario, String contrasena, String fecha_ingreso, String cod_sede) {
        String mensaje = "";
        System.out.println("La sede es: "+cod_sede+"\n");
        
        String query = "INSERT INTO empleados (cedula, nombre,telefono,direccion,email,estado,perfil,"
                + "usuario,contrasena,fecha_ingreso,cod_sede)" + "values ('" + cedula + 
                "','" + nombre.toUpperCase() + "','" + direccion.toUpperCase() + "','" + telefono +
                "','" + email.toLowerCase() + "','" + estado + "','" + perfil + "','" + usuario + 
                "','" + contrasena + "','" + fecha_ingreso + "','" + cod_sede + "');";
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);
            if (msgexito == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }

    String ModificarEmpleado(String cedula, String nombre, String telefono,
            String direccion, String email, String estado, String cargo, 
            String usuario, String contrasena, String cod_sede) {
        
        String mensaje = "";
        String query = 
                "UPDATE empleados "
                + "SET nombre = '"+nombre.toUpperCase()+"' ,"
                + "telefono = '"+telefono+"' ,"
                + "direccion = '"+direccion.toUpperCase()+"' ,"
                + "email = '"+email.toLowerCase()+"' ,"
                + "estado = '"+estado+"' ,"
                + "perfil = '"+cargo+"' ,"
                + "usuario = '"+usuario+"' ,"
                + "contrasena = '"+contrasena+"' ,"
                + "cod_sede = "+cod_sede+" "
                + "WHERE cedula = '"+cedula+"';"
                ;
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);
            if (msgexito == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }
    
    
    public ResultSet BuscarGerentesLibres(){
        
    
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultdoAux = null;
        String consulta
                = "SELECT cedula from empleados WHERE perfil = 'GERENTE'\n" +
                  "EXCEPT \n" +
                  "SELECT cedula from empleados INNER JOIN sedes ON(cedula = cedula_gerente) ";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultdoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultdoAux;
    }
    
    

}
































