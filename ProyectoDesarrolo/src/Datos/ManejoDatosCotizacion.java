/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ivanmvr
 */
public class ManejoDatosCotizacion {

    public ManejoDatosCotizacion() {
    }
    
    
    public String RealizarRegistroCotizacion(String marca,String modelo,String precio,String color,
            String cedula, String cedula_cotizante,String nombre_cotizante, String direccion_cotizante,
            String telefono_cotizante, String email_cotizante) {
        
        Calendar c = new GregorianCalendar();
        String dia = Integer.toString(c.get(Calendar.DATE));
        String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
        String annio = Integer.toString(c.get(Calendar.YEAR));
        String fecha = annio + "-" + mes + "-" + dia;
        
        ConsultasCotizacion cotizacion = new ConsultasCotizacion();
        String mensaje = cotizacion.IngresarCotizacion(cedula,marca,modelo,precio,color,fecha, cedula_cotizante, 
                nombre_cotizante, direccion_cotizante, telefono_cotizante,email_cotizante);
        
        return mensaje;
    }

    
    
    
    public ResultSet GenerarDatosFacturaVenta(String cedula_comprador,String chasis){
        
        ConsultasCotizacion venta = new ConsultasCotizacion();
        ResultSet datos_venta = venta.ConsultarDatosFactura(cedula_comprador,chasis);
        
        return datos_venta;
        
    }

    public ArrayList<ArrayList<String>> consultarCotizacionesPorCedula(String cedula) {
        ArrayList<ArrayList<String>> consultas = new ArrayList<ArrayList<String>>();
        
        ConsultasCotizacion resultadoConsulta = new ConsultasCotizacion();
        
        ResultSet resultado = resultadoConsulta.consultarInfoCotizacionesDadaCedula(cedula);
        
        try {
            while(resultado.next()){
                ArrayList<String> consulta = new ArrayList<String>();
                consulta.add(resultado.getString("cod_cotizacion"));
                System.out.println("CONSULTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA NUMERO "+resultado.getString("cod_cotizacion"));
                consulta.add(resultado.getString("fecha"));
                consulta.add(resultado.getString("marca"));
                consulta.add(resultado.getString("modelo"));
                consulta.add(resultado.getString("precio"));
                consulta.add(resultado.getString("cedula"));
                consulta.add(resultado.getString("color"));
                consulta.add(resultado.getString("cedula_cotizante"));
                consulta.add(resultado.getString("nombre_cotizante"));
                consulta.add(resultado.getString("direccion_cotizante"));
                consulta.add(resultado.getString("telefono_cotizante"));
                consulta.add(resultado.getString("correo_electronico_cotizante"));
                consultas.add(consulta);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ManejoDatosCotizacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        return consultas;
    }
    public ArrayList<String> consultarCotizacionPorCedula(int id_cotizacion,String cedula) {
        ArrayList<String> consulta = new ArrayList<String>();
        ConsultasCotizacion resultadoConsulta = new ConsultasCotizacion();
        
        ResultSet resultado = resultadoConsulta.consultarInfoCotizacion(id_cotizacion,cedula);
        
        try {
            while(resultado.next()){
                consulta.add(resultado.getString("cod_cotizacion"));
                consulta.add(resultado.getString("cedula"));
                consulta.add(resultado.getString("marca"));
                consulta.add(resultado.getString("modelo"));
                consulta.add(resultado.getString("precio"));
                consulta.add(resultado.getString("color"));
                consulta.add(resultado.getString("fecha"));
                consulta.add(resultado.getString("cedula_cotizante"));
                consulta.add(resultado.getString("nombre_cotizante"));
                consulta.add(resultado.getString("direccion_cotizante"));
                consulta.add(resultado.getString("telefono_cotizante"));
                consulta.add(resultado.getString("correo_electronico_cotizante"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ManejoDatosCotizacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        return consulta;
    }

    public String RealizarModificacionCotizacion(String cod_cotizacion, String cedula_cotizante, String nombre_comprador, String direccion_comprador, String telefono_comprador, String email_comprador) {
        
        ConsultasCotizacion cotizacion = new ConsultasCotizacion();
        String mensaje = cotizacion.modificarInformacionCotizacion(cod_cotizacion, cedula_cotizante, 
                nombre_comprador, direccion_comprador, telefono_comprador,email_comprador);
        
        return mensaje;
    }

    public ResultSet GenerarDatosCotizacion(String cod_cotizacion,String cedula_comprador, String cedula_vendedor, String marca, String modelo) {
        ConsultasCotizacion cotizacion = new ConsultasCotizacion();
        ResultSet datos_cotizacion = cotizacion.ConsultarDatosCotizacion(cod_cotizacion,cedula_comprador,cedula_vendedor,marca,modelo);
        
        return datos_cotizacion;
    
    }
    
    public int maxCotizacion(){
        ConsultasCotizacion resultadoConsulta = new ConsultasCotizacion();
        ResultSet resultado = resultadoConsulta.maxCotizaciones();
        int maxCotizacion = 0;
        try {
            while(resultado.next()){
                maxCotizacion = Integer.parseInt(resultado.getString("max"));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ManejoDatosCotizacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maxCotizacion;
    }
    
    


    
}
