
/*  
 * 
 * 
 * 
 */
package Datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasRepuesto {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasRepuesto() {
    
    }
    
    
     public String IngresarRepuesto(String nombre,int  precio,int cantidad,String descripcion,int sede) {
        String mensaje ="";
        String Query
                = "INSERT INTO repuestos (nombre,precio,cantidad,descripcion,cod_sede) "
                + "values ('" + nombre.toUpperCase() + "','" + precio + "','" + cantidad + "','" 
                + descripcion.toUpperCase() + "','"+sede+"');";
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(Query);

            if (msgexito == 1) {

                mensaje = "Registro ingresado con exito";
            }

        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();


        }
        fachada.CerrarBd();
        return mensaje;
    }

    
    public ResultSet BuscarRepuestosDisponibles(){
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT cod_repuesto,nombre,precio FROM repuestos WHERE cantidad > 0";

        try {
            
            

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;
    }
    

    public ResultSet BuscarRepuesto(String criterio, String valorbusqueda) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT * FROM repuestos "
                + " WHERE " + criterio + " = " + valorbusqueda + ";";
       

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;


    }  


    public ResultSet buscarNombresRepuestos(){
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT nombre,cantidad FROM repuestos;";


        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;


    }      
    
    
    
    
    String ModificarRepuesto(String codigo, String nombre, String precio, String cantidad, String descripcion, String sede) {
        
        String mensaje = "";
        String query = 
                "UPDATE repuestos "
                + "SET nombre = '"+nombre+"' ,"
                + "precio = "+precio+" ,"
                + "cantidad = "+cantidad+" ,"
                + "descripcion = '"+descripcion+"' ,"
                + "cod_sede = "+sede+" "
                + "WHERE cod_repuesto = "+codigo+";"
                ;
        
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);
            if (msgexito == 1) {
                mensaje = "Registro modificado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar la modificacion :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    
    

    }
    
    public void modificarInformacionRepuesto(String nombreRepuesto, String cantidad){
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        String registroEn_repuestos_ordenes
                = "INSERT INTO repuestos_ordenes VALUES("
                + "(SELECT MAX(cod_orden)FROM ordenes),"
                + "(SELECT cod_repuesto FROM repuestos WHERE nombre= '"+nombreRepuesto+"'),"
                + cantidad
                + ");";
        String modificar_cantidades_tabla_repuestos
                = "UPDATE repuestos SET "
                + "cantidad= (cantidad-"+cantidad+") WHERE nombre= '"+nombreRepuesto+"';";
        //System.out.println(registroEn_repuestos_ordenes+"\n");
        System.out.println(modificar_cantidades_tabla_repuestos+"\n");

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(registroEn_repuestos_ordenes);
        } catch (SQLException ex) {
            //System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        try {

            this.sentencia = this.conexion.createStatement();
            this.resultado = this.sentencia.executeQuery(modificar_cantidades_tabla_repuestos);
        } catch (SQLException ex) {
            //System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
    }

   

    
    

}
































