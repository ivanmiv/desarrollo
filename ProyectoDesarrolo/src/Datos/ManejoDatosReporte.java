/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.sql.ResultSet;

/**
 *
 * @author ivanmvr
 */
public class ManejoDatosReporte {

    public ManejoDatosReporte() {
    }

    public ResultSet ConsultarVentasYearMonth(int mes, int anho) {

        return new ConsultasReporte().ConsultarVentasYearMonth(mes, anho);
    }

    public ResultSet ConsultarVentasYearMonth(int mes, int anho, String sede) {

        return new ConsultasReporte().ConsultarVentasYearMonth(mes, anho, sede);
    }

    public ResultSet ConsultarVentasYear(int anho) {

        return new ConsultasReporte().ConsultarVentasYear(anho);
    }

    public ResultSet ConsultarVentasYear(int anho, String sede) {

        return new ConsultasReporte().ConsultarVentasYear(anho, sede);
    }
    
    public ResultSet ConsultarOrdenesYearMonth(int mes, int anho) {

        return new ConsultasReporte().ConsultarOrdenesYearMonth(mes, anho);
    }

    public ResultSet ConsultarOrdenesYearMonth(int mes, int anho, String sede) {

        return new ConsultasReporte().ConsultarOrdenesYearMonth(mes, anho, sede);
    }

    public ResultSet ConsultarOrdenesYear(int anho) {

        return new ConsultasReporte().ConsultarOrdenesYear(anho);
    }

    public ResultSet ConsultarOrdenesYear(int anho, String sede) {

        return new ConsultasReporte().ConsultarOrdenesYear(anho, sede);
    }
    

    public ResultSet ConsultarEmpleados() {

        return new ConsultasReporte().ConsultarEmpleados();
    }

    public ResultSet ConsultarSedes() {
        return new ConsultasReporte().ConsultarSedes();

    }

    public ResultSet ConsultarVehiculos() {
        return new ConsultasReporte().ConsultarVehiculos();
    }

    public ResultSet ConsultarRepuestos() {
        return new ConsultasReporte().ConsultarRepuestos();

    }

    public ResultSet ConsultarCotizacionesYearMonth(int mes, int anho) {
        return new ConsultasReporte().ConsultarCotizacionesYearMonth(mes, anho);
    }

    public ResultSet ConsultarCotizacionesYear(int anho) {
        return new ConsultasReporte().ConsultarCotizacionesYear(anho);
    }

}
