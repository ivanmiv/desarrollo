
/*  
 * 
 * 
 * 
 */
package Datos;

import Datos.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasVehiculo {

    private Connection conexion;
    private String usuario, contrasena, baseDatos;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasVehiculo() {
        
    }
    
    public String IngresarVehiculo(String chasis, String modelo, String placa, 
            String marca, String color, int codsede, String estado,int precio){
            
        String Query
                = "INSERT INTO vehiculos (chasis,modelo,precio,color,marca,estado,cod_sede) "
                + "values ('" + chasis + "','" +modelo + "','" + precio + "','" + color + "','" 
                +marca+ "','" + estado+ "','" + codsede+"');";
        
        if (!placa.isEmpty()) {
                
                Query
                = "INSERT INTO vehiculos (chasis,modelo,precio,color,placa,marca,estado,cod_sede) "
                + "values ('" + chasis.toUpperCase() + "','" +modelo + "','" + precio + "','" + color + "','" 
                + placa.toUpperCase() + "','"+marca+ "','" + estado+ "','" + codsede+"');";               
        }
        
         String mensaje ="";
        
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(Query);

            if (msgexito == 1) {

                mensaje = "Registro ingresado con exito";
            }

        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();


        }
        fachada.CerrarBd();
        return mensaje;
    
        
    }
    
    public ResultSet BuscarVehiculo(String criterio, String valorbusqueda) {

        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT * FROM vehiculos "
                + " WHERE " + criterio + " = " + valorbusqueda + ";";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        }
        fachada.CerrarBd();
        return resultadoAux;

    }
    
    public ResultSet BuscarVehiculosDisponibles(){
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT * FROM vehiculos NATURAL JOIN sedes WHERE estado= 'DISPONIBLE' ";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        } catch (NullPointerException e){
            
        }
        fachada.CerrarBd();
        return resultadoAux;
    }
    
    ResultSet BuscarModelosDisponibles() {
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion=fachada.getConexion();
        ResultSet resultadoAux = null;
        String consulta
                = "SELECT modelo,marca,precio,color,nombre nombre_sede FROM vehiculos NATURAL JOIN sedes WHERE estado= 'DISPONIBLE' "
                + "GROUP BY marca,modelo,precio,nombre,color";

        try {

            this.sentencia = this.conexion.createStatement();

            this.resultado = this.sentencia.executeQuery(consulta);

            if (this.resultado.isBeforeFirst()) {

                resultadoAux = this.resultado;
            }

        } catch (SQLException ex) {
            System.out.println("No se pudo realizar la consulta :" + ex.getMessage());
        } catch (NullPointerException e){
            
        }
        fachada.CerrarBd();
        return resultadoAux;
    }
    
    
    
    String ModificarVehiculo(String chasis,String nchasis,String modelo,int precio,
            String color,String placa,String marca,String estado,int sede) {
        
        String mensaje = "";
        String query = 
                "UPDATE vehiculos "
                + "SET chasis = '"+nchasis+"' ,"
                + "modelo = '"+modelo+"' ,"
                + "precio = '"+precio+"' ,"
                + "color = '"+color+"' ,"
                + "marca = '"+marca+"' ,"
                + "placa = '"+placa+"' ,"
                + "estado = '"+estado+"' ,"
                + "cod_sede = '"+sede+"' "
                + "WHERE chasis = '"+chasis+"';"
                ;
        
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {
            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);
            if (msgexito == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }

    
}
































