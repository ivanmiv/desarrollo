
/*  
 * 
 * 
 * 
 */
package Datos;

import Datos.Fachada;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Clase encargada de entregar datos al modelo
 *
 */
public class ConsultasCotizacion {

    private Connection conexion;
    private ResultSet resultado;
    private Statement sentencia;

    public ConsultasCotizacion() {

    }

    public ResultSet ConsultarDatosFactura(String cedula_comprador, String chasis) {

        ResultSet datos_venta = null;
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();

        String query = "SELECT cod_venta,nombres,direccions,telefonos ,chasis,precio,modelo,marca,placa,color,"
                + "fecha,cedula_comprador,nombre_comprador,direccion_comprador,telefono_comprador,"
                + "cedulaV,nombre "
                + "FROM vehiculos c(chasis,modelo,precio,color,placa,marca,estadoV,cod_sede)"
                + "NATURAL JOIN sedes s(cod_sede,nombreS,direccionS,telefonoS,ciudadS,cedula_gerente)"
                + "NATURAL JOIN empleados e(cedulaV,nombre,telefono,direccion,email,estadoE,perfil,usuario,contrasena,fecha_ingreso,cod_sede)"
                + "NATURAL JOIN ventas v(cod_venta,cedulaV,chasis,fecha,pagoV,cedula_comprador,nombre_comprador,direccion_comprador,telefono_comprador)"
                + "WHERE chasis = '" + chasis + "'"
                + "AND cedula_comprador= '" + cedula_comprador + "' "
                + ";";

        try {
            this.sentencia = this.conexion.createStatement();
            datos_venta = this.sentencia.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error al consultar datos factura " + ex);
        }

        return datos_venta;

    }

    String IngresarCotizacion(String cedula, String marca, String modelo, String precio, String color,
            String fecha, String cedula_cotizante, String nombre_cotizante, String direccion_cotizante,
            String telefono_cotizante, String correo_electronico_cotizante) {
        String mensaje = "";
        String query = "INSERT INTO cotizaciones(cedula,marca,modelo,precio,color,fecha,cedula_cotizante,nombre_cotizante,direccion_cotizante,telefono_cotizante,correo_electronico_cotizante)"
                + "VALUES ('" + cedula + "','" + marca + "','" + modelo + "'," + precio + ",'" + color + "','"
                + fecha + "','" + cedula_cotizante + "','" + nombre_cotizante + "','" + direccion_cotizante + "','" + telefono_cotizante + "','"
                + correo_electronico_cotizante
                + "');";

        System.out.println(query);
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {

            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);

            if (msgexito == 1) {
                mensaje = "Registro ingresado con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }

       ResultSet consultarInfoCotizacionesDadaCedula(String cedula) {
        ResultSet datos_cotizacion = null;
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();

        String query = "SELECT * FROM cotizaciones WHERE cedula_cotizante='" + cedula + "';";

        try {
            this.sentencia = this.conexion.createStatement();
            datos_cotizacion = this.sentencia.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error al consultar datos cotización " + ex);
        }

        return datos_cotizacion;

//To change body of generated methods, choose Tools | Templates.
    }
    ResultSet consultarInfoCotizacion(int id_cotizacion,String cedula) {
        ResultSet datos_cotizacion = null;
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        String id_cot = ""+id_cotizacion;

        String query = "SELECT * FROM cotizaciones WHERE cod_cotizacion = "+id_cot+" AND cedula_cotizante='" + cedula + "';";

        try {
            this.sentencia = this.conexion.createStatement();
            datos_cotizacion = this.sentencia.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error al consultar datos cotización " + ex);
        }

        return datos_cotizacion;

//To change body of generated methods, choose Tools | Templates.
    }

    String modificarInformacionCotizacion(String cod_cotizacion, String cedula_cotizante, String nombre_comprador, String direccion_comprador, String telefono_comprador, String email_comprador) {
        String mensaje = "";
        String query = "UPDATE cotizaciones SET "
                + "cedula_cotizante = '" + cedula_cotizante + "',"
                + "nombre_cotizante = '" + nombre_comprador + "',"
                + "direccion_cotizante = '" + direccion_comprador + "',"
                + "telefono_cotizante = '" + telefono_comprador + "',"
                + "correo_electronico_cotizante = '" + email_comprador + "' WHERE cod_cotizacion=" + cod_cotizacion + ";";

        System.out.println(query);
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();
        try {

            this.sentencia = this.conexion.createStatement();
            int msgexito = this.sentencia.executeUpdate(query);

            if (msgexito == 1) {
                mensaje = "Modificación ingresada con exito";
            }
        } catch (SQLException ex) {
            mensaje = "No se pudo realizar el registro :" + ex.getMessage();
        }
        fachada.CerrarBd();
        return mensaje;
    }

    ResultSet ConsultarDatosCotizacion(String cod_cotizacion,String cedula_comprador, String cedula_vendedor, String marca, String modelo) {
        ResultSet datos_cotizacion = null;
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();

        String query = "SELECT * "
                + " FROM sedes s(cod_sede,nombreS,direccionS,telefonoS,ciudadS,cedula_gerente) "
                + " NATURAL JOIN empleados e(cedulaV,nombreV,telefonoV,direccionV,emailV,estadoE,perfil,usuario,contrasena,fecha_ingreso,cod_sede) "
                + " NATURAL JOIN cotizaciones c(cod_cotizacion,cedulaV,marca,modelo,precio,color,"
                + " fecha,cedula_cotizante,nombre_cotizante,direccion_cotizante,telefono_cotizante,correo_electronico_cotizante)"
                + " WHERE cedula_cotizante = '"+cedula_comprador+"'"
                + " AND cod_cotizacion = "+cod_cotizacion+" "
                + " AND cedulaV = '"+cedula_vendedor+"' "
                + " AND marca = '"+marca+"' AND modelo = '"+modelo+"';";
        System.out.println(query);
         try {
            this.sentencia = this.conexion.createStatement();
            datos_cotizacion = this.sentencia.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error al consultar datos factura "+ ex);
        }

        return datos_cotizacion;
    }
    
    
    ResultSet maxCotizaciones() {
        ResultSet datos_cotizacion = null;
        Fachada fachada = new Fachada();
        fachada.ConectarBd();
        this.conexion = fachada.getConexion();

        String query = "SELECT MAX(cod_cotizacion) FROM cotizaciones;";
         try {
            this.sentencia = this.conexion.createStatement();
            datos_cotizacion = this.sentencia.executeQuery(query);
        } catch (SQLException ex) {
            System.out.println("Error al consultar datos"+ ex);
        }

        return datos_cotizacion;
    }
}
