/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;



import Datos.ConsultasRepuesto;
import Registros.CrearOrdenTrabajo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


/**
 *
 * @author ivanmvr
 */
public class ManejoDatosOrdenTrabajo {

    public ManejoDatosOrdenTrabajo() {
    }
    
    
    public void cargarDatosRepuestos(JTable comboBox, String dato) {
        
        ConsultasRepuesto consulta = new ConsultasRepuesto();
        ResultSet resultado = null;
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.setColumnIdentifiers(new String[]{
            "Nombre", "Cantidad disponible"
        });
        switch (dato) {
            case "repuestos":
                resultado = consulta.buscarNombresRepuestos();
                break;
            default:
                break;
        }

        try {
            while (resultado.next()) {
                ArrayList<Object> datosFila = new ArrayList();
                datosFila.add(resultado.getString("nombre"));
                
                datosFila.add(resultado.getString("cantidad"));
                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
            //e.printStackTrace();
        }
        comboBox.setModel(modelo);
    }
    
    public String crearOrdenTrabajo(int dia, int mes, int anho, String valor, String nombre,
	String cedula, String direccion, String telefono, String placa_vehiculo, String sede, String descripcion){ 
        
        ConsultasOrdenTrabajo consulta = new ConsultasOrdenTrabajo();
        

        
        String fecha = anho + "-" + mes + "-" + dia;
        
        String mensaje = consulta.registrarOrdenTrabajo(fecha,valor, nombre, cedula, direccion,telefono, placa_vehiculo,
                sede, descripcion);
        return mensaje;
        
    }
    
    public void ModificarCantidadRepuestosParaOrdenDeTrabajo(ArrayList<String> repuestosAUtilizar, ArrayList<String> cantidadRepuestosAUtilizar){
        ConsultasRepuesto consulta = new ConsultasRepuesto();
        ConsultasRepuesto modificacionRepuestosAUtilizarEnOrdenTrabajo = new ConsultasRepuesto();
        for (int i = 0; i < repuestosAUtilizar.size(); i++) {
            modificacionRepuestosAUtilizarEnOrdenTrabajo.modificarInformacionRepuesto(repuestosAUtilizar.get(i), cantidadRepuestosAUtilizar.get(i));
        }
        
        
    }

    

    public void consultarTodasOrdenesPorPlaca(String placa, JTable tablaOrdenesPlacaIngresada) throws SQLException {
       ConsultasOrdenTrabajo consulta = new ConsultasOrdenTrabajo();
        ResultSet datos = consulta.traerTodasOrdenesPlacaIngresada(placa);
        System.out.println("");
        
        
         DefaultTableModel modelo = new DefaultTableModel(){

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
         };
        modelo.setColumnIdentifiers(new String[]{
            "Número orden", "Fecha","Nombre cliente"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de usuarios

                ArrayList<Object> datosFila = new ArrayList();
                datosFila.add(datos.getString("cod_orden"));
                datosFila.add(datos.getString("fecha"));
                datosFila.add(datos.getString("nombre_cliente"));
                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(tablaOrdenesPlacaIngresada, "No se encontraron resultados para la placa ingresada",
                    "ERROR",JOptionPane.ERROR_MESSAGE);
        }
        
        tablaOrdenesPlacaIngresada.setModel(modelo);
       
        
        
    }
    
    public ArrayList<ArrayList<String>> consultarOrdenEnEspecifico( String cod_orden, String placa){
       ArrayList<ArrayList<String>> resultado = new ArrayList<>();
       ArrayList<String> infoOrden = new ArrayList<>();
       ArrayList<String> repuestosUtilizados = new ArrayList<>();
       ArrayList<String> cantidadRepuestosUtilizados = new ArrayList<>();
        ResultSet consultaInfoOrden = new ConsultasOrdenTrabajo().traerInformacionOrdenEspecifica(cod_orden,placa);
        ResultSet consulta_repuestos_ordenes = new ConsultasOrdenTrabajo().traerRepuestosOrdenEspecifica(cod_orden);
        
        
        try {
            consultaInfoOrden.next();
            //while (consultaInfoOrden.next()) {
                infoOrden.add(consultaInfoOrden.getString("cod_orden"));
                
                infoOrden.add(consultaInfoOrden.getString("fecha"));
                System.out.println("El fecha de la orden aquí es "+infoOrden.get(1)+"\n");
                infoOrden.add(consultaInfoOrden.getString("valor"));
                infoOrden.add(consultaInfoOrden.getString("nombre_cliente"));
                infoOrden.add(consultaInfoOrden.getString("cedula"));
                infoOrden.add(consultaInfoOrden.getString("direccion_cliente"));
                infoOrden.add(consultaInfoOrden.getString("telefono_cliente"));
                infoOrden.add(consultaInfoOrden.getString("placa_vehiculo"));
                infoOrden.add(consultaInfoOrden.getString("cod_sede"));
                infoOrden.add(consultaInfoOrden.getString("descripcion"));
               
            
            resultado.add(new ArrayList<String>(infoOrden));
            try {
                while (consulta_repuestos_ordenes.next()) {
                    repuestosUtilizados.add(consulta_repuestos_ordenes.getString("nombre"));
                    cantidadRepuestosUtilizados.add(consulta_repuestos_ordenes.getString("cantidad"));
                }
               resultado.add(new ArrayList<String>(repuestosUtilizados));
               resultado.add(new ArrayList<String>(cantidadRepuestosUtilizados));
            } catch (NullPointerException e) {

            }
            
            
            
        }catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
        
        
        
        return resultado;
    }

 
    public String ModificarOrdenTrabajo(String cod_orden,int dia, int mes, int anho, String valor, String nombre,
	String cedula, String direccion, String telefono, String placa_vehiculo, String sede, String descripcion){ 
        
        ConsultasOrdenTrabajo consulta = new ConsultasOrdenTrabajo();
        

        
        String fecha = anho + "-" + mes + "-" + dia;
        
        String mensaje = consulta.ModificarOrdenTrabajo(cod_orden,fecha,valor, nombre, cedula, direccion,telefono, placa_vehiculo,
                sede, descripcion);
        return mensaje;
    
    }

    

        
    
    
   
    
}
