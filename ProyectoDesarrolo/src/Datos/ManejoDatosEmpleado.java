/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Consultas.ResultadoConsultaEmpleado;
import Modificacion.RealizarModificacionEmpleado;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ivanmvr
 */
public class ManejoDatosEmpleado {

    public ManejoDatosEmpleado() {
    }


    public String RealizarRegistroEmpleado(String nombre, String cedula, String usuario,
            String direccion, String email, String telefono, String contrasena,
            int cargoSeleccionado, String sede) {

        String estado = "ACTIVO";
        String cargo = "";
        switch (cargoSeleccionado) {
            case 1:
                cargo = "GERENTE";
                break;
            case 2:
                cargo = "VENDEDOR";
                break;
            case 3:
                cargo = "JEFE TALLER";
                break;
            default:
                cargo = "-1";
                break;

        }

        ConsultasEmpleado consulta = new ConsultasEmpleado();
        sede = new Datos.Consultas().TraerCodigoSedeNombre(sede);

        Calendar c = new GregorianCalendar();
        String dia = Integer.toString(c.get(Calendar.DATE));
        String mes = Integer.toString(c.get(Calendar.MONTH) + 1);
        String annio = Integer.toString(c.get(Calendar.YEAR));
        String fecha = annio + "-" + mes + "-" + dia;
        
        
        String mensaje = consulta.IngresarEmpleado(cedula, nombre, telefono,direccion, email,
                estado, cargo, usuario, contrasena, fecha, sede);
        return mensaje;
    }

    public void ObtenerDatosEmpleados(JTable tabla) {

        ArrayList<ArrayList<Object>> informacion = new ArrayList();
        ResultSet datos = new ConsultasEmpleado().BuscarEmpleados();

        DefaultTableModel modelo = new DefaultTableModel(){

            @Override
            public boolean isCellEditable(int row, int column) {
                //Only the third column
                return false;
            }
         };
        modelo.setColumnIdentifiers(new String[]{
            "Nombre", "Cedula", "Estado"
        });

        try {
            while (datos.next()) {
                //Cuadrar tabla para cualquier cantidad de usuarios

                ArrayList<Object> datosFila = new ArrayList();

                datosFila.add(datos.getString("nombre"));
                datosFila.add(datos.getString("cedula"));
                datosFila.add(datos.getString("estado"));
                modelo.addRow(datosFila.toArray());

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
        
        tabla.setModel(modelo);
    }

    public ResultadoConsultaEmpleado CargarDatosEmpleadoAConsultar(JFrame padre, String cedula) {

        ResultadoConsultaEmpleado consultaUsuarioEspecifico = null;

        try {
             
            ConsultasEmpleado consulta = new ConsultasEmpleado();
            ResultSet datos = consulta.BuscarEmpleado("cedula","'"+cedula+"'");

            datos.next();
            String[] fecha = datos.getString("fecha_ingreso").split("-");
            
            consultaUsuarioEspecifico = new ResultadoConsultaEmpleado(padre,
                    fecha[2], fecha[1], fecha[0],datos.getString("nombre"), datos.getString("cedula"),
                    datos.getString("perfil"), datos.getString("cod_sede"), datos.getString("estado"),
                    datos.getString("direccion"),datos.getString("telefono"), datos.getString("email"),
                    datos.getString("usuario"));

        } catch (SQLException ex) {
            System.err.println(ex);
        }

        return consultaUsuarioEspecifico;
    }
    
    public RealizarModificacionEmpleado CargarDatosEmpleadoAModificar(JFrame padre, String cedula) {

        RealizarModificacionEmpleado modificacion = null;

        try {
            ConsultasEmpleado consulta = new ConsultasEmpleado();
            ResultSet datos = consulta.BuscarEmpleado("cedula", "'"+cedula+"'");

            datos.next();
            modificacion = new RealizarModificacionEmpleado(padre,
                    datos.getString("nombre"), datos.getString("cedula"),
                    datos.getString("perfil"), datos.getString("cod_sede"), datos.getString("direccion"),
                    datos.getString("telefono"), datos.getString("email"),datos.getString("estado"),
                    datos.getString("usuario"),datos.getString("contrasena"));

        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        return modificacion;
    }

    public String RealizarModificacionEmpleado(String nombre, String cedula, String usuario, String direccion,
            String email, String telefono, String contrasena, String cargo,
            String sede, String estado) {
        ConsultasEmpleado consulta = new ConsultasEmpleado();
        String cod_sede = new Datos.Consultas().TraerCodigoSedeNombre(sede);
        
        
        
        String mensaje = consulta.ModificarEmpleado(cedula, nombre, telefono,direccion, email,
                estado, cargo, usuario, contrasena, cod_sede);
        return mensaje;
    }

     
    public void ColocarGerentesLibres(JComboBox comboBox){
        ConsultasEmpleado consulta = new ConsultasEmpleado();
        ResultSet resultado = null;

       
                resultado = consulta.BuscarGerentesLibres();
        
        

        try {
            while (resultado.next()) {
                comboBox.addItem(resultado.getString("cedula"));

            }
        } catch (SQLException ex) {
            System.out.println("sqlE " + ex.getSQLState());
        }catch (NullPointerException ex){
            comboBox.setEnabled(false);
        }
    
    }
    
    
    public String TraerSedeEmpleado(String Cedula){
        String resultado = "";

        ResultSet datos = new ConsultasEmpleado().BuscarEmpleado("cedula", "'" + Cedula + "'");

        try {
            while (datos.next()) {

                resultado = datos.getString("cod_sede");

            }

        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }

        return resultado;
  
    }
    
}
