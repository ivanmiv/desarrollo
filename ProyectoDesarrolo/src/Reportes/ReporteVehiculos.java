/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Ivan
 */
public class ReporteVehiculos {

    public ReporteVehiculos() {

         
        ResultSet resultado = null;

        try {
            
            resultado = new Datos.ManejoDatosReporte().ConsultarVehiculos();
  
            if (resultado.getRow() == 0 ) {
                
                System.out.println("vacio");
            } else {
                System.out.println("no vacio");
            }
            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteVehiculos.jasper");
            
            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);     
            
            Map param = new HashMap(); 
            
            
            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));

            JasperExportManager.exportReportToPdfFile(jasperprint, "Vehiculos"+".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle("Vehiculos");
            visor.setVisible(true);
            visor.setFitWidthZoomRatio();
            
        } catch (JRException ex) {
            System.out.println("Error generando reporte " + ex);

        } catch (NullPointerException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        } catch (SQLException ex) {
            System.err.println(ex);
        }

    }
}
