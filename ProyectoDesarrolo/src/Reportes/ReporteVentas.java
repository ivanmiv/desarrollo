/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Ivan
 */
public class ReporteVentas {

    
    public ReporteVentas(int anho) {

         
        ResultSet resultado = null;

        try {
            
            resultado = new Datos.ManejoDatosReporte().ConsultarVentasYear(anho);
  
            if (resultado.getRow() == 0 ) {
                
                System.out.println("vacio");
            } else {
                System.out.println("no vacio");
            }
            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteAnho.jasper");
            
            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);     
            
            Map param = new HashMap();  
            param.put("SEDE", "AUTOS ABC ");
            param.put("TITULO", "REPORTE DE VENTAS POR AÑO");
            param.put("TITULO_COLUMNA", "AUTOS VENDIDOS");
            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));

            JasperExportManager.exportReportToPdfFile(jasperprint, "VentasYear-"+anho+".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle("VentasYear-"+anho);
            visor.setVisible(true);
            visor.setFitWidthZoomRatio();
            
        } catch (JRException ex) {
            System.out.println("Error generando reporte " + ex);

        } catch (NullPointerException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        } catch (SQLException ex) {
            System.err.println(ex);
        }

    }
    
    
    public ReporteVentas(int anho,String sede) {

         
        ResultSet resultado = null;

        try {
            
            resultado = new Datos.ManejoDatosReporte().ConsultarVentasYear(anho,sede);
  
            if (resultado.getRow() == 0 ) {
                
                System.out.println("vacio");
            } else {
                System.out.println("no vacio");
            }
            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteAnho.jasper");
            
            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);     
            
            Map param = new HashMap();  
            param.put("SEDE", "AUTOS ABC "+sede);            
            param.put("TITULO", "REPORTE DE VENTAS POR AÑO");
            param.put("TITULO_COLUMNA", "AUTOS VENDIDOS");
            
            
            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));

            JasperExportManager.exportReportToPdfFile(jasperprint, "VentasYear-"+anho+"-"+sede+".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle("VentasYear-"+anho);
            visor.setVisible(true);
            visor.setFitWidthZoomRatio();
            
        } catch (JRException ex) {
            System.out.println("Error generando reporte " + ex);

        } catch (NullPointerException e) {
            System.err.println(e);
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        } catch (SQLException ex) {
            System.err.println(ex);
        }

    }
    
    
    public ReporteVentas(int anho, int mes) {
            
         
        ResultSet resultado = null;

        try {
            
            resultado = new Datos.ManejoDatosReporte().ConsultarVentasYearMonth(mes,anho);
  
            if (resultado.getRow() == 0 ) {
                
                System.out.println("vacio");
            } else {
                System.out.println("no vacio");
            }
            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteMes.jasper");
            
            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);     
            
            Map param = new HashMap(); 
            param.put("SEDE", "AUTOS ABC ");         
            param.put("TITULO", "REPORTE DE VENTAS POR MES");
            param.put("TITULO_COLUMNA", "AUTOS VENDIDOS");
            
            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));

            JasperExportManager.exportReportToPdfFile(jasperprint, "Ventas-"+anho+"-"+mes+".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle("Ventas-"+anho+"-"+mes);
            visor.setVisible(true);
            visor.setFitWidthZoomRatio();
            
        } catch (JRException ex) {
            System.out.println("Error generando factura " + ex);

        } catch (NullPointerException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        } catch (SQLException ex) {
            System.err.println(ex);
        }

    }
    
    
    public ReporteVentas(int anho, int mes,String sede) {
            
         
        ResultSet resultado = null;

        try {
            
            resultado = new Datos.ManejoDatosReporte().ConsultarVentasYearMonth(mes,anho,sede);
  
            if (resultado.getRow() == 0 ) {
                
                System.out.println("vacio");
            } else {
                System.out.println("no vacio");
            }
            InputStream template = getClass().getResourceAsStream("/Reportes/ReporteMes.jasper");
            
            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);     
            
            Map param = new HashMap();  
            param.put("SEDE", "AUTOS ABC "+sede);         
            param.put("TITULO", "REPORTE DE VENTAS POR MES");
            param.put("TITULO_COLUMNA", "AUTOS VENDIDOS");
            
            
            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(resultado));

            JasperExportManager.exportReportToPdfFile(jasperprint, "Ventas-"+anho+"-"+mes+"-"+sede+".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            visor.setTitle("Ventas-"+anho+"-"+mes+"-"+sede);
            visor.setVisible(true);
            visor.setFitWidthZoomRatio();
            
        } catch (JRException ex) {
            System.out.println("Error generando reporte " + ex);

        } catch (NullPointerException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, "No se encontraron resultados");
        } catch (SQLException ex) {
            System.err.println(ex);
        }

    }
    
}
