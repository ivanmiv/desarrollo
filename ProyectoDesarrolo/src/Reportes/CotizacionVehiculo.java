/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Ivan
 */
public class CotizacionVehiculo {

    
    public CotizacionVehiculo(String cod_cotizacion,String cedula_comprador, String cedula_vendedor, String marca,String modelo) {
        String cod_venta = "";

        try {
            InputStream template = getClass().getResourceAsStream("/Reportes/CotizacionVehiculo.jasper");

            JasperReport reporte = (JasperReport) JRLoader.loadObject(template);

            Map param = new HashMap();

            ResultSet datos = new Datos.ManejoDatosCotizacion().GenerarDatosCotizacion(cod_cotizacion,cedula_comprador, cedula_vendedor,marca,modelo);

            datos.next();
            cod_venta = datos.getString("cod_cotizacion");
            String titulo = "Cotizacion-" + cod_venta;
            
            ResultSet datos1 = new Datos.ManejoDatosCotizacion().GenerarDatosCotizacion(cod_cotizacion,cedula_comprador, cedula_vendedor,marca,modelo);

            JasperPrint jasperprint = JasperFillManager.fillReport(reporte, param, new JRResultSetDataSource(datos1));

            JasperExportManager.exportReportToPdfFile(jasperprint, titulo + ".pdf");

            JasperViewer visor = new JasperViewer(jasperprint, false);
            
                     
            visor.setTitle(titulo);
            visor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("Error generando factura " + ex);
        } catch (SQLException ex) {
            System.out.println("Error generando factura " + ex);
            
        }
    }

}
