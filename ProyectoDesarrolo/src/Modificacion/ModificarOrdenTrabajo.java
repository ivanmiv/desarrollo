/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modificacion;

import Consultas.*;
import Datos.ManejoDatosOrdenTrabajo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author JoseAlejandro
 */
public class ModificarOrdenTrabajo extends javax.swing.JFrame {

    /**
     * Creates new form ConsultaOrdenTrabajo
     */
    JFrame padre;
    public ModificarOrdenTrabajo(JFrame padre) {
        initComponents();
        this.padre = padre;
        setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jlTitulo = new javax.swing.JLabel();
        jlPlacaVehiculo = new javax.swing.JLabel();
        jtfPlacaABuscar = new javax.swing.JTextField();
        jbBuscarTodasOrdenesPlacaIngresada = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtResultadosPlacaIngresada = new javax.swing.JTable();
        jlResultados = new javax.swing.JLabel();
        jbConsultarOrdenDeseada = new javax.swing.JButton();
        jlBotonAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ab-cars-vector-logo.png"))); // NOI18N

        jlTitulo.setFont(jlTitulo.getFont().deriveFont(jlTitulo.getFont().getSize()+14f));
        jlTitulo.setText("Modificar orden de trabajo");

        jlPlacaVehiculo.setText("Placa del vehículo:");

        jbBuscarTodasOrdenesPlacaIngresada.setText("Buscar");
        jbBuscarTodasOrdenesPlacaIngresada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbBuscarTodasOrdenesPlacaIngresadaMouseClicked(evt);
            }
        });

        jtResultadosPlacaIngresada.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Número orden", "Fecha", "Persona que hizo el registro"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtResultadosPlacaIngresada.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jtResultadosPlacaIngresada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtResultadosPlacaIngresadaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtResultadosPlacaIngresada);

        jlResultados.setText("Resultados: (seleccione la que desea consultar y a continuación presione el botón modificar)");

        jbConsultarOrdenDeseada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonmodificar.jpg"))); // NOI18N
        jbConsultarOrdenDeseada.setEnabled(false);
        jbConsultarOrdenDeseada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbConsultarOrdenDeseadaActionPerformed(evt);
            }
        });

        jlBotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlBotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAtrasMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(213, 213, 213)
                .addComponent(jlBotonAtras)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jlTitulo))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jlPlacaVehiculo)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jtfPlacaABuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jbBuscarTodasOrdenesPlacaIngresada)))))
                        .addGap(0, 14, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlResultados, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jbConsultarOrdenDeseada, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jlTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlPlacaVehiculo)
                    .addComponent(jtfPlacaABuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbBuscarTodasOrdenesPlacaIngresada))
                .addGap(11, 11, 11)
                .addComponent(jlResultados)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbConsultarOrdenDeseada, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlBotonAtras))
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbBuscarTodasOrdenesPlacaIngresadaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbBuscarTodasOrdenesPlacaIngresadaMouseClicked
       ManejoDatosOrdenTrabajo consultaPlaca = new ManejoDatosOrdenTrabajo();
       jtfPlacaABuscar.setText(jtfPlacaABuscar.getText().toString().toUpperCase());
        try {
            consultaPlaca.consultarTodasOrdenesPorPlaca( jtfPlacaABuscar.getText().toString().toUpperCase(), jtResultadosPlacaIngresada );
        } catch (SQLException ex) {
            
        }
       
    }//GEN-LAST:event_jbBuscarTodasOrdenesPlacaIngresadaMouseClicked

    private void jlBotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonAtrasMouseClicked

    private void jtResultadosPlacaIngresadaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtResultadosPlacaIngresadaMouseClicked
        for (int i = 0; i < jtResultadosPlacaIngresada.getRowCount(); i++) {
            if(jtResultadosPlacaIngresada.isRowSelected(i)){
                jbConsultarOrdenDeseada.setEnabled(true);
            }
        }
    }//GEN-LAST:event_jtResultadosPlacaIngresadaMouseClicked

    private void jbConsultarOrdenDeseadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbConsultarOrdenDeseadaActionPerformed
        String cod_orden = jtResultadosPlacaIngresada.getValueAt(jtResultadosPlacaIngresada.getSelectedRow(),0).toString();
        ArrayList<ArrayList<String>> informacionDeConsulta = new ManejoDatosOrdenTrabajo().consultarOrdenEnEspecifico(cod_orden,jtfPlacaABuscar.getText());
        ArrayList<String> informacionOrden = informacionDeConsulta.get(0);
        ArrayList<String> repuestosAUtilizar = null;
            ArrayList<String> cantidadRepuestosAUtilizar = null;
        try {
            if(informacionDeConsulta.size()==3)
            {
            repuestosAUtilizar = informacionDeConsulta.get(1);
            cantidadRepuestosAUtilizar = informacionDeConsulta.get(2);
            }
            new ResultadoModificacionOrdenTrabajo(this,
                    informacionOrden.get(0).toString(),//cod_orden
                    informacionOrden.get(1).toString(),//fecha
                    informacionOrden.get(2).toString(),//valor
                    informacionOrden.get(3).toString(),//nombre_cliente
                    informacionOrden.get(4).toString(),//cedula_cliente
                    informacionOrden.get(5).toString(),//direccion_cliente
                    informacionOrden.get(6).toString(),//telefono_cliente
                    informacionOrden.get(7).toString(),//placa
                    informacionOrden.get(8).toString(),//sede
                    informacionOrden.get(9).toString(),//descripcion
                    repuestosAUtilizar,
                    cantidadRepuestosAUtilizar);
        } catch (SQLException ex) {
            Logger.getLogger(ModificarOrdenTrabajo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jbConsultarOrdenDeseadaActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(ConsultaOrdenTrabajo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(ConsultaOrdenTrabajo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(ConsultaOrdenTrabajo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(ConsultaOrdenTrabajo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new ConsultaOrdenTrabajo().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbBuscarTodasOrdenesPlacaIngresada;
    private javax.swing.JButton jbConsultarOrdenDeseada;
    private javax.swing.JLabel jlBotonAtras;
    private javax.swing.JLabel jlPlacaVehiculo;
    private javax.swing.JLabel jlResultados;
    private javax.swing.JLabel jlTitulo;
    private javax.swing.JTable jtResultadosPlacaIngresada;
    private javax.swing.JTextField jtfPlacaABuscar;
    // End of variables declaration//GEN-END:variables
}
