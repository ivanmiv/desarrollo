package Modificacion;

import Datos.ManejoDatosVehiculo;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class ModificarVehiculo extends javax.swing.JFrame {

   JFrame padre;
    public ModificarVehiculo(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        
        new ManejoDatosVehiculo().ObtenerDatosVehiculosDisponibles(jtableModificarVehiculo);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtableModificarVehiculo = new javax.swing.JTable();
        jlIconUser = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();
        jlbotonModificar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Modificar Vehiculo");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jtableModificarVehiculo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Marca", "Cedula", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jtableModificarVehiculo.setGridColor(new java.awt.Color(255, 255, 255));
        jtableModificarVehiculo.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtableModificarVehiculo);

        jlIconUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/iconVehiculos.jpg"))); // NOI18N

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });

        jlbotonModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonContinuar.png"))); // NOI18N
        jlbotonModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonModificarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(190, 190, 190)
                .addComponent(jlbotonModificar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(182, 182, 182)
                        .addComponent(jlIconUser)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jlbotonAtras)))
                .addGap(18, 18, 18))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlIconUser)
                    .addComponent(jlbotonAtras))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jlbotonModificar)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos de Botones
    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
       this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
                
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlbotonModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonModificarMouseClicked
        this.setVisible(false);    
        int row=jtableModificarVehiculo.getSelectedRow();
        String chasis = (String)jtableModificarVehiculo.getModel().getValueAt(row, 0);
        RealizarModificacionVehiculo u =  new ManejoDatosVehiculo().CargarDatosvehiculoAModificar(this,chasis);
        u.setVisible(true);
    }//GEN-LAST:event_jlbotonModificarMouseClicked

  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jlIconUser;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlbotonModificar;
    private javax.swing.JTable jtableModificarVehiculo;
    // End of variables declaration//GEN-END:variables
}
