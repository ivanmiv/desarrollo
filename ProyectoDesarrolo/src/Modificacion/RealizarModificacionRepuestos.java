package Modificacion;

import Datos.ManejoDatosRepuesto;
import Datos.ManejoDatosSede;
import Datos.ValidarNumero;
import javax.swing.JFrame;
import javax.swing.JOptionPane;




public class RealizarModificacionRepuestos extends javax.swing.JFrame {

    JFrame padre;
    public RealizarModificacionRepuestos(JFrame padre,String codigo,String nombre,String precio,String cantidad,
                                String descripcion,String sede) {
        
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        new ManejoDatosSede().CargarDatosComboBox(jcbSede, "sedes");
        jcbSede.setSelectedItem(sede);
        this.jtfCodigo.setText(codigo);
        this.jtfNombre.setText(nombre);
        this.jtfCantidadR.setText(cantidad);
        this.jtfPrecioR.setText(precio);
        this.jtaDescripcionR.setText(descripcion);
        this.jcbSede.setSelectedItem(sede);
        
        
        jtfCantidadR.setInputVerifier(new ValidarNumero(jlValidacion, 100));
        jtfPrecioR.setInputVerifier(new ValidarNumero(jlValidacion, 100));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCrearUser = new javax.swing.JPanel();
        jtfCantidadR = new javax.swing.JTextField();
        jlIconRepuestos = new javax.swing.JLabel();
        jlNombreR = new javax.swing.JLabel();
        jlCantidadR = new javax.swing.JLabel();
        jlSedeR = new javax.swing.JLabel();
        jlDescripcion = new javax.swing.JLabel();
        jlPrecio = new javax.swing.JLabel();
        jlBotonModificar = new javax.swing.JLabel();
        jlAtras = new javax.swing.JLabel();
        jtfPrecioR = new javax.swing.JTextField();
        jtfNombre = new javax.swing.JTextField();
        jcbSede = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtaDescripcionR = new javax.swing.JTextArea();
        jlCodigo = new javax.swing.JLabel();
        jtfCodigo = new javax.swing.JTextField();
        jlValidacion = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Modificar Repuesto");
        setResizable(false);

        jPanelCrearUser.setBackground(new java.awt.Color(255, 255, 255));

        jlIconRepuestos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/IconRepuestos.jpg"))); // NOI18N

        jlNombreR.setText("Nombre:");

        jlCantidadR.setText("Cantidad:");

        jlSedeR.setText("Sede:");

        jlDescripcion.setText("Descripcion:");

        jlPrecio.setText("Precio:");

        jlBotonModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonmodificar.jpg"))); // NOI18N
        jlBotonModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonModificarMouseClicked(evt);
            }
        });

        jlAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlAtrasMouseClicked(evt);
            }
        });

        jtaDescripcionR.setColumns(20);
        jtaDescripcionR.setRows(5);
        jScrollPane1.setViewportView(jtaDescripcionR);

        jlCodigo.setText("Codigo:");

        jtfCodigo.setEditable(false);

        jlValidacion.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanelCrearUserLayout = new javax.swing.GroupLayout(jPanelCrearUser);
        jPanelCrearUser.setLayout(jPanelCrearUserLayout);
        jPanelCrearUserLayout.setHorizontalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(221, 221, 221)
                        .addComponent(jlBotonModificar))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                .addComponent(jlPrecio)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jtfPrecioR, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                .addComponent(jlCantidadR)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jtfCantidadR, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                                .addComponent(jlNombreR)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jtfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlSedeR)
                                    .addComponent(jlDescripcion))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jcbSede, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                                .addComponent(jlCodigo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jtfCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(115, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconRepuestos)
                        .addGap(58, 58, 58)
                        .addComponent(jlAtras)
                        .addGap(43, 43, 43))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlValidacion)
                        .addGap(235, 235, 235))))
        );
        jPanelCrearUserLayout.setVerticalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jlAtras))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jlIconRepuestos)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCodigo)
                    .addComponent(jtfCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlNombreR)
                    .addComponent(jtfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCantidadR)
                    .addComponent(jtfCantidadR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfPrecioR, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlPrecio))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbSede, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlSedeR))
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jlDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(37, 37, 37)
                .addComponent(jlValidacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlBotonModificar)
                .addContainerGap())
        );

        getContentPane().add(jPanelCrearUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
    }//GEN-LAST:event_jlAtrasMouseClicked

    private void jlBotonModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonModificarMouseClicked
             
        String codigo = jtfCodigo.getText();
        String nombre = jtfNombre.getText().trim();
        String cantidad = jtfCantidadR.getText().trim();
        String precio = jtfPrecioR.getText().trim();
        String sede = (String)jcbSede.getSelectedItem();
        String descripcion = jtaDescripcionR.getText();
        
        if(jlValidacion.getText().length()!=0){
            return;
        }
        if (nombre.length() == 0
            || descripcion.length() == 0
            || jtfPrecioR.getText().trim().length() == 0
            || sede.equalsIgnoreCase("")
            ) {
            JOptionPane.showMessageDialog(this, "Por favor complete todos los campos", "Ingrso de repuesto de vehiculo", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        
        
        String mensaje = new ManejoDatosRepuesto().RealizarModificacionRepuesto(codigo, nombre, precio
                , cantidad, descripcion, sede);
        JOptionPane.showMessageDialog(this, mensaje, "Modificacion de repuesto", JOptionPane.INFORMATION_MESSAGE);
            this.padre.setVisible(true);
            this.dispose();
    }//GEN-LAST:event_jlBotonModificarMouseClicked

 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelCrearUser;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox jcbSede;
    private javax.swing.JLabel jlAtras;
    private javax.swing.JLabel jlBotonModificar;
    private javax.swing.JLabel jlCantidadR;
    private javax.swing.JLabel jlCodigo;
    private javax.swing.JLabel jlDescripcion;
    private javax.swing.JLabel jlIconRepuestos;
    private javax.swing.JLabel jlNombreR;
    private javax.swing.JLabel jlPrecio;
    private javax.swing.JLabel jlSedeR;
    private javax.swing.JLabel jlValidacion;
    private javax.swing.JTextArea jtaDescripcionR;
    private javax.swing.JTextField jtfCantidadR;
    private javax.swing.JTextField jtfCodigo;
    private javax.swing.JTextField jtfNombre;
    private javax.swing.JTextField jtfPrecioR;
    // End of variables declaration//GEN-END:variables
}
