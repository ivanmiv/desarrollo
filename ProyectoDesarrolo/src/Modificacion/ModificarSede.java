package Modificacion;


import Datos.ManejoDatosSede;
import javax.swing.JFrame;


public class ModificarSede extends javax.swing.JFrame {
    JFrame padre;
    public ModificarSede(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        
        new ManejoDatosSede().ObtenerDatosTodaslasSedes(jtSedes);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLIconSede = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtSedes = new javax.swing.JTable();
        jlBotonContinuar = new javax.swing.JLabel();
        jlBotonAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Modificar Sede");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLIconSede.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/SedeIcon.png"))); // NOI18N

        jtSedes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Nombre", "cc. Gerente"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jtSedes.setGridColor(new java.awt.Color(255, 255, 255));
        jtSedes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtSedes);

        jlBotonContinuar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonContinuar.png"))); // NOI18N
        jlBotonContinuar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonContinuarMouseClicked(evt);
            }
        });

        jlBotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlBotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAtrasMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLIconSede)
                        .addGap(188, 188, 188))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jlBotonAtras)
                        .addGap(49, 49, 49))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(178, 178, 178)
                        .addComponent(jlBotonContinuar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 402, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 112, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlBotonAtras)
                .addGap(3, 3, 3)
                .addComponent(jLIconSede)
                .addGap(33, 33, 33)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlBotonContinuar)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlBotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonAtrasMouseClicked

    private void jlBotonContinuarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonContinuarMouseClicked
        this.setVisible(false);    
        int row=jtSedes.getSelectedRow();
        String codsede = (String)jtSedes.getModel().getValueAt(row, 1);
        RealizarModificacionSede s =  new ManejoDatosSede().CargarDatosSedeAModificar(this,codsede);
        s.setVisible(true);
        
    }//GEN-LAST:event_jlBotonContinuarMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLIconSede;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jlBotonAtras;
    private javax.swing.JLabel jlBotonContinuar;
    private javax.swing.JTable jtSedes;
    // End of variables declaration//GEN-END:variables
}
