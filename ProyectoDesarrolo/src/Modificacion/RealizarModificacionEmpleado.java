package Modificacion;

import Datos.ManejoDatosEmpleado;
import Datos.Encriptar;
import Datos.ManejoDatosSede;
import Datos.ValidarCantidadCaracteres;
import Datos.ValidarEmail;
import Datos.ValidarNumero;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class RealizarModificacionEmpleado extends javax.swing.JFrame {

    JFrame padre;

    public RealizarModificacionEmpleado(JFrame padre, String nombre, String cedula, String cargo,
            String sede, String direccion, String telefono, String email, String estado, String user,String pass) {
        this.padre = padre;
        initComponents();
        setLocationRelativeTo(null);
        new ManejoDatosSede().CargarDatosComboBox(jcbNombreSede, "sedes");
        jcbNombreSede.setSelectedItem(new ManejoDatosSede().TraerNombreSedeCodigo(Integer.parseInt(sede)));
        jcbCargoUser.setSelectedItem(cargo);
        jcbEstado.setSelectedItem(estado);
        jtfTelefono.setText(telefono);
        jtfCedulaUser.setText(cedula);
        jtfDireccion.setText(direccion);
        jtfEmailUser.setText(email);
        jtfNombreUser.setText(nombre);
        jtfDireccion.setText(direccion);
        jtfUser.setText(user);
        jPassword.setText(new Encriptar().DesencriptarMd5(pass));
        
          
        jtfNombreUser.setInputVerifier(new ValidarCantidadCaracteres(jlValidacion, 100));
        jtfTelefono.setInputVerifier(new ValidarNumero(jlValidacion, 10));
        jtfEmailUser.setInputVerifier(new ValidarEmail(jlValidacion, 100));
        jtfCedulaUser.setInputVerifier(new ValidarNumero(jlValidacion, 100));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jPanelCrearUser = new javax.swing.JPanel();
        jlIconUSer = new javax.swing.JLabel();
        jlNombreUser = new javax.swing.JLabel();
        jlCedula = new javax.swing.JLabel();
        jlCargo = new javax.swing.JLabel();
        jlSede = new javax.swing.JLabel();
        jlDireccion = new javax.swing.JLabel();
        jlTelefono = new javax.swing.JLabel();
        jlBotonModificarUser = new javax.swing.JLabel();
        jlAtras = new javax.swing.JLabel();
        jtfCedulaUser = new javax.swing.JTextField();
        jtfEmailUser = new javax.swing.JTextField();
        jtfDireccion = new javax.swing.JTextField();
        jtfNombreUser = new javax.swing.JTextField();
        jcbCargoUser = new javax.swing.JComboBox();
        jcbNombreSede = new javax.swing.JComboBox();
        jlEmail = new javax.swing.JLabel();
        jtfTelefono = new javax.swing.JTextField();
        jcbEstado = new javax.swing.JComboBox();
        jlEstado = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jtfUser = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPassword = new javax.swing.JPasswordField();
        jlValidacion = new javax.swing.JLabel();

        jToolBar1.setRollover(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Modificar Usuario");
        setResizable(false);

        jPanelCrearUser.setBackground(new java.awt.Color(255, 255, 255));

        jlIconUSer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/contact-icon.png"))); // NOI18N

        jlNombreUser.setText("Nombre:");

        jlCedula.setText("Cèdula:");

        jlCargo.setText("Cargo:");

        jlSede.setText("Sede:");

        jlDireccion.setText("Direcciòn");

        jlTelefono.setText("Telèfono:");

        jlBotonModificarUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonmodificar.jpg"))); // NOI18N
        jlBotonModificarUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonModificarUserMouseClicked(evt);
            }
        });

        jlAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlAtrasMouseClicked(evt);
            }
        });

        jtfCedulaUser.setEditable(false);

        jcbCargoUser.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "GERENTE", "VENDEDOR", "JEFE TALLER" }));

        jlEmail.setText("Email:");

        jcbEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ACTIVO", "INACTIVO" }));

        jlEstado.setText("Estado:");

        jLabel1.setText("Usuario:");

        jLabel2.setText("Contraseña:");

        jPassword.setText("jPasswordField1");

        jlValidacion.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanelCrearUserLayout = new javax.swing.GroupLayout(jPanelCrearUser);
        jPanelCrearUser.setLayout(jPanelCrearUserLayout);
        jPanelCrearUserLayout.setHorizontalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlNombreUser)
                        .addGap(29, 29, 29)
                        .addComponent(jtfNombreUser, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlTelefono)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                            .addComponent(jtfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlSede)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jcbNombreSede, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlCargo)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jcbCargoUser, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlCedula)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jtfCedulaUser, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlDireccion)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jtfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCrearUserLayout.createSequentialGroup()
                            .addComponent(jlEstado)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jcbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                            .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                        .addComponent(jlEmail)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(28, 28, 28)))
                                .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addGap(8, 8, 8)))
                            .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jtfEmailUser, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                                .addComponent(jtfUser)
                                .addComponent(jPassword)))))
                .addGap(0, 124, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconUSer)
                        .addGap(85, 85, 85)
                        .addComponent(jlAtras)
                        .addGap(43, 43, 43))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlBotonModificarUser)
                        .addGap(193, 193, 193))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlValidacion)
                        .addGap(235, 235, 235))))
        );
        jPanelCrearUserLayout.setVerticalGroup(
            jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlAtras)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 143, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCrearUserLayout.createSequentialGroup()
                        .addComponent(jlIconUSer)
                        .addGap(55, 55, 55)))
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlNombreUser)
                    .addComponent(jtfNombreUser, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCedula)
                    .addComponent(jtfCedulaUser, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlCargo)
                    .addComponent(jcbCargoUser, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlEstado)
                    .addComponent(jcbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlSede, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbNombreSede, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlDireccion))
                .addGap(20, 20, 20)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlTelefono)
                    .addComponent(jtfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlEmail)
                    .addComponent(jtfEmailUser, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfUser, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanelCrearUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(jlValidacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlBotonModificarUser)
                .addContainerGap())
        );

        getContentPane().add(jPanelCrearUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        
        this.dispose();
    }//GEN-LAST:event_jlAtrasMouseClicked

    private void jlBotonModificarUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonModificarUserMouseClicked
        // Modificar usuario
        String contrasena = new Encriptar().EncriptarMd5(String.valueOf(jPassword.getPassword()));
        
        String nombre = jtfNombreUser.getText().trim().toUpperCase();
        String cedula = jtfCedulaUser.getText().trim();
        String usuario = jtfUser.getText().trim();
        String direccion = jtfDireccion.getText().trim().toUpperCase();
        String email = jtfEmailUser.getText().trim().toUpperCase();
        String telefono = jtfTelefono.getText().trim();
        String cargoSeleccionado = (String) jcbCargoUser.getSelectedItem();
        String sede = (String) jcbNombreSede.getSelectedItem();
        String estado = (String) jcbEstado.getSelectedItem();

        if(jlValidacion.getText().length()!=0){
            return;
        }
        if (cedula.length() == 0
            || contrasena.length() == 0
            || usuario.length() == 0
            || direccion.length() == 0
            || usuario.length() == 0
            || telefono.length() == 0
            || nombre.length() == 0
            ) {
            JOptionPane.showMessageDialog(this, "Por favor complete todos los campos", "Cotizacion de vehiculo", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        
        String mensaje = new ManejoDatosEmpleado().RealizarModificacionEmpleado(nombre, cedula,
                usuario, direccion, email, telefono, contrasena, cargoSeleccionado, sede, estado);
        JOptionPane.showMessageDialog(this, mensaje, "Registro de empleado", JOptionPane.INFORMATION_MESSAGE);
        padre.setVisible(true);
        this.dispose();

    }//GEN-LAST:event_jlBotonModificarUserMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanelCrearUser;
    private javax.swing.JPasswordField jPassword;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JComboBox jcbCargoUser;
    private javax.swing.JComboBox jcbEstado;
    private javax.swing.JComboBox jcbNombreSede;
    private javax.swing.JLabel jlAtras;
    private javax.swing.JLabel jlBotonModificarUser;
    private javax.swing.JLabel jlCargo;
    private javax.swing.JLabel jlCedula;
    private javax.swing.JLabel jlDireccion;
    private javax.swing.JLabel jlEmail;
    private javax.swing.JLabel jlEstado;
    private javax.swing.JLabel jlIconUSer;
    private javax.swing.JLabel jlNombreUser;
    private javax.swing.JLabel jlSede;
    private javax.swing.JLabel jlTelefono;
    private javax.swing.JLabel jlValidacion;
    private javax.swing.JTextField jtfCedulaUser;
    private javax.swing.JTextField jtfDireccion;
    private javax.swing.JTextField jtfEmailUser;
    private javax.swing.JTextField jtfNombreUser;
    private javax.swing.JTextField jtfTelefono;
    private javax.swing.JTextField jtfUser;
    // End of variables declaration//GEN-END:variables
}
