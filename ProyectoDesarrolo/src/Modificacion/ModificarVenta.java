package Modificacion;


import Datos.ManejoDatosVenta;
import javax.swing.JFrame;


public class ModificarVenta extends javax.swing.JFrame {
    JFrame padre;
   
    public ModificarVenta(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        new ManejoDatosVenta().ObtenerDatosVentas(jtableConsultarVenta);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jlIconVenta = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();
        jpConsultarVenta = new javax.swing.JPanel();
        jlbotonConsultar = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtableConsultarVenta = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Modificar Venta");
        setMinimumSize(new java.awt.Dimension(789, 686));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jlIconVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/IconVentas.jpg"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(27, 217, 21, 0);
        jPanel1.add(jlIconVenta, gridBagConstraints);

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 117, 0, 10);
        jPanel1.add(jlbotonAtras, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.NORTH);

        jpConsultarVenta.setBackground(new java.awt.Color(255, 255, 255));
        jpConsultarVenta.setLayout(null);

        jlbotonConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonConsultar.jpg"))); // NOI18N
        jlbotonConsultar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonConsultarMouseClicked(evt);
            }
        });
        jpConsultarVenta.add(jlbotonConsultar);
        jlbotonConsultar.setBounds(330, 490, 115, 38);

        jtableConsultarVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Codigo_Venta", "Precio", "Chasis"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jtableConsultarVenta.setGridColor(new java.awt.Color(255, 255, 255));
        jtableConsultarVenta.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtableConsultarVenta);

        jpConsultarVenta.add(jScrollPane1);
        jScrollPane1.setBounds(30, 20, 740, 437);

        getContentPane().add(jpConsultarVenta, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos de Botones
    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);        
        this.dispose();        
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlbotonConsultarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonConsultarMouseClicked
        int row=jtableConsultarVenta.getSelectedRow();
        if (row == -1) return;
        String cod_venta = (String) jtableConsultarVenta.getModel().getValueAt(row, 0);
        String chasis = (String) jtableConsultarVenta.getModel().getValueAt(row, 1);
        String cedula_comprador = (String) jtableConsultarVenta.getModel().getValueAt(row, 3);
        RealizarModificacionVenta modificacion =new ManejoDatosVenta().CargarDatosVentaAModificar(this, cod_venta,cedula_comprador, chasis);
        modificacion.setVisible(true);
    }//GEN-LAST:event_jlbotonConsultarMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jlIconVenta;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlbotonConsultar;
    private javax.swing.JPanel jpConsultarVenta;
    private javax.swing.JTable jtableConsultarVenta;
    // End of variables declaration//GEN-END:variables
}
