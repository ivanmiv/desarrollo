package Modificacion;


import Datos.ManejoDatosEmpleado;
import Datos.ManejoDatos;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


public class ModificarEmpleado extends javax.swing.JFrame {

    JFrame padre;
    public ModificarEmpleado(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
        
        new ManejoDatosEmpleado().ObtenerDatosEmpleados(jtEmpleados);
    }

    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpanelModificarUser = new javax.swing.JPanel();
        jlIconUser = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();
        jlbotonContinuar = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtEmpleados = new javax.swing.JTable();
        jbrefrescar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Modificar Usuario");
        setResizable(false);

        jpanelModificarUser.setBackground(new java.awt.Color(255, 255, 255));

        jlIconUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/contact-icon.png"))); // NOI18N

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });

        jlbotonContinuar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonContinuar.png"))); // NOI18N
        jlbotonContinuar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonContinuarMouseClicked(evt);
            }
        });

        jtEmpleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nombre", "Cedula", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jtEmpleados.setFocusable(false);
        jtEmpleados.setGridColor(new java.awt.Color(255, 255, 255));
        jtEmpleados.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtEmpleados);

        jbrefrescar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/reload.png"))); // NOI18N
        jbrefrescar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jbrefrescarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jpanelModificarUserLayout = new javax.swing.GroupLayout(jpanelModificarUser);
        jpanelModificarUser.setLayout(jpanelModificarUserLayout);
        jpanelModificarUserLayout.setHorizontalGroup(
            jpanelModificarUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanelModificarUserLayout.createSequentialGroup()
                .addGap(190, 190, 190)
                .addComponent(jlbotonContinuar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanelModificarUserLayout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(jpanelModificarUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpanelModificarUserLayout.createSequentialGroup()
                        .addComponent(jbrefrescar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 150, Short.MAX_VALUE)
                        .addComponent(jlIconUser)
                        .addGap(106, 106, 106)
                        .addComponent(jlbotonAtras)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanelModificarUserLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31))))
        );
        jpanelModificarUserLayout.setVerticalGroup(
            jpanelModificarUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanelModificarUserLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpanelModificarUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlIconUser)
                    .addGroup(jpanelModificarUserLayout.createSequentialGroup()
                        .addGroup(jpanelModificarUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jpanelModificarUserLayout.createSequentialGroup()
                                .addComponent(jlbotonAtras)
                                .addGap(77, 77, 77))
                            .addGroup(jpanelModificarUserLayout.createSequentialGroup()
                                .addComponent(jbrefrescar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(jlbotonContinuar)))
                .addContainerGap())
        );

        getContentPane().add(jpanelModificarUser, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents
  //Eventos Botones
    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
                
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlbotonContinuarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonContinuarMouseClicked
        this.setVisible(false);    
        int row=jtEmpleados.getSelectedRow();
        String cedula = (String)jtEmpleados.getModel().getValueAt(row, 1);
        RealizarModificacionEmpleado u = new ManejoDatosEmpleado().CargarDatosEmpleadoAModificar(this,cedula);
        u.setVisible(true);
        
    }//GEN-LAST:event_jlbotonContinuarMouseClicked

    public void Recargar(){
        new ManejoDatosEmpleado().ObtenerDatosEmpleados(jtEmpleados); 
    }
    private void jbrefrescarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jbrefrescarMouseClicked
        new ManejoDatosEmpleado().ObtenerDatosEmpleados(jtEmpleados);
    }//GEN-LAST:event_jbrefrescarMouseClicked
/*
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RealizarModificacionEmpleado().setVisible(true);
            }
        });
    }*/

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jbrefrescar;
    private javax.swing.JLabel jlIconUser;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlbotonContinuar;
    private javax.swing.JPanel jpanelModificarUser;
    private javax.swing.JTable jtEmpleados;
    // End of variables declaration//GEN-END:variables

    
}
