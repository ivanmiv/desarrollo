package Interfaz;

import java.util.ArrayList;

public class GuiABC extends javax.swing.JFrame {

    ArrayList usuario;

    public GuiABC(ArrayList usuario) {
        super("Menú Principal");
        this.usuario = usuario;

        initComponents();
        setLocationRelativeTo(null);

        MostrarSegunPerfil();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPrincipal = new javax.swing.JPanel();
        jlUsuarios = new javax.swing.JLabel();
        jlReportes = new javax.swing.JLabel();
        jlVentas = new javax.swing.JLabel();
        jlSedes = new javax.swing.JLabel();
        jlCotizacion = new javax.swing.JLabel();
        jlOrdenTra = new javax.swing.JLabel();
        jlIconAuto = new javax.swing.JLabel();
        jlVehiculos = new javax.swing.JLabel();
        jlRepuestos = new javax.swing.JLabel();
        jlBotonSalir = new javax.swing.JLabel();
        jPanelUser = new javax.swing.JPanel();
        jlnombreUser = new javax.swing.JLabel();
        jlIconUser = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Menu Principal");
        setUndecorated(true);
        setResizable(false);

        jPanelPrincipal.setBackground(new java.awt.Color(255, 255, 255));

        jlUsuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/usuarios.jpg"))); // NOI18N
        jlUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlUsuariosMouseClicked(evt);
            }
        });

        jlReportes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/reportes.jpg"))); // NOI18N
        jlReportes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlReportesMouseClicked(evt);
            }
        });

        jlVentas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/ventas.png"))); // NOI18N
        jlVentas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlVentasMouseClicked(evt);
            }
        });

        jlSedes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/sedes.jpg"))); // NOI18N
        jlSedes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlSedesMouseClicked(evt);
            }
        });

        jlCotizacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/cotizacion.jpg"))); // NOI18N
        jlCotizacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlCotizacionMouseClicked(evt);
            }
        });

        jlOrdenTra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/ordenes de trabajo.jpg"))); // NOI18N
        jlOrdenTra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlOrdenTraMouseClicked(evt);
            }
        });

        jlIconAuto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/IMG/239600176.jpg"))); // NOI18N

        jlVehiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/AUTOMOVILES.jpg"))); // NOI18N
        jlVehiculos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlVehiculosMouseClicked(evt);
            }
        });

        jlRepuestos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/REPUESTOS.jpg"))); // NOI18N
        jlRepuestos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlRepuestosMouseClicked(evt);
            }
        });

        jlBotonSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/Color-network-tab-button-vector-material-54144.jpg"))); // NOI18N
        jlBotonSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonSalirMouseClicked(evt);
            }
        });

        jPanelUser.setBackground(new java.awt.Color(0, 0, 0));

        jlnombreUser.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlnombreUser.setForeground(new java.awt.Color(255, 255, 255));

        jlIconUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/iconUsuarioMenuP.png"))); // NOI18N

        javax.swing.GroupLayout jPanelUserLayout = new javax.swing.GroupLayout(jPanelUser);
        jPanelUser.setLayout(jPanelUserLayout);
        jPanelUserLayout.setHorizontalGroup(
            jPanelUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUserLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jlIconUser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlnombreUser)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelUserLayout.setVerticalGroup(
            jPanelUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUserLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jlnombreUser)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jlIconUser, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
        );

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/SysGen-Cares-Final.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanelPrincipalLayout = new javax.swing.GroupLayout(jPanelPrincipal);
        jPanelPrincipal.setLayout(jPanelPrincipalLayout);
        jPanelPrincipalLayout.setHorizontalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                        .addGap(139, 139, 139)
                        .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                                    .addGap(390, 390, 390)
                                    .addComponent(jlBotonSalir))
                                .addComponent(jlIconAuto, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jlOrdenTra)
                                    .addComponent(jlUsuarios))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlReportes)
                                    .addComponent(jlSedes, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlVentas)
                                    .addComponent(jlCotizacion, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlRepuestos)
                                    .addComponent(jlVehiculos))
                                .addGap(76, 76, 76)))))
                .addContainerGap(87, Short.MAX_VALUE))
        );
        jPanelPrincipalLayout.setVerticalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addComponent(jPanelUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addComponent(jlIconAuto, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlUsuarios)
                    .addComponent(jlReportes)
                    .addComponent(jlVentas)
                    .addComponent(jlVehiculos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlOrdenTra, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlSedes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlCotizacion, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlRepuestos, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jlBotonSalir))
                .addGap(27, 27, 27))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlUsuariosMouseClicked
        if (jlUsuarios.isEnabled()) {
            this.setVisible(false);
            OpcionesUsuarios user = new OpcionesUsuarios(this);
            user.setVisible(true);
        }
    }//GEN-LAST:event_jlUsuariosMouseClicked

    private void jlSedesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSedesMouseClicked
        if (jlSedes.isEnabled()) {
            this.setVisible(false);
            OpcionesSedes sede = new OpcionesSedes(this);
            sede.setVisible(true);
        }
    }//GEN-LAST:event_jlSedesMouseClicked

    private void jlVehiculosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlVehiculosMouseClicked
        if (jlVehiculos.isEnabled()) {
            this.setVisible(false);
            OpcionesVehiculos vehiculo = new OpcionesVehiculos(this);
            vehiculo.setVisible(true);
        }
    }//GEN-LAST:event_jlVehiculosMouseClicked

    private void jlRepuestosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRepuestosMouseClicked
        if (jlRepuestos.isEnabled()) {
            this.setVisible(false);
            OpcionesRepuestos repuesto = new OpcionesRepuestos(this);
            repuesto.setVisible(true);
        }

    }//GEN-LAST:event_jlRepuestosMouseClicked

    private void jlOrdenTraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlOrdenTraMouseClicked
        //temporal para pruebas
        if (jlOrdenTra.isEnabled()) {
            this.setVisible(false);

            OpcionesOrdenesTrabajo opcionesOrden = new OpcionesOrdenesTrabajo(this);//temporar para mostrar
            opcionesOrden.setVisible(true);
        }
    }//GEN-LAST:event_jlOrdenTraMouseClicked


    private void jlReportesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlReportesMouseClicked
        if (jlReportes.isEnabled()) {
            OpcionesReporte reportes = new OpcionesReporte(this);
            reportes.setVisible(true);
        }
    }//GEN-LAST:event_jlReportesMouseClicked

    private void jlVentasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlVentasMouseClicked
        if (jlVentas.isEnabled()) {
            this.setVisible(false);
            OpcionesVentas venta = new OpcionesVentas(this,usuario.get(0).toString());//temporar para mostrar
            venta.setVisible(true);
        }
    }//GEN-LAST:event_jlVentasMouseClicked

    private void jlBotonSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonSalirMouseClicked
        new GuiLogin().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonSalirMouseClicked

    private void jlCotizacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlCotizacionMouseClicked
        if (jlVentas.isEnabled()) {
            this.setVisible(false);
            OpcionesCotizaciones venta = new OpcionesCotizaciones(this,usuario.get(0).toString());//temporar para mostrar
            venta.setVisible(true);
        }
    }//GEN-LAST:event_jlCotizacionMouseClicked

    public void MostrarSegunPerfil() {

        String cedula = usuario.get(0).toString();
        String nombre = usuario.get(1).toString();
        String perfil = usuario.get(2).toString();

        jlnombreUser.setText("<html>" + nombre + "<br>" + perfil + "</html>");

        if (perfil.equalsIgnoreCase("gerente")) {
            jlCotizacion.setEnabled(false);
            jlVentas.setEnabled(false);
            jlOrdenTra.setEnabled(false);
        } else if (perfil.equalsIgnoreCase("vendedor")) {


            jlOrdenTra.setEnabled(false);
            jlReportes.setEnabled(false);
            jlRepuestos.setEnabled(false);
            jlSedes.setEnabled(false);
            jlUsuarios.setEnabled(false);
            jlVehiculos.setEnabled(false);

        } else if (perfil.equalsIgnoreCase("jefe taller")) {
            jlCotizacion.setEnabled(false);
            jlVentas.setEnabled(false);
            jlReportes.setEnabled(false);
            jlRepuestos.setEnabled(false);
            jlSedes.setEnabled(false);
            jlUsuarios.setEnabled(false);
            jlVehiculos.setEnabled(false);

        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelPrincipal;
    private javax.swing.JPanel jPanelUser;
    private javax.swing.JLabel jlBotonSalir;
    private javax.swing.JLabel jlCotizacion;
    private javax.swing.JLabel jlIconAuto;
    private javax.swing.JLabel jlIconUser;
    private javax.swing.JLabel jlOrdenTra;
    private javax.swing.JLabel jlReportes;
    private javax.swing.JLabel jlRepuestos;
    private javax.swing.JLabel jlSedes;
    private javax.swing.JLabel jlUsuarios;
    private javax.swing.JLabel jlVehiculos;
    private javax.swing.JLabel jlVentas;
    private javax.swing.JLabel jlnombreUser;
    // End of variables declaration//GEN-END:variables


    /* 
    
     public static void main(String args[]) 
     {
        
     java.awt.EventQueue.invokeLater(new Runnable() {
     public void run() {
     new GuiABC().setVisible(true);
                
     }
            
     });
         
     }
     

 
     */
}
