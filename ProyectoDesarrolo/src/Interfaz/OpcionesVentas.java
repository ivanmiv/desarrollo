package Interfaz;


import Consultas.ConsultarVenta;
import Modificacion.ModificarVenta;
import Registros.RegistrarVenta;
import javax.swing.JFrame;

public class OpcionesVentas extends javax.swing.JFrame {

    JFrame padre;
    String Cedula;
    public OpcionesVentas(JFrame padre,String cedulaVendedor) {
        this.padre = padre;
        this.Cedula = cedulaVendedor;
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelUsuarios = new javax.swing.JPanel();
        jlRealizarVenta = new javax.swing.JLabel();
        jlConsultarVenta = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();
        jlModificarVenta = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestion Ventas");
        setBackground(new java.awt.Color(254, 254, 254));
        setResizable(false);

        jPanelUsuarios.setBackground(new java.awt.Color(255, 255, 255));

        jlRealizarVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/Registrarventas.jpg"))); // NOI18N
        jlRealizarVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlRealizarVentaMouseClicked(evt);
            }
        });

        jlConsultarVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/consultarVenta.jpg"))); // NOI18N
        jlConsultarVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlConsultarVentaMouseClicked(evt);
            }
        });

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });

        jlModificarVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/modificarVenta.jpg"))); // NOI18N
        jlModificarVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlModificarVentaMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelUsuariosLayout = new javax.swing.GroupLayout(jPanelUsuarios);
        jPanelUsuarios.setLayout(jPanelUsuariosLayout);
        jPanelUsuariosLayout.setHorizontalGroup(
            jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jlRealizarVenta)
                .addGroup(jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jlConsultarVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlModificarVenta))
                    .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jlbotonAtras)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelUsuariosLayout.setVerticalGroup(
            jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlConsultarVenta)
                    .addComponent(jlRealizarVenta)
                    .addComponent(jlModificarVenta))
                .addGap(27, 27, 27)
                .addComponent(jlbotonAtras)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanelUsuarios, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos Botones 

    private void jlRealizarVentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRealizarVentaMouseClicked
        this.setVisible(false);
        RegistrarVenta rv = new RegistrarVenta(this,this.Cedula);
        rv.setVisible(true);
    }//GEN-LAST:event_jlRealizarVentaMouseClicked

    private void jlConsultarVentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlConsultarVentaMouseClicked
        this.setVisible(false);
        ConsultarVenta cv = new ConsultarVenta(this);
        cv.setVisible(true);
                           

    }//GEN-LAST:event_jlConsultarVentaMouseClicked

    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlModificarVentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlModificarVentaMouseClicked
        this.setVisible(false);
        ModificarVenta mv = new ModificarVenta(this);
        mv.setVisible(true);
    }//GEN-LAST:event_jlModificarVentaMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelUsuarios;
    private javax.swing.JLabel jlConsultarVenta;
    private javax.swing.JLabel jlModificarVenta;
    private javax.swing.JLabel jlRealizarVenta;
    private javax.swing.JLabel jlbotonAtras;
    // End of variables declaration//GEN-END:variables
}

