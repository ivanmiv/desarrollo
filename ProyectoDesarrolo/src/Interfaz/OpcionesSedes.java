package Interfaz;


import Modificacion.ModificarSede;
import Registros.CrearSede;
import Consultas.ConsultarSede;
import javax.swing.JFrame;


public class OpcionesSedes extends javax.swing.JFrame {
    JFrame padre;
    public OpcionesSedes(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jlcrearSede = new javax.swing.JLabel();
        jlmodificarSede = new javax.swing.JLabel();
        jlconsultarSede = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestion Sedes");
        setBackground(new java.awt.Color(254, 254, 254));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jlcrearSede.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/crearS.jpg"))); // NOI18N
        jlcrearSede.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlcrearSedeMouseClicked(evt);
            }
        });

        jlmodificarSede.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/modificarS.jpg"))); // NOI18N
        jlmodificarSede.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlmodificarSedeMouseClicked(evt);
            }
        });

        jlconsultarSede.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/consultarS.jpg"))); // NOI18N
        jlconsultarSede.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlconsultarSedeMouseClicked(evt);
            }
        });

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlcrearSede)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlmodificarSede)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlconsultarSede, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(194, 194, 194)
                .addComponent(jlbotonAtras)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlmodificarSede)
                    .addComponent(jlconsultarSede)
                    .addComponent(jlcrearSede))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(jlbotonAtras)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlcrearSedeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlcrearSedeMouseClicked
        this.setVisible(false);
        CrearSede  sede = new CrearSede(this);
         sede.setVisible(true);
    }//GEN-LAST:event_jlcrearSedeMouseClicked

    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlmodificarSedeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlmodificarSedeMouseClicked
        this.setVisible(false);
        ModificarSede  sede = new ModificarSede(this);
        sede.setVisible(true); 
    }//GEN-LAST:event_jlmodificarSedeMouseClicked

    private void jlconsultarSedeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlconsultarSedeMouseClicked
        this.setVisible(false);
        ConsultarSede  sede = new ConsultarSede(this);
        sede.setVisible(true); 
    }//GEN-LAST:event_jlconsultarSedeMouseClicked

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlconsultarSede;
    private javax.swing.JLabel jlcrearSede;
    private javax.swing.JLabel jlmodificarSede;
    // End of variables declaration//GEN-END:variables
}
