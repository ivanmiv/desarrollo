package Interfaz;


import Modificacion.ModificarEmpleado;
import Registros.CrearEmpleado;
import Consultas.ConsultarEmpleado;
import javax.swing.JFrame;

public class OpcionesUsuarios extends javax.swing.JFrame {

    JFrame padre;

    public OpcionesUsuarios(JFrame padre) {
        this.padre = padre;
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelUsuarios = new javax.swing.JPanel();
        jlCrearUser = new javax.swing.JLabel();
        jlConsultar = new javax.swing.JLabel();
        jlBotonAtras = new javax.swing.JLabel();
        jlModificar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestion Usuarios");
        setBackground(new java.awt.Color(254, 254, 254));
        setResizable(false);

        jPanelUsuarios.setBackground(new java.awt.Color(255, 255, 255));

        jlCrearUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/crearU.jpg"))); // NOI18N
        jlCrearUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlCrearUserMouseClicked(evt);
            }
        });

        jlConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/consultar.jpg"))); // NOI18N
        jlConsultar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlConsultarMouseClicked(evt);
            }
        });

        jlBotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlBotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAtrasMouseClicked(evt);
            }
        });

        jlModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/modificarU.jpg"))); // NOI18N
        jlModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlModificarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelUsuariosLayout = new javax.swing.GroupLayout(jPanelUsuarios);
        jPanelUsuarios.setLayout(jPanelUsuariosLayout);
        jPanelUsuariosLayout.setHorizontalGroup(
            jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jlCrearUser)
                .addGroup(jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jlConsultar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlModificar))
                    .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jlBotonAtras)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelUsuariosLayout.setVerticalGroup(
            jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlConsultar)
                    .addComponent(jlCrearUser)
                    .addComponent(jlModificar))
                .addGap(27, 27, 27)
                .addComponent(jlBotonAtras)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanelUsuarios, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos Botones 

    private void jlCrearUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlCrearUserMouseClicked
        this.setVisible(false);
        CrearEmpleado u = new CrearEmpleado(this);
        u.setVisible(true);
    }//GEN-LAST:event_jlCrearUserMouseClicked

    private void jlConsultarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlConsultarMouseClicked
        this.setVisible(false);
        ConsultarEmpleado u = new ConsultarEmpleado(this);
        u.setVisible(true);

    }//GEN-LAST:event_jlConsultarMouseClicked

    private void jlBotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonAtrasMouseClicked

    private void jlModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlModificarMouseClicked
        this.setVisible(false);
        ModificarEmpleado u = new ModificarEmpleado(this);
        u.setVisible(true);
    }//GEN-LAST:event_jlModificarMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelUsuarios;
    private javax.swing.JLabel jlBotonAtras;
    private javax.swing.JLabel jlConsultar;
    private javax.swing.JLabel jlCrearUser;
    private javax.swing.JLabel jlModificar;
    // End of variables declaration//GEN-END:variables
}
