package Interfaz;


import Modificacion.ModificarSede;
import Registros.CrearSede;
import Consultas.ConsultarSede;
import Consultas.ConsultarVehiculo;
import Modificacion.ModificarVehiculo;
import Registros.IngresarVehiculo;
import javax.swing.JFrame;


public class OpcionesVehiculos extends javax.swing.JFrame {
    JFrame padre;
    public OpcionesVehiculos(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jlcrearVehiculo = new javax.swing.JLabel();
        jlmodificarVehiculo = new javax.swing.JLabel();
        jlconsultarVehiculo = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestion Vehiculos");
        setBackground(new java.awt.Color(254, 254, 254));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jlcrearVehiculo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/ingresarV.jpg"))); // NOI18N
        jlcrearVehiculo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlcrearVehiculoMouseClicked(evt);
            }
        });

        jlmodificarVehiculo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/modificarV.jpg"))); // NOI18N
        jlmodificarVehiculo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlmodificarVehiculoMouseClicked(evt);
            }
        });

        jlconsultarVehiculo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/consultarV.jpg"))); // NOI18N
        jlconsultarVehiculo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlconsultarVehiculoMouseClicked(evt);
            }
        });

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlcrearVehiculo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlmodificarVehiculo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlconsultarVehiculo, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(194, 194, 194)
                .addComponent(jlbotonAtras)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlmodificarVehiculo)
                    .addComponent(jlconsultarVehiculo)
                    .addComponent(jlcrearVehiculo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(jlbotonAtras)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlcrearVehiculoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlcrearVehiculoMouseClicked
        this.setVisible(false);
        IngresarVehiculo v = new IngresarVehiculo(this);
         v.setVisible(true);
    }//GEN-LAST:event_jlcrearVehiculoMouseClicked

    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlmodificarVehiculoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlmodificarVehiculoMouseClicked
        this.setVisible(false);
        ModificarVehiculo  mv = new ModificarVehiculo(this);
        mv.setVisible(true); 
    }//GEN-LAST:event_jlmodificarVehiculoMouseClicked

    private void jlconsultarVehiculoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlconsultarVehiculoMouseClicked
        this.setVisible(false);
        ConsultarVehiculo  cv = new ConsultarVehiculo(this);
        cv.setVisible(true); 
    }//GEN-LAST:event_jlconsultarVehiculoMouseClicked

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlconsultarVehiculo;
    private javax.swing.JLabel jlcrearVehiculo;
    private javax.swing.JLabel jlmodificarVehiculo;
    // End of variables declaration//GEN-END:variables
}
