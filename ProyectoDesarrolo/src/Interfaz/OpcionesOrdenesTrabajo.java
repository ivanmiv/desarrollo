package Interfaz;


import Consultas.ConsultaOrdenTrabajo;
import Modificacion.ModificarEmpleado;
import Registros.CrearEmpleado;
import Consultas.ConsultarEmpleado;
import Modificacion.ModificarOrdenTrabajo;
import Registros.CrearOrdenTrabajo;
import javax.swing.JFrame;

public class OpcionesOrdenesTrabajo extends javax.swing.JFrame {

    JFrame padre;

    public OpcionesOrdenesTrabajo(JFrame padre) {
        this.padre = padre;
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelOrdenTrabajo = new javax.swing.JPanel();
        jlCrearOrdenTrabajo = new javax.swing.JLabel();
        jlConsultarOrden = new javax.swing.JLabel();
        jlBotonAtras = new javax.swing.JLabel();
        jlModificarorden = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestion Ordenes Trabajo");
        setBackground(new java.awt.Color(254, 254, 254));
        setResizable(false);

        jPanelOrdenTrabajo.setBackground(new java.awt.Color(255, 255, 255));

        jlCrearOrdenTrabajo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/registrar-orden-de-trabajo.jpg"))); // NOI18N
        jlCrearOrdenTrabajo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlCrearOrdenTrabajoMouseClicked(evt);
            }
        });

        jlConsultarOrden.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/consultar-orden-de-trabajo.jpg"))); // NOI18N
        jlConsultarOrden.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlConsultarOrdenMouseClicked(evt);
            }
        });

        jlBotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlBotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAtrasMouseClicked(evt);
            }
        });

        jlModificarorden.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/modificar-orden-de-trabajo.jpg"))); // NOI18N
        jlModificarorden.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlModificarordenMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelOrdenTrabajoLayout = new javax.swing.GroupLayout(jPanelOrdenTrabajo);
        jPanelOrdenTrabajo.setLayout(jPanelOrdenTrabajoLayout);
        jPanelOrdenTrabajoLayout.setHorizontalGroup(
            jPanelOrdenTrabajoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelOrdenTrabajoLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jlCrearOrdenTrabajo)
                .addGroup(jPanelOrdenTrabajoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelOrdenTrabajoLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jlConsultarOrden)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlModificarorden))
                    .addGroup(jPanelOrdenTrabajoLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jlBotonAtras)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelOrdenTrabajoLayout.setVerticalGroup(
            jPanelOrdenTrabajoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelOrdenTrabajoLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanelOrdenTrabajoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlConsultarOrden)
                    .addComponent(jlCrearOrdenTrabajo)
                    .addComponent(jlModificarorden))
                .addGap(27, 27, 27)
                .addComponent(jlBotonAtras)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanelOrdenTrabajo, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos Botones 

    private void jlCrearOrdenTrabajoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlCrearOrdenTrabajoMouseClicked
        this.setVisible(false);
        CrearOrdenTrabajo u = new CrearOrdenTrabajo(this);
        u.setVisible(true);
    }//GEN-LAST:event_jlCrearOrdenTrabajoMouseClicked

    private void jlConsultarOrdenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlConsultarOrdenMouseClicked
        this.setVisible(false);
        ConsultaOrdenTrabajo opciones = new ConsultaOrdenTrabajo(this);
        opciones.setVisible(true);

    }//GEN-LAST:event_jlConsultarOrdenMouseClicked

    private void jlBotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonAtrasMouseClicked

    private void jlModificarordenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlModificarordenMouseClicked
        ModificarOrdenTrabajo modificacion = new ModificarOrdenTrabajo(this);
        modificacion.setVisible(true);
        this.setVisible(false);
        this.dispose();
        //ModificarEmpleado u = new ModificarEmpleado(this);
        //u.setVisible(true);
    }//GEN-LAST:event_jlModificarordenMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelOrdenTrabajo;
    private javax.swing.JLabel jlBotonAtras;
    private javax.swing.JLabel jlConsultarOrden;
    private javax.swing.JLabel jlCrearOrdenTrabajo;
    private javax.swing.JLabel jlModificarorden;
    // End of variables declaration//GEN-END:variables
}
