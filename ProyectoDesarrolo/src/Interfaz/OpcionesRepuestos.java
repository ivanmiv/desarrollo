package Interfaz;


import Consultas.ConsultarRepuesto;
import Modificacion.ModificarSede;
import Registros.CrearSede;
import Consultas.ConsultarSede;
import Consultas.ConsultarVehiculo;
import Modificacion.ModificarRepuesto;
import Modificacion.ModificarVehiculo;
import Registros.IngresarRepuesto;
import Registros.IngresarVehiculo;
import javax.swing.JFrame;


public class OpcionesRepuestos extends javax.swing.JFrame {
    JFrame padre;
    public OpcionesRepuestos(JFrame padre) {
        this.padre=padre;
        initComponents();
        setLocationRelativeTo(null);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jlIngresarRepuesto = new javax.swing.JLabel();
        jlmodificarRepuesto = new javax.swing.JLabel();
        jlconsultarRepuesto = new javax.swing.JLabel();
        jlbotonAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestion Repuestos");
        setBackground(new java.awt.Color(254, 254, 254));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jlIngresarRepuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/ingresarR.jpg"))); // NOI18N
        jlIngresarRepuesto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlIngresarRepuestoMouseClicked(evt);
            }
        });

        jlmodificarRepuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/modificarR.jpg"))); // NOI18N
        jlmodificarRepuesto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlmodificarRepuestoMouseClicked(evt);
            }
        });

        jlconsultarRepuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/consultarR - copia.jpg"))); // NOI18N
        jlconsultarRepuesto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlconsultarRepuestoMouseClicked(evt);
            }
        });

        jlbotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlbotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlbotonAtrasMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlIngresarRepuesto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jlmodificarRepuesto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlconsultarRepuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(194, 194, 194)
                .addComponent(jlbotonAtras)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlmodificarRepuesto)
                    .addComponent(jlconsultarRepuesto)
                    .addComponent(jlIngresarRepuesto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(jlbotonAtras)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlIngresarRepuestoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlIngresarRepuestoMouseClicked
        this.setVisible(false);
        IngresarRepuesto r = new IngresarRepuesto(this);
         r.setVisible(true);
    }//GEN-LAST:event_jlIngresarRepuestoMouseClicked

    private void jlbotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlbotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlbotonAtrasMouseClicked

    private void jlmodificarRepuestoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlmodificarRepuestoMouseClicked
        this.setVisible(false);
        ModificarRepuesto  mr = new ModificarRepuesto(this);
        mr.setVisible(true); 
    }//GEN-LAST:event_jlmodificarRepuestoMouseClicked

    private void jlconsultarRepuestoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlconsultarRepuestoMouseClicked
        this.setVisible(false);
        ConsultarRepuesto  cr = new ConsultarRepuesto(this);
        cr.setVisible(true); 
    }//GEN-LAST:event_jlconsultarRepuestoMouseClicked

   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jlIngresarRepuesto;
    private javax.swing.JLabel jlbotonAtras;
    private javax.swing.JLabel jlconsultarRepuesto;
    private javax.swing.JLabel jlmodificarRepuesto;
    // End of variables declaration//GEN-END:variables
}
