package Interfaz;

import Consultas.ConsultaCotizacion;
import Consultas.ConsultaCotizacion;
import Modificacion.ModificacionCotizacion;
import Registros.RegistrarCotizacion;
import javax.swing.JFrame;

public class OpcionesCotizaciones extends javax.swing.JFrame {

    JFrame padre;
    String id_vendedor;

    public OpcionesCotizaciones(JFrame padre, String id_vendedor) {
        this.padre = padre;
        this.id_vendedor = id_vendedor;
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelUsuarios = new javax.swing.JPanel();
        jlRealizarCotizacion = new javax.swing.JLabel();
        jlConsultarCotizacion = new javax.swing.JLabel();
        jlModificarCotizacion = new javax.swing.JLabel();
        jlBotonAtras = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gestion Ventas");
        setBackground(new java.awt.Color(254, 254, 254));
        setPreferredSize(new java.awt.Dimension(437, 296));
        setResizable(false);

        jPanelUsuarios.setBackground(new java.awt.Color(255, 255, 255));

        jlRealizarCotizacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/realizarCotizacion.jpg"))); // NOI18N
        jlRealizarCotizacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlRealizarCotizacionMouseClicked(evt);
            }
        });

        jlConsultarCotizacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/consultarCotizacion.jpg"))); // NOI18N
        jlConsultarCotizacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlConsultarCotizacionMouseClicked(evt);
            }
        });

        jlModificarCotizacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/modificarCotizacion.jpg"))); // NOI18N
        jlModificarCotizacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlModificarCotizacionMouseClicked(evt);
            }
        });

        jlBotonAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/botonAtras.png"))); // NOI18N
        jlBotonAtras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlBotonAtrasMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelUsuariosLayout = new javax.swing.GroupLayout(jPanelUsuarios);
        jPanelUsuarios.setLayout(jPanelUsuariosLayout);
        jPanelUsuariosLayout.setHorizontalGroup(
            jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jlRealizarCotizacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlConsultarCotizacion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlModificarCotizacion)
                .addContainerGap(14, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelUsuariosLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jlBotonAtras)
                .addGap(162, 162, 162))
        );
        jPanelUsuariosLayout.setVerticalGroup(
            jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUsuariosLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanelUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlConsultarCotizacion)
                    .addComponent(jlRealizarCotizacion)
                    .addComponent(jlModificarCotizacion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlBotonAtras)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        getContentPane().add(jPanelUsuarios, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //Eventos Botones 

    private void jlRealizarCotizacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRealizarCotizacionMouseClicked
        this.setVisible(false);
        RegistrarCotizacion rv = new RegistrarCotizacion(this, id_vendedor);
        rv.setVisible(true);
    }//GEN-LAST:event_jlRealizarCotizacionMouseClicked

    private void jlConsultarCotizacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlConsultarCotizacionMouseClicked
        ConsultaCotizacion consulta = new ConsultaCotizacion(this);
        this.setVisible(false);
                           

    }//GEN-LAST:event_jlConsultarCotizacionMouseClicked

    private void jlModificarCotizacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlModificarCotizacionMouseClicked
        ModificacionCotizacion modificacion = new ModificacionCotizacion(this);
        modificacion.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jlModificarCotizacionMouseClicked

    private void jlBotonAtrasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlBotonAtrasMouseClicked
        this.setVisible(false);
        padre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlBotonAtrasMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelUsuarios;
    private javax.swing.JLabel jlBotonAtras;
    private javax.swing.JLabel jlConsultarCotizacion;
    private javax.swing.JLabel jlModificarCotizacion;
    private javax.swing.JLabel jlRealizarCotizacion;
    // End of variables declaration//GEN-END:variables
}
