DROP TABLE IF EXISTS empleados CASCADE;
DROP TABLE IF EXISTS vehiculos CASCADE;
DROP TABLE IF EXISTS sedes CASCADE;
DROP TABLE IF EXISTS cotizaciones CASCADE;
DROP TABLE IF EXISTS repuestos CASCADE;
DROP TABLE IF EXISTS ventas CASCADE;
DROP TABLE IF EXISTS ordenes CASCADE;
DROP TABLE IF EXISTS repuestos_ordenes CASCADE;

DROP SEQUENCE IF EXISTS scod_cotizacion CASCADE;
DROP SEQUENCE IF EXISTS scod_orden CASCADE;
DROP SEQUENCE IF EXISTS scod_repuesto CASCADE;
DROP SEQUENCE IF EXISTS scod_sede CASCADE;
DROP SEQUENCE IF EXISTS scod_venta CASCADE;

 CREATE SEQUENCE scod_sede
  start with 1
  increment by 1
  maxvalue 999
  minvalue 1;
  
 CREATE SEQUENCE scod_repuesto
  start with 1
  increment by 1
  maxvalue 1000000
  minvalue 1;
  
  
 CREATE SEQUENCE scod_orden
  start with 1
  increment by 1
  maxvalue 1000000
  minvalue 1;
  
  CREATE SEQUENCE scod_venta
  start with 1
  increment by 1
  maxvalue 10000000
  minvalue 1;
  
  CREATE SEQUENCE scod_cotizacion
  start with 1
  increment by 1
  maxvalue 1000000
  minvalue 1;

CREATE TABLE sedes
(
	cod_sede integer DEFAULT nextval('scod_sede') NOT NULL, 
	nombre character varying(50) UNIQUE NOT NULL,
	direccion character varying(50)  NOT NULL,
	telefono character varying(50) NOT NULL,	
	ciudad character varying(50) NOT NULL,
	cedula_gerente varchar(20) NOT NULL,
	CONSTRAINT sede_pkey PRIMARY KEY(cod_sede)
);

CREATE TABLE empleados
(
	 cedula varchar(20) NOT NULL,
	 nombre varchar(100) NOT NULL,
	 telefono varchar(100) NOT NULL,
	 direccion varchar(199) NOT NULL,
	 email varchar(100) UNIQUE,
	 estado  varchar NOT NULL CHECK (estado='ACTIVO' OR estado='INACTIVO'),
	 perfil varchar NOT NULL CHECK (perfil = 'GERENTE' OR perfil =  'VENDEDOR' OR perfil =  'JEFE TALLER'),
	 usuario varchar(100) UNIQUE NOT NULL,
	 contrasena varchar(100) NOT NULL,
	 fecha_ingreso  date NOT NULL,
	 cod_sede integer NOT NULL,
	 CONSTRAINT empleado_pkey PRIMARY KEY(cedula),
	 CONSTRAINT sede_pkey FOREIGN KEY (cod_sede) REFERENCES sedes (cod_sede) ON UPDATE CASCADE ON DELETE NO ACTION
 );
 
 
 
 CREATE TABLE vehiculos 
 (
	chasis varchar(50) NOT NULL,
	modelo varchar(100) NOT NULL,
	precio integer NOT NULL,
	color varchar(20) NOT NULL,
	placa varchar(20) DEFAULT 'nuevo',
	marca varchar(100),
	estado varchar NOT NULL CHECK (estado='VENDIDO' OR estado='DISPONIBLE'),
	cod_sede integer NOT NULL,
	CONSTRAINT vehiculo_pk PRIMARY KEY(chasis),
	CONSTRAINT sede_vehiculo_fk FOREIGN KEY (cod_sede) REFERENCES sedes (cod_sede) ON UPDATE CASCADE ON DELETE NO ACTION
 );
 
 CREATE TABLE repuestos
 (
	cod_repuesto integer DEFAULT nextval('scod_repuesto') NOT NULL,
	nombre varchar(100) UNIQUE NOT NULL ,
	precio integer NOT NULL,
	cantidad integer NOT NULL,
	descripcion varchar(1000),
	cod_sede integer NOT NULL,
	CONSTRAINT repuesto_pk PRIMARY KEY(cod_repuesto),
	CONSTRAINT sede_repuesto_fk FOREIGN KEY (cod_sede) REFERENCES sedes (cod_sede) ON UPDATE CASCADE ON DELETE NO ACTION
 );
 
CREATE TABLE ordenes
 (
	cod_orden integer DEFAULT nextval('scod_orden') NOT NULL,
	fecha date NOT NULL,
	valor integer,
	cedula varchar(20) NOT NULL,
	descripcion varchar(2000),
	placa_vehiculo varchar(20),
	nombre_cliente varchar(100),
	direccion_cliente varchar(100),
	telefono_cliente varchar(10),
	cod_sede integer,
	CONSTRAINT orden_pk PRIMARY KEY(cod_orden),
	CONSTRAINT cod_sede_fk FOREIGN KEY (cod_sede) REFERENCES sedes(cod_sede) ON UPDATE CASCADE ON DELETE NO ACTION

 );
 
 CREATE TABLE repuestos_ordenes
 (
	cod_orden integer NOT NULL,
	cod_repuesto integer NOT NULL,
	cantidad integer NOT NULL,
	CONSTRAINT repuesto_orden_pk PRIMARY KEY(cod_orden,cod_repuesto),
	CONSTRAINT orden_orden_fk FOREIGN KEY (cod_orden) REFERENCES ordenes (cod_orden) ON UPDATE CASCADE ON DELETE NO ACTION,
	CONSTRAINT orden_repuesto_fk FOREIGN KEY (cod_repuesto) REFERENCES repuestos (cod_repuesto) ON UPDATE CASCADE ON DELETE NO ACTION
 );

 
 CREATE TABLE ventas
 (
	cod_venta integer DEFAULT nextval('scod_venta') NOT NULL,
	cedula varchar(20) NOT NULL,
	chasis varchar(50) NOT NULL,
	fecha date NOT NULL,
   	pago varchar NOT NULL CHECK (pago ='CHEQUE' OR pago='EFECTIVO'),
	cedula_comprador varchar(20) NOT NULL,
	nombre_comprador varchar(100) NOT NULL,
	direccion_comprador varchar(200) NOT NULL,
    telefono_comprador varchar(100) NOT NULL,
	correo_electronico_comprador varchar(100) ,
	cod_sede integer NOT NULL,
	CONSTRAINT venta_pk PRIMARY KEY(cod_venta,chasis,cedula),
	CONSTRAINT chasis_unico UNIQUE(chasis),	
	CONSTRAINT vendedor_fk FOREIGN KEY (cedula) REFERENCES empleados (cedula) ON UPDATE CASCADE ON DELETE NO ACTION,
	CONSTRAINT vehiculo_fk FOREIGN KEY (chasis) REFERENCES vehiculos (chasis) ON UPDATE CASCADE ON DELETE NO ACTION,
	CONSTRAINT cod_sede_fk FOREIGN KEY (cod_sede) REFERENCES sedes(cod_sede) ON UPDATE CASCADE ON DELETE NO ACTION
 );


CREATE TABLE cotizaciones
(
	cod_cotizacion integer DEFAULT nextval('scod_cotizacion') NOT NULL,
	cedula varchar(20) NOT NULL,
	marca varchar(100),
	modelo varchar(100) NOT NULL,
	precio integer NOT NULL,
	color varchar(20) NOT NULL,	
	fecha date NOT NULL,
	cedula_cotizante varchar(20) NOT NULL,
	nombre_cotizante varchar(100) NOT NULL,
	direccion_cotizante varchar(200) NOT NULL,
    telefono_cotizante varchar(100) NOT NULL,
	correo_electronico_cotizante varchar(100) ,
	CONSTRAINT cotizacion_pk PRIMARY KEY(cod_cotizacion),
	CONSTRAINT vendedor_cotizacion_fk FOREIGN KEY (cedula) REFERENCES empleados (cedula) ON UPDATE CASCADE ON DELETE NO ACTION
	
);
